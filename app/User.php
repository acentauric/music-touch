<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    //Informando a tabela relacionada no banco de dados
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Retorna todas as Bandas de um Usuário
    public function bandas()
    {
        return $this->hasMany(Banda::class);
    }

    //Retorna todos os Estilos de um Usuário
    public function estilos()
    {
        return $this->hasMany(Estilo::class);
    }
}
