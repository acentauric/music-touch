<?php

namespace App\Policies;

use App\User;
use App\Banda;

use Illuminate\Auth\Access\HandlesAuthorization;

class BandaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    //Método para determinar se o Usuário pode Deletar a Banda (SQL Delete)
    public function destroy(User $user, Banda $banda)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Banda
        return $user->id === $banda->user_id;
    }

    //Método para determinar se o Usuário pode Atualizar a Banda (SQL Update)
    public function update(User $user, Banda $banda)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Banda
        return $user->id === $banda->user_id;
    }
}
