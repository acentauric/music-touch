<?php

namespace App\Policies;

use App\User;
use App\Crawler;

use Illuminate\Auth\Access\HandlesAuthorization;

class CrawlerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    //Método para determinar se o Usuário pode Deletar a Crawler (SQL Delete)
    public function destroy(User $user, Crawler $crawler)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Crawler
        return $user->id === $crawler->user_id;
    }

    //Método para determinar se o Usuário pode Atualizar a Crawler (SQL Update)
    public function update(User $user, Crawler $crawler)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Crawler
        return $user->id === $crawler->user_id;
    }
}
