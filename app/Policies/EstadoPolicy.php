<?php

namespace App\Policies;

use App\User;
use App\Estado;

use Illuminate\Auth\Access\HandlesAuthorization;

class EstadoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    //Método para determinar se o Usuário pode Deletar a Estado (SQL Delete)
    public function destroy(User $user, Estado $estado)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Estado
        return $user->id === $estado->user_id;
    }

    //Método para determinar se o Usuário pode Atualizar a Estado (SQL Update)
    public function update(User $user, Estado $estado)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular da Estado
        return $user->id === $estado->user_id;
    }
}
