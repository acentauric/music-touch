<?php

namespace App\Policies;

use App\User;
use App\Estilo;

use Illuminate\Auth\Access\HandlesAuthorization;

class EstiloPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    //Método para determinar se o Usuário pode Deletar o Estilo (SQL Delete)
    public function destroy(User $user, Estilo $estilo)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular do Estilo
        return $user->id === $estilo->user_id;
    }

    //Método para determinar se o Usuário pode Atualizar o Estilo (SQL Update)
    public function update(User $user, Estilo $estilo)
    {
        //Realiza uma comparação do Usuário logado com o Usuário titular do Estilo
        return $user->id === $estilo->user_id;
    }
}
