<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Estilo extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'estilos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome'];

    /**
     * Get the user that owns the band.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
