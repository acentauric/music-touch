<?php

namespace App\Repositories;

use App\User;
use DB;

class EstiloRepository
{
    //Retorna todos os registros cadastradas
    public function all()
    {
        return $estilos = DB::select('select * from estilos');
    }

    //Retorna todos os registros cadastrados por um Usuário
    public function oneUser($user)
    {
        return $estilos = DB::select('select * from estilos where user_id = ?', [$user]);
    }

    //Retorna todos os registros cadastrados por outros Usuários
    public function otherUsers($user)
    {
        return $estilos = DB::select('select * from estilos where user_id <> ?', [$user]);
    }

    //Retorna todas os Registros de um Usuário
    public function forUser(User $user)
    {
        return $user->estilos()
            ->orderBy('created_at', 'asc')
            ->get();
    }
}