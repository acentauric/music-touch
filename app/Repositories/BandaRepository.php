<?php

namespace App\Repositories;

use App\User;
use DB;

class BandaRepository
{
    //Retorna todos os registros cadastradas
    public function all()
    {
        return $bandas = DB::select('select * from bandas');
    }

    //Retorna todos os registros cadastrados por um Usuário
    public function oneUser($user)
    {
        return $bandas = DB::select('select * from bandas where user_id = ?', [$user]);
    }

    //Retorna todos os registros cadastrados por outros Usuários
    public function otherUsers($user)
    {
        return $bandas = DB::select('select * from bandas where user_id <> ?', [$user]);
    }

    //Retorna todas os Estilos de um Usuário
    public function forUser(User $user)
    {
        return $user->bandas()
            ->orderBy('created_at', 'asc')
            ->get();
    }
}