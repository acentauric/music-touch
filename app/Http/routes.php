<?php


//Rota para a tela do módulo de bandas
Route::get('/website/modulos/bandas', function () {
    return view('website/modulos/bandas');
});

//***********************************
//Rota para Website/Home
if(true)
{
    Route::get('/',                                     'Website\Home\HomeController@index');
    Route::get('/website/home',                         'Website\Home\HomeController@index');
    Route::get('/website/home/auth/login',              'Website\Home\HomeController@login');
    Route::get('/website/home/auth/passwords/email',    'Website\Home\HomeController@email');
    Route::get('/website/informacoes/quem-somos',        'Website\Home\HomeController@quemsomos');
}

//***********************************
//Rota para Website/Contato
if(true)
{
    Route::get('/website/contato',  'Website\Contato\ContatoController@index');
    Route::post('/website/contato', 'Website\Contato\ContatoController@enviar');
}

//***********************************
//Rota para Autenticação
if(true)
{
    Route::auth();
}

//***********************************
//Rotas para Admin/Músicos
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/admin/musicos', 'Admin\MusicoController@index');

    //Rotas READ para o Módulo
    Route::get('/admin/musicos/consultar/{musico}', 'Admin\MusicoController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/admin/musicos/editar/{musico}', 'Admin\MusicoController@edit');
    Route::put('/admin/musicos/atualizar/{musico}','Admin\MusicoController@update');

    //Rotas AJAX o Módulo
    Route::post('/admin/musicos/ajax', 'Admin\MusicoController@ajax');
}

//***********************************
//Rotas para Admin/Produtoras
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/admin/produtoras', 'Admin\ProdutoraController@index');

    //Rotas CREAT para o Módulo
    Route::get('/admin/produtoras/cadastrar', 'Admin\ProdutoraController@create');
    Route::post('/admin/produtoras/', 'Admin\ProdutoraController@store');

    //Rotas READ para o Módulo
    Route::get('/admin/produtoras/consultar/{produtora}', 'Admin\ProdutoraController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/admin/produtoras/editar/{produtora}', 'Admin\ProdutoraController@edit');
    Route::put('/admin/produtoras/atualizar/{produtora}','Admin\ProdutoraController@update');
    Route::get('/admin/produtoras/gerenciar/{produtora}','Admin\ProdutoraController@manage');

    //Rotas AJAX o Módulo
    Route::post('/admin/produtoras/ajax', 'Admin\ProdutoraController@ajax');
}


//***********************************
//Rotas para Admin/Casas de Show
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/admin/casas-de-show', 'Admin\CasaDeShowController@index');

    //Rotas AJAX o Módulo
    Route::post('/admin/casas-de-show/ajax', 'Admin\CasaDeShowController@ajax');
}

//***********************************
//Rotas para Admin/Cadastro/Geográfico/Estados
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/estados', 'Admin\EstadoController@index');
    Route::get('/estados/filtro/{filtro}', 'Admin\EstadoController@index');

    //Rotas CREAT para o Módulo
    Route::get('/estado/cadastrar', 'Admin\EstadoController@create');
    Route::post('/estado', 'Admin\EstadoController@store');

    //Rotas READ para o Módulo
    Route::get('/estado/consultar/{estado}', 'Admin\EstadoController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/estado/editar/{estado}', 'Admin\EstadoController@edit');
    Route::put('/estado/{estado}','Admin\EstadoController@update');

    //Rotas DELETE para o Módulo
    Route::delete('/estado/{estado}', 'Admin\EstadoController@destroy');

    //Rotas AJAX o Módulo
    Route::post('/estadoAjax', 'Admin\EstadoController@ajax');
    Route::get('/admin/estadoAjax/get-cidades/{estado}', 'Admin\EstadoController@get_cidades');
}

//***********************************
//Rotas para Admin/Cadastro/Geográfico/Cidades
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/cidades', 'Admin\CidadeController@index');
    Route::get('/cidades/filtro/{filtro}', 'Admin\CidadeController@index');

    //Rotas CREAT para o Módulo
    Route::get('/cidade/cadastrar', 'Admin\CidadeController@create');
    Route::post('/cidade', 'Admin\CidadeController@store');

    //Rotas READ para o Módulo
    Route::get('/cidade/consultar/{cidade}', 'Admin\CidadeController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/cidade/editar/{cidade}', 'Admin\CidadeController@edit');
    Route::put('/cidade/{cidade}','Admin\CidadeController@update');

    //Rotas DELETE para o Módulo
    Route::delete('/cidade/{cidade}', 'Admin\CidadeController@destroy');

    //Rotas AJAX o Módulo
    Route::post('/cidadeAjax', 'Admin\CidadeController@ajax');
    Route::get('/get-cidades/{idEstado}', 'Admin\CidadeController@getCidadesByEstado');
    
}

//***********************************
//Rotas para Admin/Cadastro/Musical/Estilos
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/estilos', 'Admin\EstiloController@index');
    Route::get('/estilos/filtro/{filtro}', 'Admin\EstiloController@index');

    //Rotas CREAT para o Módulo
    Route::get('/estilo/cadastrar', 'Admin\EstiloController@create');
    Route::post('/estilo', 'Admin\EstiloController@store');

    //Rotas READ para o Módulo
    Route::get('/estilo/consultar/{estilo}', 'Admin\EstiloController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/estilo/editar/{estilo}', 'Admin\EstiloController@edit');
    Route::put('/estilo/{estilo}','Admin\EstiloController@update');

    //Rotas DELETE para o Módulo
    Route::delete('/estilo/{estilo}', 'Admin\EstiloController@destroy');

    //Rotas AJAX o Módulo
    Route::post('/estiloAjax', 'Admin\EstiloController@ajax');
}

//***********************************
//Rotas para Admin/Cadastro/Musical/Instrumentos
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/instrumentos', 'Admin\InstrumentoController@index');

    //Rotas AJAX o Módulo
    Route::post('/instrumentoAjax', 'Admin\InstrumentoController@ajax');
}

//***********************************
//Rotas para Admin/Bandas
if(true)
{
    //Rotas INDEX para o Módulo
    Route::get('/admin/bandas', 'Admin\BandaController@index');
    Route::get('/admin/bandas/listar/shows', 'Admin\ShowController@index');

    //Rotas CREATE para o Módulo
    Route::get('/banda/cadastrar', 'BandaController@create');    
    Route::post('/banda', 'BandaController@store');
    Route::get('/admin/bandas/cadastrar/show', 'Admin\BandaController@createShow');
    Route::post('/admin/bandas/cadastrar/show', 'Admin\BandaController@storeShow');

    //Rotas READ para o Módulo
    Route::get('/admin/bandas/consultar/{banda}', 'Admin\BandaController@show');

    //Rotas UPDATE para o Módulo
    Route::get('/admin/bandas/editar/{banda}', 'Admin\BandaController@edit');
    Route::get('/admin/bandas/gerenciar/{banda}','Admin\BandaController@manage');
    Route::put('/admin/bandas/atualizar/{banda}', 'Admin\BandaController@update');
    Route::get('/admin/bandas/conexoes/{banda}/{option}','Admin\BandaController@connections');

    //Rotas DELETE para o Módulo
    Route::delete('/banda/{banda}', 'BandaController@destroy');

    //Rotas AJAX o Módulo
    Route::post('/admin/bandas/ajax', 'Admin\BandaController@ajax');
    Route::post('/admin/shows/ajax', 'Admin\ShowController@ajax');
}

//***********************************
//Rotas para Admin/Crawler
if(true)
{
    Route::get('/crawlers', 'Admin\CrawlerController@index');
    Route::get('/crawlers/{crawler}', 'Admin\CrawlerController@index');

    Route::post('/crawlerFsbAjax', 'Admin\CrawlerController@importAjax');
}