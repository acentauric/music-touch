<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Estilo;
use App\Repositories\EstiloRepository;

//Lógica do Controler
class EstiloController extends Controller
{
    //Instância para a classe Repositório
    protected $estilos;

    //Método Executado na Construção do Controller
    public function __construct(EstiloRepository $estilos)
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');

        //Instancia um objeto do tipo Repositório
        $this->estilos = $estilos;
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        //Apresenta a tela inicial de acordo com o filtro realizado pelo Usuário
        switch ($filtro)
        {
            //Captura todos os registros cadastradas
            //case 'todas': $estilos = Estilo::all(); break;
            case 'todos': $estilos = $this->estilos->all();break;

            //Captura todos os registros cadastrados por um Usuário
            //case 'minhas': $estilos = $this->estilos->forUser($request->user()); break;
            case 'meus': $estilos = $this->estilos->oneUser($request->user()->id); break;

            //Captura todos os registros cadastrados por outros Usuários
            case 'outros': $estilos = $this->estilos->otherUsers($request->user()->id);break;
        }

        //Redireciona para a view passando as informações das estilos
        return view('estilos.index', ['estilos' => $estilos]);
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('estilos.create');
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255',
        ]);

        //Converte os dados para adição no banco de dados
        //...

        //Cadastra o Novo Registro no Banco de Dados
        $request->user()->estilos()->create([
            'nome' => $request->nome,
        ]);

        //Retorna para a tela inicial
        return redirect('/estilos');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Estilo $estilo)
    {
        //Retorna para a view enviando os dados do Registro
        return view('estilos.show', [
            'estilo' => $estilo,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Estilo $estilo)
    {
        //Retorna para a view com o Formulário de Edição
        return view('estilos.edit', ['estilo' => $estilo]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Estilo $estilo)
    {
        //Verifica se existe autorização para a Atualização do Registro
        $this->authorize('update', $estilo);

        //Captura os Campos apresentados para Atualização
        $estilo->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $estilo->save();

        //Retorna para a tela inicial
        return redirect('/estilos');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Estilo $estilo)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $estilo);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $estilo->delete();

        //Retorna para a tela inicial
        return redirect('/estilos');
    }
}