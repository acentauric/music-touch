<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Produtora;
use App\Models\Admin\Estado;
use App\Models\Admin\Cidade;

//Lógica do Controler
class ProdutoraController extends Controller
{
    //Instância para a classe Repositório
    protected $produtoras;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        //Captura todos os registros cadastradas
        $produtoras = Produtora::all();

        //Redireciona para a view passando as informações das produtoras
        return view('admin.produtoras.index', ['produtoras' => $produtoras])
            ->with('registrosJson', json_encode($produtoras))
            ->with('registrosQtd', count($produtoras))
            ->with('pageName', 'Produtoras');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request, Produtora $produtora)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('admin.produtoras.create', ['produtora' => $produtora]);
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request, Produtora $produtora)
    {
        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255',
        ]);

        //Converte os dados para adição no banco de dados
        //$request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        //$request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        $produtora->create([
            'nome' => $request->nome,
            'user_id' => Auth::id(),
        ]);

        //Retorna para a tela inicial
        return redirect('/admin/produtoras');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Produtora $produtora)
    {
        //Retorna para a view enviando os dados do Registro
        return view('admin.produtoras.show', [
            'produtora' => $produtora,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Produtora $produtora)
    {
        //Retorna para a view com o Formulário de Edição
        return view('admin.produtoras.edit', ['produtora' => $produtora]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Produtora $produtora)
    {
        //Verifica se existe autorização para a Atualização do Registro
        //$this->authorize('update', $produtora);

        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255'
        ]);

        //Converte os dados para adição no banco de dados
        //if ($request->data_de_nascimento != ''){$request->data_de_nascimento = implode("-",array_reverse(explode("/",$request->data_de_nascimento)));}else{$request->data_de_nascimento = NULL;}

        //Captura os Campos apresentados para Atualização
        $produtora->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $produtora->save();

        //Retorna para a tela inicial
        return redirect('/admin/produtoras');
    }

    //Método para Preparar o Gerenciamento de um Registro
    public function manage(Request $request, Produtora $produtora)
    {
        //Retorna para a view com o Formulário de Edição
        return view('admin.produtoras.manage', ['produtora' => $produtora])
            ->with('estados', Estado::orderBy('nome', 'asc')->get())
            ->with('cidades', Cidade::orderBy('nome', 'asc')->get());;
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Produtora $produtora)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $produtora);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $produtora->delete();

        //Retorna para a tela inicial
        return redirect('/produtoras');
    }

    //Método para Tratar o Registro via Ajax
    public function ajax(Request $request, Produtora $produtora)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $produtora->create([
                    'nome' => $request->nome,
                    'user_id' => Auth::id(),
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

                break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $produtora = Produtora::find($request->id);

                //Captura os Campos apresentados para Atualização
                $produtora->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $produtora->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

                break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $produtora = Produtora::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $produtora->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

                break;
        }
    }
}