<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Estilo;

//Lógica do Controler
class EstiloController extends Controller
{
    //Instância para a classe Repositório
    protected $estilos;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index()
    {
        //Captura todos os registros cadastradas
        $estilos = Estilo::all();

        //Realiza tratamento para retornar os dados em Json para o JqGrid
        $estilosJson = json_encode($estilos);

        //Armazena a quantidade de registros retornados do banco de dados
        $estilosQtd = count($estilos);

        //Redireciona para a view passando as informações das estilos
        return view('admin.estilos.index', ['estilos' => $estilos])
            ->with('estilosJson', $estilosJson)
            ->with('estilosQtd', $estilosQtd)
            ->with('pageName', 'Estilos');
    }

    //Método para Tratar o Registro via Ajax
    public function ajax(Request $request, Estilo $estilo)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $estilo->create([
                    'nome' => $request->nome,
                    'user_id' => Auth::id(),
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

            break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $estilo = Estilo::find($request->id);

                //Captura os Campos apresentados para Atualização
                $estilo->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $estilo->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

            break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $estilo = Estilo::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $estilo->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

            break;
        }
    }
}