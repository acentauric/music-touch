<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Musico;
use App\Models\Instrumento;
use App\Models\Admin\Estado;
use App\Models\Admin\Cidade;

//Lógica do Controler
class MusicoController extends Controller
{
    //Instância para a classe Repositório
    protected $musicos;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        //Captura todos os registros cadastradas
        $musicos = Musico::all();

        //Redireciona para a view passando as informações das musicos
        return view('admin.musicos.index', ['musicos' => $musicos])
            ->with('registrosJson', json_encode($musicos))
            ->with('registrosQtd', count($musicos))
            ->with('pageName', 'Musicos');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('musicos.create');
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        //$this->validate($request, [
        //    'nome' => 'required|max:255',
        //    'data_de_formacao' => 'required|date_format:"d/m/Y"',
        //    'data_de_termino' => 'date_format:"d/m/Y"|after:data_de_formacao',
        //]);

        //Converte os dados para adição no banco de dados
        //$request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        //$request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        //$request->user()->musicos()->create([
        //    'nome' => $request->nome,
        //    'data_de_formacao' => $request->data_de_formacao,
        //    'data_de_termino' => $request->data_de_termino,
        //]);

        //Retorna para a tela inicial
        return redirect('/musicos');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Musico $musico)
    {		
        //Retorna para a view enviando os dados do Registro
        return view('admin.musicos.show', [
            'musico' => $musico,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Musico $musico)
    {
        //Retorna para a view com o Formulário de Edição
        return view('admin.musicos.edit', ['musico' => $musico])
            ->with('instrumentos', Instrumento::orderBy('nome', 'asc')->get())
            ->with('estados', Estado::orderBy('nome', 'asc')->get())
            ->with('cidades', Cidade::orderBy('nome', 'asc')->get());
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Musico $musico)
    {
        //Verifica se existe autorização para a Atualização do Registro
        //$this->authorize('update', $musico);

        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255',
            'data_de_nascimento' => 'date_format:"d/m/Y"|before:tomorrow',
        ]);

        //Converte os dados para adição no banco de dados
        if ($request->data_de_nascimento != ''){$request->data_de_nascimento = implode("-",array_reverse(explode("/",$request->data_de_nascimento)));}else{$request->data_de_nascimento = NULL;}
        if ($request->tipo_de_funcao == 'NULL'){$request->tipo_de_funcao = NULL;}
        if ($request->estado_id == 'NULL'){$request->estado_id = NULL;}
        if ($request->logradouro == ''){$request->logradouro = NULL;}

        //Captura os Campos apresentados para Atualização
        $musico->nome = $request->nome;
        $musico->data_de_nascimento = $request->data_de_nascimento;
        $musico->tipo_de_funcao = $request->tipo_de_funcao;
        $musico->estado_id = $request->estado_id;
        $musico->logradouro = $request->logradouro;

        //Atualiza o Registro no Banco de dados
        $musico->save();

        //Retorna para a tela inicial
        return redirect('/admin/musicos');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Musico $musico)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $musico);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $musico->delete();

        //Retorna para a tela inicial
        return redirect('/musicos');
    }

    //Método para Tratar o Registro via Ajax
    public function ajax(Request $request, Musico $musico)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $musico->create([
                    'nome' => $request->nome,
                    'user_id' => Auth::id(),
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

                break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $musico = Musico::find($request->id);

                //Captura os Campos apresentados para Atualização
                $musico->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $musico->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

                break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $musico = Musico::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $musico->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

                break;
        }
    }
}