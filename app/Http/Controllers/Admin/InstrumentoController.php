<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Instrumento;

//Lógica do Controler
class InstrumentoController extends Controller
{
    //Instância para a classe Repositório
    protected $instrumentos;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index()
    {
        //Captura todos os registros cadastradas
        $instrumentos = Instrumento::all();

        //Realiza tratamento para retornar os dados em Json para o JqGrid
        $instrumentosJson = json_encode($instrumentos);

        //Armazena a quantidade de registros retornados do banco de dados
        $instrumentosQtd = count($instrumentos);

        //Redireciona para a view passando as informações das instrumentos
        return view('admin.instrumentos.index', ['instrumentos' => $instrumentos])
            ->with('instrumentosJson', $instrumentosJson)
            ->with('instrumentosQtd', $instrumentosQtd)
            ->with('pageName', 'Instrumentos');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete) via Ajax
    public function ajax(Request $request, Instrumento $instrumento)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $instrumento->create([
                    'nome' => $request->nome,
                    'user_id' => Auth::id(),
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

            break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $instrumento = Instrumento::find($request->id);

                //Captura os Campos apresentados para Atualização
                $instrumento->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $instrumento->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

            break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $instrumento = Instrumento::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $instrumento->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

            break;
        }
    }
}