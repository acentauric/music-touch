<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cidade;
use App\Models\Estado;

//Lógica do Controler
class CidadeController extends Controller
{
    //Instância para a classe Repositório
    protected $cidades;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        //Apresenta a tela inicial de acordo com o filtro realizado pelo Usuário
        switch ($filtro)
        {
            //Captura todos os registros cadastradas
            case 'todos':  $cidades = Cidade::all();break;

            //Captura todos os registros cadastrados por um Usuário
            case 'meus': $cidades = Cidade::forUser($request->user()->id);break;

            //Captura todos os registros cadastrados por outros Usuários
            case 'outros': $cidades = Cidade::otherUsers($request->user()->id);break;
        }

        //Realiza tratamento para retornar os dados em Json para o JqGrid
        $cidadesJson = json_encode($cidades);

        //Armazena a quantidade de registros retornados do banco de dados
        $cidadesQtd = count($cidades);

        //Redireciona para a view passando as informações das cidades
        return view('admin.cidades.index', ['cidades' => $cidades])
            ->with('cidadesJson', $cidadesJson)
            ->with('cidadesQtd', $cidadesQtd)
            ->with('pageName', 'Cidades');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('cidades.create');
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        //$this->validate($request, [
        //    'nome' => 'required|max:255',
        //    'data_de_formacao' => 'required|date_format:"d/m/Y"',
        //    'data_de_termino' => 'date_format:"d/m/Y"|after:data_de_formacao',
        //]);

        //Converte os dados para adição no banco de dados
        //$request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        //$request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        //$request->user()->cidades()->create([
        //    'nome' => $request->nome,
        //    'data_de_formacao' => $request->data_de_formacao,
        //    'data_de_termino' => $request->data_de_termino,
        //]);

        //Retorna para a tela inicial
        return redirect('/cidades');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Cidade $cidade)
    {
        //Retorna para a view enviando os dados do Registro
        return view('cidades.show', [
            'cidade' => $cidade,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Cidade $cidade)
    {
        //Retorna para a view com o Formulário de Edição
        return view('cidades.edit', ['cidade' => $cidade]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Cidade $cidade)
    {
        //Verifica se existe autorização para a Atualização do Registro
        $this->authorize('update', $cidade);

        //Captura os Campos apresentados para Atualização
        $cidade->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $cidade->save();

        //
        echo 'cheguei aquiiiiiiiiiiii'; return (0);

        //Retorna para a tela inicial
        return redirect('/cidades');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Cidade $cidade)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $cidade);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $cidade->delete();

        //Retorna para a tela inicial
        return redirect('/cidades');
    }

    //Método para tratar um Registro do Banco de Dados via Ajax
    public function ajax(Request $request, Cidade $cidade)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $cidade->create([
                    'nome' => $request->nome,
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

            break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $cidade = Cidade::find($request->id);

                //Captura os Campos apresentados para Atualização
                $cidade->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $cidade->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

            break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $cidade = Cidade::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $cidade->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

            break;
        }
    }

    //Método para Retornar Cidades de um Estado via Ajax
    public function getCidadesByEstado($idEstado)
    {
        //Captura o Estado a Partir do Id enviado
        $estado = Estado::find($idEstado);

        //Captura todas as cidades do Estado Capturado
        $cidades = $estado->cidades()->getQuery()->get(['id', 'nome']);

        //Retorna um Objeto Json com as Cidades do Estado selecionado
        return response()->json($cidades);
    }
}