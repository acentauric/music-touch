<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Show;
use App\Models\Admin\Estado;
use App\Models\Admin\Cidade;
use App\Models\Admin\CasaDeShow;
use App\Repositories\ShowRepository;

//Lógica do Controler
class ShowController extends Controller
{
    //Instância para a classe Repositório
    protected $shows;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        ///Captura todos os registros cadastradas
        $shows = Show::all();

        //Redireciona para a view passando as informações das shows
        return view('admin.shows.index', ['shows' => $shows])
            ->with('registrosJson', json_encode($shows))
            ->with('registrosQtd', count($shows))
            ->with('pageName', 'Shows');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('shows.create');
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255',
            'data_de_formacao' => 'required|date_format:"d/m/Y"',
            'data_de_termino' => 'date_format:"d/m/Y"|after:data_de_formacao',
        ]);

        //Converte os dados para adição no banco de dados
        $request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        $request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        $request->user()->shows()->create([
            'nome' => $request->nome,
            'data_de_formacao' => $request->data_de_formacao,
            'data_de_termino' => $request->data_de_termino,
        ]);

        //Retorna para a tela inicial
        return redirect('/shows');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function createShow(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('admin.shows.createShow')
            ->with('casas_de_show', CasaDeShow::orderBy('nome', 'asc')->get())
            ->with('shows', Show::orderBy('nome', 'asc')->get());
    }
    
    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Show $show)
    {
        //Retorna para a view enviando os dados do Registro
        return view('shows.show', [
            'show' => $show,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Show $show)
    {
        //Retorna para a view com o Formulário de Edição
        return view('shows.edit', ['show' => $show]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Show $show)
    {
        //Verifica se existe autorização para a Atualização do Registro
        //$this->authorize('update', $show);

        //Realiza o processo de validação dos campos do formulário
        $this->validate($request, [
            'nome' => 'required|max:255',
            'telefone_fixo' => 'max:14',
            'telefone_celular' => 'max:15',
            'email' => 'max:100',
            'site' => 'max:100',
            'facebook' => 'max:100',
            'email' => 'max:100',            
            'logradouro' => 'max:255',
            'numero' => 'max:50',
            'cep' => 'max:10',
        ]);

        //Converte os dados para adição no banco de dados
        if ($request->telefone_fixo == ''){$request->telefone_fixo = NULL;}
        if ($request->telefone_celular == ''){$request->telefone_celular = NULL;}
        if ($request->email == ''){$request->email = NULL;}
        if ($request->site == ''){$request->site = NULL;}
        if ($request->facebook == ''){$request->facebook = NULL;}
        if ($request->youtube == ''){$request->youtube = NULL;}
        if ($request->cidade_id == 'NULL'){$request->cidade_id = NULL;}
        if ($request->logradouro == ''){$request->logradouro = NULL;}
        if ($request->numero == ''){$request->numero = NULL;}
        if ($request->cep == ''){$request->cep = NULL;}

        //Captura os Campos apresentados para Atualização
        $show->nome = $request->nome;
        $show->telefone_fixo = $request->telefone_fixo;
        $show->telefone_celular = $request->telefone_celular;
        $show->email = $request->email;
        $show->site = $request->site;
        $show->facebook = $request->facebook;
        $show->youtube = $request->youtube;
        $show->cidade_id = $request->cidade_id;
        $show->logradouro = $request->logradouro;
        $show->numero = $request->numero;
        $show->cep = $request->cep;
        
        //Atualiza o Registro no Banco de dados
        $show->save();

        //Retorna para a tela inicial
        return redirect('/admin/shows');
    }

    //Método para Preparar o Gerenciamento de um Registro
    public function manage(Request $request, Show $show)
    {
        //Retorna para a view com o Formulário de Gerenciamento
        return view('admin.shows.manage', ['show' => $show])
            ->with('estados', Estado::orderBy('nome', 'asc')->get())
            ->with('cidades', Cidade::orderBy('nome', 'asc')->get());
    }

    //Método para Preparar o Gerenciamento das Conexões de um Registro
    public function connections(Request $request, Show $show)
    {
        //Realiza todas as ações relacioandas ao crawler
        if (true)
        {
            //Define a URL do Site que será acessada para realizar o Crawler
            $site = "http://www.fotolog.com";

            //Define o PROFILE que será acessado para realizar o Crawler
            $profile = "/hardcallrock";

            //Define a PÁGINA que será acessada para realizar o Crawler
            $page = "/mosaic";

            //Define a URL do Site que será acessado para realizar o Crawler
            $file = $site.$profile.$page;

            //Instancia um novo objeto do tipo DOMDocument para o devido tratamento dos caracteres
            $doc = new \DOMDocument('1.0', 'UTF-8');

            //Set error level
            $internalErrors = libxml_use_internal_errors(true);

            //Carrega o arquivo HTML utilizando o método do DOMDocument
            $doc->loadHTMLFile($file);

            //Restore error level
            libxml_use_internal_errors($internalErrors);

            //Instancia o objeto xPath a partir do arquivo HTML carregado
            $xpath = new \DOMXpath($doc);

            //Realiza um switch para definir qual informação será coletada (Informações do Profile)
            $xPathQuery = '//*[@id="wall_infos_profile"]';

            //Executa a query xPath e armazena o resultado em um Array
            $elements = $xpath->query($xPathQuery);

            //Captura LINK e USUÁRIO
            if (true)
            {
                //Realiza um switch para definir qual informação será coletada (Informações do Profile)
                $xPathQuery = '//*[@id="wall_infos_profile"]/h3/a';

                //Executa a query xPath e armazena o resultado em um Array
                $elements = $xpath->query($xPathQuery);

                //Percorre todos os elementos retornados
                foreach ($elements as $element)
                {
                    //Armazena o LINK do Perfil Rastreado
                    $crawled["link"] = trim($element->getAttribute('href'));

                    //Captura todos os childNodes do Elemento
                    $nodes = $element->childNodes;

                    //Percorre todos os childNodes capturados ()
                    foreach ($nodes as $node)
                    {
                        //Armana o USUÁRIO do Perfil Rastreado
                        $crawled["user"] = trim($node->nodeValue);
                    }
                }
            }

            //Captura FOTO DO PERFIL
            if (true)
            {
                //Realiza um switch para definir qual informação será coletada (Informações do Profile)
                $xPathQuery = '//*[@id="wall_infos_profile"]/img';

                //Executa a query xPath e armazena o resultado em um Array
                $elements = $xpath->query($xPathQuery);

                //Percorre todos os elementos retornados
                foreach ($elements as $element)
                {
                    //Armazena a FOTO DO PERFIL Rastreado
                    $crawled["foto"] = trim($element->getAttribute('src'));
                }
            }

            //Captura SEXO, ESTADO CIVIL, ANIVERSÁRIO, DATA DE CADASTRO
            if (true)
            {
                //Realiza um switch para definir qual informação será coletada (Informações do Profile)
                $xPathQuery = '//*[@id="wall_infos_profile"]/p[1]/text()[1]';

                //Executa a query xPath e armazena o resultado em um Array
                $elements = $xpath->query($xPathQuery);

                //Percorre todos os elementos retornados
                foreach ($elements as $element)
                {
                    //Separa as informações presentes no node (Sexo; Estado Civil; Aniversário)
                    $arraySexoEstadoCivilAniversario  = explode('-', $element->nodeValue);

                    //Remove os epaços em branco desnecesásrio dos dados separados
                    $arraySexoEstadoCivilAniversario[0] = trim($arraySexoEstadoCivilAniversario[0]);
                    $arraySexoEstadoCivilAniversario[1] = trim($arraySexoEstadoCivilAniversario[1]);
                    $arraySexoEstadoCivilAniversario[2] = trim($arraySexoEstadoCivilAniversario[2]);

                    //Realiza tratamento para apresentar os dados corretamente (Sexo)
                    switch ($arraySexoEstadoCivilAniversario[0])
                    {
                        case 'Homem':  $arraySexoEstadoCivilAniversario[0] = 'Masculino'; break;
                        case 'Mulher':  $arraySexoEstadoCivilAniversario[0] = 'Feminino'; break;
                    }

                    //Realiza tratamento para apresentar os dados corretamente (Estado Civil)
                    switch ($arraySexoEstadoCivilAniversario[1])
                    {
                        case 'single':  $arraySexoEstadoCivilAniversario[1] = 'Solteiro(a)'; break;
                        case 'seeing_someone':  $arraySexoEstadoCivilAniversario[1] = 'Solteiro(a)'; break;
                        case 'married':  $arraySexoEstadoCivilAniversario[1] = 'Casado(a)'; break;
                    }

                    //Realiza tratamento para apresentar os dados corretamente (Data de Aniversário)
                    switch ($arraySexoEstadoCivilAniversario[2])
                    {
                        case 'single':  $arraySexoEstadoCivilAniversario[2] = $arraySexoEstadoCivilAniversario[2]; break;
                    }

                    //Armazena a FOTO DO PERFIL Rastreado
                    $crawled["sexo"] = $arraySexoEstadoCivilAniversario[0];
                    $crawled["estado_civil"] = $arraySexoEstadoCivilAniversario[1];
                    $crawled["aniversario"] = $arraySexoEstadoCivilAniversario[2];
                }
            }

            //Captura ENDEREÇO
            if (true)
            {
                //Realiza um switch para definir qual informação será coletada (Informações do Profile)
                $xPathQuery = '//*[@id="wall_infos_profile"]/p[2]/b';

                //Executa a query xPath e armazena o resultado em um Array
                $elements = $xpath->query($xPathQuery);

                //Percorre todos os elementos retornados
                foreach ($elements as $element)
                {
                    //Armazena o Link do Perfil Rastreado
                    $crawled["link"] = trim($element->getAttribute('href'));

                    //Captura todos os childNodes do Elemento
                    $nodes = $element->childNodes;

                    //Percorre todos os childNodes capturados ()
                    foreach ($nodes as $node)
                    {
                        //Armazena o Nome do Perfil Rastreado
                        $cidadeEstadoPais[] = trim($node->nodeValue);
                    }
                }

                //Armazena o ENDEREÇO do Perfil Rastreado
                $crawled["cidade"] = $cidadeEstadoPais[0];
                $crawled["estado"] = $cidadeEstadoPais[1];
                $crawled["pais"] = $cidadeEstadoPais[2];
            }

            //Realiza tratamento para transoformar o array com os dados em Objeto PHP
            $crawled = (object)$crawled;
        }

        //Realiza uma comparação entre os dados rastreados e os dados armazenados em banco de dados
        if (true)
        {
            //Verifica se o nome do usuário é o mesmo

            //Verifica se o nome do usuário é o mesmo

        }

        //Retorna para a view com o Formulário de Gerenciamento das Conexões
        return view('admin.shows.connections', ['show' => $show])
            ->with('crawled', $crawled);
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Show $show)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $show);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $show->delete();

        //Retorna para a tela inicial
        return redirect('/shows');
    }

    //Método para Tratar o Registro via Ajax
    public function ajax(Request $request, Show $show)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $show_new = $show->create([
                    'nome' => $request->nome,
                    'user_id' => Auth::id(),
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!', 'id' => $show_new->id]);

                break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $show = Show::find($request->id);

                //Captura os Campos apresentados para Atualização
                $show->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $show->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

                break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $show = Show::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $show->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

                break;
        }
    }
}