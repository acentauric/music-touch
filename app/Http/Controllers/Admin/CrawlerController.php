<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Crawler;
use App\Models\Musico;

//Lógica do Controler
class CrawlerController extends Controller
{
    //Instância para a classe Repositório
    protected $crawlers;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        //$this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $crawler="fsb")
    {
        //Apresenta a tela inicial de acordo com o filtro realizado pelo Usuário
        switch ($crawler)
        {
            //Apresenta o Crawler para o site Forme sua Banda
            case 'fsb':

                //Defino a URL do Site que será acessando para realizar o Crawler
                $file = "http://www.formesuabanda.com.br/index.php";

                //Realiza um switch para definir qual será o tipo de registro
                switch ($tipo=1)
                {
                    case 1:  $file .= '?tipo=1'; break; //Músicos
                    case 2:  $file .= '?tipo=2'; break; //Bandas
                }

                //Realiza um switch para definir qual será o tipo de instrumento
                switch ($instrumento=-1)
                {
                    case -1: $file .= '&instrumento='; break; //Todos
                    case 0: $file .= '&instrumento=0'; break; //Banjoísta
                    case 1: $file .= 'instrumento=1'; break; //Vocalista
                    case 2: $file .= 'instrumento=2'; break; //Baixista
                    case 3: $file .= 'instrumento=3'; break; //Baterista
                    case 4: $file .= 'instrumento=4'; break; //Percussionista
                    case 5: $file .= 'instrumento=5'; break; //Gaitista
                    case 6: $file .= 'instrumento=6'; break; //Guitarrista (Base)
                    case 7: $file .= 'instrumento=7'; break; //Guitarrista (Solo)
                    case 8: $file .= 'instrumento=8'; break; //Cavaquinista
                    case 9: $file .= 'instrumento=9'; break; //Pandeirista
                    case 10: $file .= 'instrumento=10'; break; //Tecladista
                    case 11: $file .= 'instrumento=11'; break; //Pianista
                    case 12: $file .= 'instrumento=12'; break; //Violinista (violino)
                    case 13: $file .= 'instrumento=13'; break; //Violonista (violão)
                    case 14: $file .= 'instrumento=14'; break; //Violeiro (viola)
                    case 15: $file .= 'instrumento=15'; break; //DJ
                    case 16: $file .= 'instrumento=16'; break; //Harpista
                    case 17: $file .= 'instrumento=17'; break; //Sanfoneiro
                    case 18: $file .= 'instrumento=18'; break; //Zabumbeiro
                    case 19: $file .= 'instrumento=19'; break; //Triangulista
                    case 20: $file .= 'instrumento=20'; break; //Flautista
                    case 21: $file .= 'instrumento=21'; break; //Saxofonista
                    case 22: $file .= 'instrumento=22'; break; //Trompetista
                    case 23: $file .= 'instrumento=23'; break; //Trombonista
                    case 24: $file .= 'instrumento=24'; break; //Sopro - Outros
                    case 25: $file .= 'instrumento=25'; break; //Ukulelista
                    case 26: $file .= 'instrumento=26'; break; //Bandolinista
                    case 27: $file .= 'instrumento=27'; break; //Berimbalista
                    case 28: $file .= 'instrumento=28'; break; //Violoncelista
                    case 29: $file .= 'instrumento=29'; break; //Cuiqueiro
                }

                //Realiza um switch para definir qual será o estado
                switch ($id_estado='ba')
                {
                    case '': $file .= '&id_estado<>xx'; break; //Todos
                    case 'ba': $file .= '&id_estado=ba'; break; //Bahia
                }

                //Realiza um switch para definir qual será a cidade
                switch ($id_cidade='64')
                {
                    case '': $file .= '&id_cidade<>xx'; break; //Todos
                    case '64': $file .= '&id_cidade=64'; break; //Salvador (Bahia)
                }

                //Realiza um switch para definir alguma coisa não sei o que
                switch ($x='18')
                {
                    case '18': $file .= '&x=18'; break; //????
                }

                //Realiza um switch para definir alguma coisa não sei o que
                switch ($y='7')
                {
                    case '7': $file .= '&y=7'; break; //????
                }

                //Instancia um novo objeto do tipo DOMDocument para o devido tratamento dos caracteres
                $doc = new \DOMDocument('1.0', 'UTF-8');

                //Set error level
                $internalErrors = libxml_use_internal_errors(true);

                //Carrega o arquivo HTML utilizando o método do DOMDocument
                $doc->loadHTMLFile($file);

                //Restore error level
                libxml_use_internal_errors($internalErrors);

                //Instancia o objeto xPath a partir do arquivo HTML carregado
                $xpath = new \DOMXpath($doc);

                //Realiza um switch para definir qual informação será coletada
                switch ($xPathQueryOption=1)
                {
                    case 1: $xPathQuery = '//*[@id="cnt"]/ul/li'; break; //Todos os Músicos
                }

                //Executa a query xPath e armazena o resultado em um Array
                $elements = $xpath->query($xPathQuery);

                //Verifica se a query utilizada retornou elementos
                if (!is_null($elements))
                {
                    //Percorre todos os elementos retornados
                    foreach ($elements as $element)
                    {
                        //Captura todos os childNodes do Elemento
                        $nodes = $element->childNodes;

                        //Percorre todos os childNodes capturados (dentro da tag li / Todos os músicos)
                        foreach ($nodes as $node)
                        {
                            //Armana o valor do node em uma variável (remove espaços em branco)
                            $nodeValue = trim($node->nodeValue);

                            //Verifica se o node capturado retornou algum valor
                            if ($nodeValue != '')
                            {
                                //Percorre todos os childNodes capturados (dentro da tag a / todas as informações do músico)
                                foreach ($node->childNodes as $count=>$node2)
                                {
                                    //Armana o valor do node em uma variável (remove espaços em branco)
                                    $nodeValue2 = trim($node2->nodeValue);

                                    //Verifica se o node capturado retornou algum valor
                                    if ($nodeValue2 != '')
                                    {
                                        //Verifica se o node é do tipo Elemento (Nome, Idade e Tipo de Função)
                                        if (get_class($node2) == 'DOMElement' and $count == 2)
                                        {
                                            //Separa as informações presentes no node (Nome, Idade e Tipo de Função)
                                            $arrayNomeIdadeTipoDeFuncao  = explode(',', $nodeValue2);

                                            //Armazena as informações do músico em variáveis separadas
                                            $musico["nome"] = $arrayNomeIdadeTipoDeFuncao[0];
                                            $musico["idadeAtual"] = $arrayNomeIdadeTipoDeFuncao[1];
                                            $musico["tipoDeFuncao"] = $arrayNomeIdadeTipoDeFuncao[2];
                                        }
                                        //Verifica se o node é do tipo Texto (Endereço ou Gênero/Estilo)
                                        else if (get_class($node2) == 'DOMText')
                                        {
                                            //Verifica o node sem uma lógica exata (Endereço)
                                            if ($count == 3)
                                            {
                                                //Separa as informações presentes no node (Cidade/Estado, Região)
                                                $arrayCidadeEstadoRegiao  = explode('-', $nodeValue2);

                                                //Separa as informações presentes no node (Cidade, Estado)
                                                $arrayCidadeEstado = explode('/', $arrayCidadeEstadoRegiao[0]);

                                                //Armazena as informações do músico em variáveis separadas
                                                $musico["cidade"] = $arrayCidadeEstado[0];
                                                $musico["estado"] = $arrayCidadeEstado[1];
                                                $musico["bairroRegiao"] = $arrayCidadeEstadoRegiao[1];
                                            }
                                            //Verifica o node sem uma lógica exata (Gêneros/Estilos)
                                            else if ($count == 5)
                                            {
                                                //Separa as informações presentes no node (Gêneros/Estilos)
                                                $arrayGeneroEstilo  = explode(',', $nodeValue2);

                                                //Captura a quantidade de estilos do músico
                                                $qtdArrayGeneroEstilo = count($arrayGeneroEstilo);

                                                //Cria/Reseta o array de Gêneros/Estilos para armazenar somente as novas informações
                                                $musico["generoEstilo"] = array();

                                                //Percorre todos os estilos do músico
                                                for ($i = 0; $i < $qtdArrayGeneroEstilo; $i++)
                                                {
                                                    //Armazena as informações do músico em variáveis separadas
                                                    $musico["generoEstilo"][] = $arrayGeneroEstilo[$i];
                                                }
                                            }
                                        }
                                    }
                                }

                                //Armazena o músico coletado em um Array de músicos
                                $musicos[] = $musico;
                            }
                        }
                    }
                }

                //Realiza tratamento para retornar os dados em Json para o JqGrid
                $musicosJson = json_encode($musicos);

                //Armazena a quantidade de registros retornados do banco de dados
                $musicosQtd = count($musicos);

                //Redireciona para a view passando as informações dos registros
                return view('admin.crawlers.fsb', ['estados' => $musicos])
                    ->with('registrosJson', $musicosJson)
                    ->with('registrosQtd', $musicosQtd)
                    ->with('pageName', 'Crawler FSB');

            break;
        }
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('crawlers.create');
    }

    //Método para Importar um Novo Registro para o Banco de Dados (SQL Insert)
    public function importAjax(Request $request)
    {
        //Verifica se existe autorização para a Atualização do Registro
        //$this->authorize('update', $estado);

        //Instancia um objeto da classe músico para realizar a Importação
        $musico = new Musico();

        //Captura os Campos apresentados para Atualização
        $musico->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $musico->save();

        return response()->json(['success' => true, 'message' => 'Successfully updated!']);
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        //$this->validate($request, [
        //    'nome' => 'required|max:255',
        //    'data_de_formacao' => 'required|date_format:"d/m/Y"',
        //    'data_de_termino' => 'date_format:"d/m/Y"|after:data_de_formacao',
        //]);

        //Converte os dados para adição no banco de dados
        //$request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        //$request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        //$request->user()->crawlers()->create([
        //    'nome' => $request->nome,
        //    'data_de_formacao' => $request->data_de_formacao,
        //    'data_de_termino' => $request->data_de_termino,
        //]);

        //Retorna para a tela inicial
        return redirect('/crawlers');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Crawler $crawler)
    {
        //Retorna para a view enviando os dados do Registro
        return view('crawlers.show', [
            'crawler' => $crawler,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Crawler $crawler)
    {
        //Retorna para a view com o Formulário de Edição
        return view('crawlers.edit', ['crawler' => $crawler]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update) via Ajax
    public function updateAjax(Request $request, Crawler $crawler)
    {
        //Verifica se existe autorização para a Atualização do Registro
        //$this->authorize('update', $crawler);

        //Captura os Campos apresentados para Atualização
        $crawler->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $crawler->save();

        return response()->json(['success' => true, 'message' => 'Successfully updated!']);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Crawler $crawler)
    {
        //Verifica se existe autorização para a Atualização do Registro
        $this->authorize('update', $crawler);

        //Captura os Campos apresentados para Atualização
        $crawler->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $crawler->save();

        //
        echo 'cheguei aquiiiiiiiiiiii'; return (0);

        //Retorna para a tela inicial
        return redirect('/crawlers');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Crawler $crawler)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $crawler);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $crawler->delete();

        //Retorna para a tela inicial
        return redirect('/crawlers');
    }
}