<?php
//Definição do Diretório em que o Concorole se Encontra
namespace App\Http\Controllers\Admin;

//Inclusão de Arquivos Necessários Conforme Lógica do Controler
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Estado;
use App\Models\Admin\Cidade;

//Lógica do Controler
class EstadoController extends Controller
{
    //Instância para a classe Repositório
    protected $estados;

    //Método Executado na Construção do Controller
    public function __construct()
    {
        //Realiza a verificação de autenticação
        $this->middleware('auth');
    }

    //Método Inicial do Controler
    public function index(Request $request, $filtro="todos")
    {
        //Apresenta a tela inicial de acordo com o filtro realizado pelo Usuário
        switch ($filtro)
        {
            //Captura todos os registros cadastradas
            case 'todos':  $estados = Estado::all();break;

            //Captura todos os registros cadastrados por um Usuário
            case 'meus': $estados = Estado::forUser($request->user()->id);break;

            //Captura todos os registros cadastrados por outros Usuários
            case 'outros': $estados = Estado::otherUsers($request->user()->id);break;
        }

        //Realiza tratamento para retornar os dados em Json para o JqGrid
        $estadosJson = json_encode($estados);

        //Armazena a quantidade de registros retornados do banco de dados
        $estadosQtd = count($estados);

        //Redireciona para a view passando as informações das estados
        return view('admin.estados.index', ['estados' => $estados])
            ->with('estadosJson', $estadosJson)
            ->with('estadosQtd', $estadosQtd)
            ->with('pageName', 'Estados');
    }

    //Método para Preparar o Cadastro de um Novo Regisro
    public function create(Request $request)
    {
        //Retorna para a view com o Formulário de Cadastro
        return view('estados.create');
    }

    //Método para Armazenar um Novo Registro no Banco de Dados (SQL Insert)
    public function store(Request $request)
    {
        //Realiza o processo de validação dos campos do formulário
        //$this->validate($request, [
        //    'nome' => 'required|max:255',
        //    'data_de_formacao' => 'required|date_format:"d/m/Y"',
        //    'data_de_termino' => 'date_format:"d/m/Y"|after:data_de_formacao',
        //]);

        //Converte os dados para adição no banco de dados
        //$request->data_de_formacao = implode("-",array_reverse(explode("/",$request->data_de_formacao)));
        //$request->data_de_termino = implode("-",array_reverse(explode("/",$request->data_de_termino)));

        //Cadastra o Novo Registro no Banco de Dados
        //$request->user()->estados()->create([
        //    'nome' => $request->nome,
        //    'data_de_formacao' => $request->data_de_formacao,
        //    'data_de_termino' => $request->data_de_termino,
        //]);

        //Retorna para a tela inicial
        return redirect('/estados');
    }

    //Método para Apresentar a Consulta de um Registro
    public function show(Request $request, Estado $estado)
    {
        //Retorna para a view enviando os dados do Registro
        return view('estados.show', [
            'estado' => $estado,
        ]);
    }
    
    //Método para Preparar a Edição de um Registro
    public function edit(Request $request, Estado $estado)
    {
        //Retorna para a view com o Formulário de Edição
        return view('estados.edit', ['estado' => $estado]);
    }

    //Método para Atualizar um Registro no Banco de Dados (SQL Update)
    public function update(Request $request, Estado $estado)
    {
        //Verifica se existe autorização para a Atualização do Registro
        $this->authorize('update', $estado);

        //Captura os Campos apresentados para Atualização
        $estado->nome = $request->nome;

        //Atualiza o Registro no Banco de dados
        $estado->save();

        //
        echo 'cheguei aquiiiiiiiiiiii'; return (0);

        //Retorna para a tela inicial
        return redirect('/estados');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete)
    public function destroy(Request $request, Estado $estado)
    {
        //Verifica se existe autorização para a deleção do Registro
        $this->authorize('destroy', $estado);

        //Deleta o registro no Banco de Dacos (SQL Delete)
        $estado->delete();

        //Retorna para a tela inicial
        return redirect('/estados');
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete) via Ajax
    public function ajax(Request $request, Estado $estado)
    {
        //Verifica a ação enviada por Ajax
        switch ($request->oper)
        {
            //Operação de Adição
            case 'add':

                //Realiza o processo de validação dos campos do formulário
                $this->validate($request, [
                    'nome' => 'required|max:255',
                ]);

                //Cadastra o Novo Registro no Banco de Dados
                $estado->create([
                    'nome' => $request->nome,
                ]);

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro cadastrado com sucesso!']);

            break;

            //Operação de Edição
            case 'edit':

                //Captura o registro a ser atualizado
                $estado = Estado::find($request->id);

                //Captura os Campos apresentados para Atualização
                $estado->nome = $request->nome;

                //Atualiza o Registro no Banco de dados
                $estado->save();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro atualizado com sucesso!']);

            break;

            //Operação de Deleção
            case 'del':

                //Captura o registro a ser deletado
                $estado = Estado::find($request->id);

                //Deleta o registro no Banco de Dacos (SQL Delete)
                $estado->delete();

                //Retorna resposta para a view informando que a atualização foi realizada
                return response()->json(['success' => true, 'message' => 'Registro deletado com sucesso!']);

            break;
        }
    }

    //Método para Deletar um Registro do Banco de Dados (SQL Delete) via Ajax
    public function get_cidades(Request $request, Estado $estado)
    {
        //Captura as Cidades do Estado que foi selecionado
        $cidades = $estado->cidades()->getQuery()->get(['id', 'nome']);

        //Retorna resposta para a view informando que a atualização foi realizada
        return response()->json($cidades);
    }
}