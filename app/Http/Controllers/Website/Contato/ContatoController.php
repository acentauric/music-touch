<?php
//Definição do Diretório em que o Controller se Encontra
namespace App\Http\Controllers\Website\Contato;

//Inclusão de Arquivos Necessários Conforme Lógica do Controller
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contato;

//Lógica do Controller
class ContatoController extends Controller
{
    //Método Inicial
    public function index(Request $request)
    {	
        //Redireciona para a view passando as informações Necessárias
        return view('website/contato/index');        
    }

    //Método para o Envio do Formulário de Contato
    public function enviar(Request $request)
    {
        //Redireciona para a view passando as informações Necessárias
        return view('website/contato/index');
    }
}