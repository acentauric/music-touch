<?php
//Definição do Diretório em que o Controller se Encontra
namespace App\Http\Controllers\Website\Home;

//Inclusão de Arquivos Necessários Conforme Lógica do Controller
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Home;

//Lógica do Controller
class HomeController extends Controller
{
    //Método Inicial
    public function index(Request $request)
    {	
        //Redireciona para a view passando as informações Necessárias
        return view('website/home/index');
    }

    //Método para a realização de Login
    public function login(Request $request)
    {
        //Redireciona para a view passando as informações Necessárias
        return view('website/home/auth/login');
    }

    //Método para Enviar Email para Resetar a Senha do Usuário
    public function email(Request $request)
    {
        //Redireciona para a view passando as informações Necessárias
        return view('website/home/auth/passwords/email');
    }

    //Método para a apreentação da Informações de Quem Somos
    public function quemsomos(Request $request)
    {
        //Redireciona para a view passando as informações Necessárias
        return view('website/home/quem-somos');
    }
}