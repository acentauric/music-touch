<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estilo extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'estilos';

    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'user_id'];
}
