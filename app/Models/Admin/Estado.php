<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'estados';
    
    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'cod_ibge', 'sigla'];

    //Retorna os Músicos associados ao Estado
    public function musico(){return $this->belongsTo('App\Models\Musico');}

    //Retorna as Cidades associadas ao Estado
    public function cidades(){return $this->hasMany('App\Models\Admin\Cidade');}

}
