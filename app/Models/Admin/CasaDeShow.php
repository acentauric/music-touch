<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CasaDeShow extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'casas_de_show';
    
    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'user_id'];
}
