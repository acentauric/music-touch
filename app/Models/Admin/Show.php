<?php

namespace App\Models\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'shows';

    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'user_id'];

    //Retorna as Bandas associadas ao Show
    public function bandas(){return $this->belongsToMany('App\Models\Admin\Banda')->withTimestamps();}
}
