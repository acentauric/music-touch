<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'cidades';

    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'estado_id'];

    //Retorna o Estado associado à Cidade
    public function estado(){return $this->belongsTo('App\Models\Admin\Estado');}

    //Retorna as Bandas associadas à Cidade
    public function bandas(){return $this->hasMany('App\Models\Admin\Banda');}
}
