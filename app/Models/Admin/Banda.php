<?php

namespace App\Models\Admin;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Banda extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'bandas';

    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'user_id', 'data_de_formacao', 'data_de_termino'];

    //Define os campos do tipo data
    protected $dates = ['data_de_formacao', 'data_de_termino'];

    //Retorna a Cidade associada à Banda
    public function cidade(){return $this->belongsTo('App\Models\Admin\Cidade');}

    //Retorna os Shows associados à Banda
    public function shows(){return $this->belongsToMany('App\Models\Admin\Show');}
}
