<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Produtora extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'produtoras';
    
    //Define os campos que podem ser modificados
    protected $fillable = [
        'nome'
        ,'user_id'];
}
