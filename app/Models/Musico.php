<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Musico extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'musicos';
    
    //Define os campos que podem ser modificados
    protected $fillable = [
        'nome'
        ,'user_id'
        ,'data_de_nascimento'
        ,'ano_de_inicio_carreira'
        ,'influências'
        ,'mensagem'
        ,'tipo_de_funcao'
        ,'estado_id'
        ,'logradouro'];

    //Define os campos do tipo data
    protected $dates = ['data_de_nascimento'];

    //Retorna o Estado associado com o Músico
    public function estado(){return $this->hasOne('App\Models\Estado', 'id', 'estado_id');}
}
