<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Banda extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'bandas';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'data_de_formacao', 'data_de_termino'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['data_de_formacao', 'data_de_termino'];

    /**
     * Get the user that owns the band.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function otherUsers($userId){
        return Banda::where('user_id','<>',$userId)->get();
    }
    
    public static function forUser($userId){
        return Banda::where('user_id',$userId)->get();
    }
}
