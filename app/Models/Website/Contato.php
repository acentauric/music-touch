<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = '';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['', '', ''];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['', ''];
}
