<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    //Informando a tabela relacionada no banco de dados
    protected $table = 'instrumentos';

    //Define os campos que podem ser modificados
    protected $fillable = ['nome', 'user_id'];
}
