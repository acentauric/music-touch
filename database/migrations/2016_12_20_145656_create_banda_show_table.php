<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandaShowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banda_show', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();

            $table->integer('banda_id')->unsigned()->nullable();
            $table->foreign('banda_id')->references('id')->on('bandas');

            $table->integer('show_id')->unsigned()->nullable();
            $table->foreign('show_id')->references('id')->on('shows');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banda_show');
    }
}
