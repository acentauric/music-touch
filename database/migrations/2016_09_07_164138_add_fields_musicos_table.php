<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsMusicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('musicos', function (Blueprint $table) {
            $table->date('data_de_nascimento')->nullable()->comment('Data de Nascimento do Músico.');
            $table->integer('ano_de_inicio_carreira')->nullable()->comment('Ano de Início da Carreira como Músico.');
            $table->string('influências')->nullable()->comment('Bandas ou Músicos que tem influência.');
            $table->string('mensagem')->nullable()->comment('Mensagem do Músico para os visitantes.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('musicos', function (Blueprint $table) {
            $table->dropColumn([
                'data_de_nascimento',
                'ano_de_inicio_carreira',
                'influências',
                'mensagem']);
        });
    }
}
