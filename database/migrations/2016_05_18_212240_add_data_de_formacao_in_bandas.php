<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataDeFormacaoInBandas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->date('data_de_formacao');
            $table->date('data_de_termino');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->dropColumn(['data_de_formacao', 'data_de_termino']);
        });
    }
}
