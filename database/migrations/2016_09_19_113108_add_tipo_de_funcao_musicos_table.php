<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoDeFuncaoMusicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('musicos', function (Blueprint $table) {
            $table->integer('tipo_de_funcao')->nullable()->comment('Define o Tipo de Função do Músico.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('musicos', function (Blueprint $table) {
            $table->dropColumn(['tipo_de_funcao']);
        });
    }
}
