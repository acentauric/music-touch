<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogradouroMusicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('musicos', function (Blueprint $table) {            
            $table->string('logradouro')->nullable()->comment('Define qual o Logradouro de registro do Endereço do Musico.');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('musicos', function (Blueprint $table) {
            $table->dropColumn([
                'logradouro',
            ]);
        });
    }
}
