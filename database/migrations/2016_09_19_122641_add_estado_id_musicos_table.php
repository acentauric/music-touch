<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadoIdMusicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('musicos', function (Blueprint $table) {
            $table->integer('estado_id')->nullable()->comment('Define qual o Estado de Residência do Músico.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('musicos', function (Blueprint $table) {
            $table->dropColumn(['estado_id']);
        });
    }
}
