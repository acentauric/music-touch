<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContatosBandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->char('telefone_fixo', 14)->nullable()->comment('Define qual o Número do Telefone Fixo da Banda.');
            $table->char('telefone_celular', 15)->nullable()->comment('Define qual o Número do Telefone Celular da Banda.');
            $table->string('email', 100)->nullable()->comment('Define qual o Email da Banda.');
            $table->string('site', 100)->nullable()->comment('Define qual o Site Oficial da Banda.');
            $table->string('facebook', 100)->nullable()->comment('Define qual o Facebook da Banda.');
            $table->string('youtube', 100)->nullable()->comment('Define qual o Youtube da Banda.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->dropColumn([
                'telefone_fixo',
                'telefone_celular',
                'email',
                'site',
                'facebook',
                'youtube',
            ]);
        });
    }
}
