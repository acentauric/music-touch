<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estados', function (Blueprint $table) {
            $table->integer('cod_ibge')->comment('Código disponível no site do IBGE.');
            $table->char('sigla', 2)->comment('Sigal de 2 caracteres conforme site do IBGE.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estados', function (Blueprint $table) {
            $table->dropColumn(['cod_ibge', 'sigla']);
        });
    }
}
