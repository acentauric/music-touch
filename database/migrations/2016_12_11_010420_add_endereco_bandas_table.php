<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnderecoBandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->integer('cidade_id')->unsigned()->nullable()->comment('Define qual a Cidade de registro do Endereço da Banda.');
            $table->foreign('cidade_id')->references('id')->on('cidades');
            $table->string('logradouro')->nullable()->comment('Define qual o Logradouro de registro do Endereço da Banda.');
            $table->string('numero', 50)->nullable()->comment('Define qual o Número do Logradouro de registro do Endereço da Banda.');
            $table->char('cep', 10)->nullable()->comment('Define qual CEP de registro do Endereço da Banda.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bandas', function (Blueprint $table) {
            $table->dropColumn([
                'cidade_id',
                'logradouro',
                'numero',
                'cep',
            ]);
        });
    }
}
