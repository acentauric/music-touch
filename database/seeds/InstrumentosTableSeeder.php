<?php

use Illuminate\Database\Seeder;
use App\Models\Instrumento;

class InstrumentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Limpa todos os dados da tabela no banco de dados
        Instrumento::truncate();

        //Desabilita o Log para evitar o uso desnecessário de memória
        DB::disableQueryLog();

        //Armazena as informações no banco de dados
        Instrumento::create(["nome" => "Bandolim"]);
        Instrumento::create(["nome" => "Banjo"]);
        Instrumento::create(["nome" => "Bateria"]);
        Instrumento::create(["nome" => "Berimbau"]);
        Instrumento::create(["nome" => "Cavaquinho"]);
        Instrumento::create(["nome" => "Contra-Baixo"]);
        Instrumento::create(["nome" => "Cuíca"]);
        Instrumento::create(["nome" => "Flauta"]);
        Instrumento::create(["nome" => "Gaita"]);
        Instrumento::create(["nome" => "Guitarra Base"]);
        Instrumento::create(["nome" => "Guitarra Solo"]);
        Instrumento::create(["nome" => "Harpa"]);
        Instrumento::create(["nome" => "Pandeiro"]);
        Instrumento::create(["nome" => "Percussão"]);
        Instrumento::create(["nome" => "Piano"]);
        Instrumento::create(["nome" => "Pickup"]);
        Instrumento::create(["nome" => "Sanfona"]);
        Instrumento::create(["nome" => "Sax"]);
        Instrumento::create(["nome" => "Teclado"]);
        Instrumento::create(["nome" => "Triângulo"]);
        Instrumento::create(["nome" => "Trombone"]);
        Instrumento::create(["nome" => "Trompete"]);
        Instrumento::create(["nome" => "Ukelele"]);
        Instrumento::create(["nome" => "Viola"]);
        Instrumento::create(["nome" => "Violão"]);
        Instrumento::create(["nome" => "Violino"]);
        Instrumento::create(["nome" => "Violoncelo"]);
        Instrumento::create(["nome" => "Zabumba"]);
    }
}
