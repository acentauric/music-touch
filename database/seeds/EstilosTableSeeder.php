<?php

use Illuminate\Database\Seeder;
use App\Models\Estilo;

class EstilosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Limpa todos os dados da tabela no banco de dados
        Estilo::truncate();

        //Desabilita o Log para evitar o uso desnecessário de memória
        DB::disableQueryLog();

        //Armazena as informações no banco de dados
        Estilo::create(["nome" => "Axé"]);
        Estilo::create(["nome" => "Black Music"]);
        Estilo::create(["nome" => "Blues"]);
        Estilo::create(["nome" => "Bossa Nova"]);
        Estilo::create(["nome" => "Chillout"]);
        Estilo::create(["nome" => "Classic Rock"]);
        Estilo::create(["nome" => "Clássico"]);
        Estilo::create(["nome" => "Country"]);
        Estilo::create(["nome" => "Dance"]);
        Estilo::create(["nome" => "Disco"]);
        Estilo::create(["nome" => "Electro Swing"]);
        Estilo::create(["nome" => "Eletrônica"]);
        Estilo::create(["nome" => "Emocore"]);
        Estilo::create(["nome" => "Fado"]);
        Estilo::create(["nome" => "Folk"]);
        Estilo::create(["nome" => "Forró"]);
        Estilo::create(["nome" => "Funk"]);
        Estilo::create(["nome" => "Funk Carioca"]);
        Estilo::create(["nome" => "Gospel/Religioso"]);
        Estilo::create(["nome" => "Gótico"]);
        Estilo::create(["nome" => "Grunge"]);
        Estilo::create(["nome" => "Hard Rock"]);
        Estilo::create(["nome" => "Hard Core"]);
        Estilo::create(["nome" => "Heavy Metal"]);
        Estilo::create(["nome" => "Hip Hop"]);
        Estilo::create(["nome" => "House"]);
        Estilo::create(["nome" => "Indie"]);
        Estilo::create(["nome" => "Industrial"]);
        Estilo::create(["nome" => "Infantil"]);
        Estilo::create(["nome" => "Instrumental"]);
        Estilo::create(["nome" => "J-Pop/J-Rock"]);
        Estilo::create(["nome" => "Jazz"]);
        Estilo::create(["nome" => "K-Pop/K-Rock"]);
        Estilo::create(["nome" => "Kizomba"]);
        Estilo::create(["nome" => "Metal"]);
        Estilo::create(["nome" => "MPB"]);
        Estilo::create(["nome" => "Músicas Gaúchas"]);
        Estilo::create(["nome" => "New Age"]);
        Estilo::create(["nome" => "New Wave"]);
        Estilo::create(["nome" => "Pagode"]);
        Estilo::create(["nome" => "Piano Rock"]);
        Estilo::create(["nome" => "Pop"]);
        Estilo::create(["nome" => "Pop/Punk"]);
        Estilo::create(["nome" => "Pop/Rock"]);
        Estilo::create(["nome" => "Pós-Punk"]);
        Estilo::create(["nome" => "Post-Rock"]);
        Estilo::create(["nome" => "Power-Pop"]);
        Estilo::create(["nome" => "Progressivo"]);
        Estilo::create(["nome" => "Psicodelia"]);
        Estilo::create(["nome" => "Punk Rock"]);
        Estilo::create(["nome" => "R&B"]);
        Estilo::create(["nome" => "Rap"]);
        Estilo::create(["nome" => "Reggae"]);
        Estilo::create(["nome" => "Reggaeton"]);
        Estilo::create(["nome" => "Regional"]);
        Estilo::create(["nome" => "Rock"]);
        Estilo::create(["nome" => "Rock Alternativo"]);
        Estilo::create(["nome" => "Rockabilly"]);
        Estilo::create(["nome" => "Romântico"]);
        Estilo::create(["nome" => "Samba"]);
        Estilo::create(["nome" => "Samba Enredo"]);
        Estilo::create(["nome" => "Sertanejo"]);
        Estilo::create(["nome" => "Ska"]);
        Estilo::create(["nome" => "Soft Rock"]);
        Estilo::create(["nome" => "Soul Music"]);
        Estilo::create(["nome" => "Surf Music"]);
        Estilo::create(["nome" => "Tecnopop"]);
        Estilo::create(["nome" => "Trance"]);
        Estilo::create(["nome" => "Trilha Sonora"]);
        Estilo::create(["nome" => "Trip-Hop"]);
        Estilo::create(["nome" => "Tropical House"]);
        Estilo::create(["nome" => "Velha Guarda"]);
        Estilo::create(["nome" => "World Music"]);
    }
}
