<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesAcreSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Acrelândia', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Assis Brasil', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Brasiléia', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Bujari', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Capixaba', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Cruzeiro do Sul', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Epitaciolândia', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Feijó', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Jordão', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Mâncio Lima', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Manoel Urbano', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Marechal Thaumaturgo', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Plácido de Castro', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Porto Acre', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Porto Walter', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Rio Branco', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Rodrigues Alves', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Santa Rosa do Purus', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Sena Madureira', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Senador Guiomard', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Tarauacá', 'estado_id' => 1]);
        Cidade::create(['nome' => 'Xapuri', 'estado_id' => 1]);
    }
}