<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesSergipeSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Amparo de São Francisco', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Aquidabã', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Aracaju', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Arauá', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Areia Branca', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Barra dos Coqueiros', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Boquim', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Brejo Grande', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Campo do Brito', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Canhoba', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Canindé de São Francisco', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Capela', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Carira', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Carmópolis', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Cedro de São João', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Cristinápolis', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Cumbe', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Divina Pastora', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Estância', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Feira Nova', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Frei Paulo', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Gararu', 'estado_id' => 26]);
        Cidade::create(['nome' => 'General Maynard', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Gracho Cardoso', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Ilha das Flores', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Indiaroba', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Itabaiana', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Itabaianinha', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Itabi', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Itaporanga d`Ajuda', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Japaratuba', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Japoatã', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Lagarto', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Laranjeiras', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Macambira', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Malhada dos Bois', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Malhador', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Maruim', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Moita Bonita', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Monte Alegre de Sergipe', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Muribeca', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Neópolis', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Nossa Senhora Aparecida', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Nossa Senhora da Glória', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Nossa Senhora das Dores', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Nossa Senhora de Lourdes', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Nossa Senhora do Socorro', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Pacatuba', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Pedra Mole', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Pedrinhas', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Pinhão', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Pirambu', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Poço Redondo', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Poço Verde', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Porto da Folha', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Propriá', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Riachão do Dantas', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Riachuelo', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Ribeirópolis', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Rosário do Catete', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Salgado', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Santa Luzia do Itanhy', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Santa Rosa de Lima', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Santana do São Francisco', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Santo Amaro das Brotas', 'estado_id' => 26]);
        Cidade::create(['nome' => 'São Cristóvão', 'estado_id' => 26]);
        Cidade::create(['nome' => 'São Domingos', 'estado_id' => 26]);
        Cidade::create(['nome' => 'São Francisco', 'estado_id' => 26]);
        Cidade::create(['nome' => 'São Miguel do Aleixo', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Simão Dias', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Siriri', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Telha', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Tobias Barreto', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Tomar do Geru', 'estado_id' => 26]);
        Cidade::create(['nome' => 'Umbaúba', 'estado_id' => 26]);
    }
}