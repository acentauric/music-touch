<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesRoraimaSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Alto Alegre', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Amajari', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Boa Vista', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Bonfim', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Cantá', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Caracaraí', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Caroebe', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Iracema', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Mucajaí', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Normandia', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Pacaraima', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Rorainópolis', 'estado_id' => 23]);
        Cidade::create(['nome' => 'São João da Baliza', 'estado_id' => 23]);
        Cidade::create(['nome' => 'São Luiz', 'estado_id' => 23]);
        Cidade::create(['nome' => 'Uiramutã', 'estado_id' => 23]);
    }
}