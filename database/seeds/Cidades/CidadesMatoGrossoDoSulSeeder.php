<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesMatoGrossoDoSulSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Água Clara', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Alcinópolis', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Amambaí', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Anastácio', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Anaurilândia', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Angélica', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Antônio João', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Aparecida do Taboado', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Aquidauana', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Aral Moreira', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bandeirantes', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bataguassu', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bataiporã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bela Vista', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bodoquena', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Bonito', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Brasilândia', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Caarapó', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Camapuã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Campo Grande', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Caracol', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Cassilândia', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Chapadão do Sul', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Corguinho', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Coronel Sapucaia', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Corumbá', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Costa Rica', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Coxim', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Deodápolis', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Dois Irmãos do Buriti', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Douradina', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Dourados', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Eldorado', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Fátima do Sul', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Figueirão', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Glória de Dourados', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Guia Lopes da Laguna', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Iguatemi', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Inocência', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Itaporã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Itaquiraí', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Ivinhema', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Japorã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Jaraguari', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Jardim', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Jateí', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Juti', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Ladário', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Laguna Carapã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Maracaju', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Miranda', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Mundo Novo', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Naviraí', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Nioaque', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Nova Alvorada do Sul', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Nova Andradina', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Novo Horizonte do Sul', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Paranaíba', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Paranhos', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Pedro Gomes', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Ponta Porã', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Porto Murtinho', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Ribas do Rio Pardo', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Rio Brilhante', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Rio Negro', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Rio Verde de Mato Grosso', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Rochedo', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Santa Rita do Pardo', 'estado_id' => 12]);
        Cidade::create(['nome' => 'São Gabriel do Oeste', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Selvíria', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Sete Quedas', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Sidrolândia', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Sonora', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Tacuru', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Taquarussu', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Terenos', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Três Lagoas', 'estado_id' => 12]);
        Cidade::create(['nome' => 'Vicentina', 'estado_id' => 12]);
    }
}