<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesMatoGrossoSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Acorizal', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Água Boa', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alta Floresta', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alto Araguaia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alto Boa Vista', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alto Garças', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alto Paraguai', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Alto Taquari', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Apiacás', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Araguaiana', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Araguainha', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Araputanga', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Arenápolis', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Aripuanã', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Barão de Melgaço', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Barra do Bugres', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Barra do Garças', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Bom Jesus do Araguaia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Brasnorte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Cáceres', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Campinápolis', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Campo Novo do Parecis', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Campo Verde', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Campos de Júlio', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Canabrava do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Canarana', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Carlinda', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Castanheira', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Chapada dos Guimarães', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Cláudia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Cocalinho', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Colíder', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Colniza', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Comodoro', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Confresa', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Conquista d`Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Cotriguaçu', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Cuiabá', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Curvelândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Curvelândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Denise', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Diamantino', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Dom Aquino', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Feliz Natal', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Figueirópolis d`Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Gaúcha do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'General Carneiro', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Glória d`Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Guarantã do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Guiratinga', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Indiavaí', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Ipiranga do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Itanhangá', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Itaúba', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Itiquira', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Jaciara', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Jangada', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Jauru', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Juara', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Juína', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Juruena', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Juscimeira', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Lambari d`Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Lucas do Rio Verde', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Luciára', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Marcelândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Matupá', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Mirassol d`Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nobres', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nortelândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nossa Senhora do Livramento', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Bandeirantes', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Brasilândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Canaã do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Guarita', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Lacerda', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Marilândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Maringá', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Monte verde', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Mutum', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Olímpia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Santa Helena', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Ubiratã', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Nova Xavantina', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Novo Horizonte do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Novo Mundo', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Novo Santo Antônio', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Novo São Joaquim', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Paranaíta', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Paranatinga', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Pedra Preta', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Peixoto de Azevedo', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Planalto da Serra', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Poconé', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Pontal do Araguaia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Ponte Branca', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Pontes e Lacerda', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Porto Alegre do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Porto dos Gaúchos', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Porto Esperidião', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Porto Estrela', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Poxoréo', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Primavera do Leste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Querência', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Reserva do Cabaçal', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Ribeirão Cascalheira', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Ribeirãozinho', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Rio Branco', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Rondolândia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Rondonópolis', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Rosário Oeste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Salto do Céu', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santa Carmem', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santa Cruz do Xingu', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santa Rita do Trivelato', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santa Terezinha', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santo Afonso', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santo Antônio do Leste', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Santo Antônio do Leverger', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São Félix do Araguaia', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São José do Povo', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São José do Rio Claro', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São José do Xingu', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São José dos Quatro Marcos', 'estado_id' => 11]);
        Cidade::create(['nome' => 'São Pedro da Cipa', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Sapezal', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Serra Nova Dourada', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Sinop', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Sorriso', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Tabaporã', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Tangará da Serra', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Tapurah', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Terra Nova do Norte', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Tesouro', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Torixoréu', 'estado_id' => 11]);
        Cidade::create(['nome' => 'União do Sul', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Vale de São Domingos', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Várzea Grande', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Vera', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Vila Bela da Santíssima Trindade', 'estado_id' => 11]);
        Cidade::create(['nome' => 'Vila Rica', 'estado_id' => 11]);
    }
}