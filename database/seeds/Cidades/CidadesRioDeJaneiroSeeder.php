<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesRioDeJaneiroSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Angra dos Reis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Aperibé', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Araruama', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Areal', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Armação dos Búzios', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Arraial do Cabo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Barra do Piraí', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Barra Mansa', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Belford Roxo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Bom Jardim', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Bom Jesus do Itabapoana', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cabo Frio', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cachoeiras de Macacu', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cambuci', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Campos dos Goytacazes', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cantagalo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Carapebus', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cardoso Moreira', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Carmo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Casimiro de Abreu', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Comendador Levy Gasparian', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Conceição de Macabu', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Cordeiro', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Duas Barras', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Duque de Caxias', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Engenheiro Paulo de Frontin', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Guapimirim', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Iguaba Grande', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Itaboraí', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Itaguaí', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Italva', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Itaocara', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Itaperuna', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Itatiaia', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Japeri', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Laje do Muriaé', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Macaé', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Macuco', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Magé', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Mangaratiba', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Maricá', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Mendes', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Mesquita', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Miguel Pereira', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Miracema', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Natividade', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Nilópolis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Niterói', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Nova Friburgo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Nova Iguaçu', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Paracambi', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Paraíba do Sul', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Parati', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Paty do Alferes', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Petrópolis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Pinheiral', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Piraí', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Porciúncula', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Porto Real', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Quatis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Queimados', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Quissamã', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Resende', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Rio Bonito', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Rio Claro', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Rio das Flores', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Rio das Ostras', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Rio de Janeiro', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Santa Maria Madalena', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Santo Antônio de Pádua', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São Fidélis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São Francisco de Itabapoana', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São Gonçalo', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São João da Barra', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São João de Meriti', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São José de Ubá', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São José do Vale do Rio Pret', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São Pedro da Aldeia', 'estado_id' => 19]);
        Cidade::create(['nome' => 'São Sebastião do Alto', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Sapucaia', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Saquarema', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Seropédica', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Silva Jardim', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Sumidouro', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Tanguá', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Teresópolis', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Trajano de Morais', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Três Rios', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Valença', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Varre-Sai', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Vassouras', 'estado_id' => 19]);
        Cidade::create(['nome' => 'Volta Redonda', 'estado_id' => 19]);
    }
}