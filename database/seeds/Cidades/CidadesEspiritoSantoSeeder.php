<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesEspiritoSantoSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Afonso Cláudio', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Água Doce do Norte', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Águia Branca', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Alegre', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Alfredo Chaves', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Alto Rio Novo', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Anchieta', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Apiacá', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Aracruz', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Atilio Vivacqua', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Baixo Guandu', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Barra de São Francisco', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Boa Esperança', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Bom Jesus do Norte', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Brejetuba', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Cachoeiro de Itapemirim', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Cariacica', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Castelo', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Colatina', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Conceição da Barra', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Conceição do Castelo', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Divino de São Lourenço', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Domingos Martins', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Dores do Rio Preto', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Ecoporanga', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Fundão', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Governador Lindenberg', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Guaçuí', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Guarapari', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Ibatiba', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Ibiraçu', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Ibitirama', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Iconha', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Irupi', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Itaguaçu', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Itapemirim', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Itarana', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Iúna', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Jaguaré', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Jerônimo Monteiro', 'estado_id' => 8]);
        Cidade::create(['nome' => 'João Neiva', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Laranja da Terra', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Linhares', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Mantenópolis', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Marataízes', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Marechal Floriano', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Marilândia', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Mimoso do Sul', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Montanha', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Mucurici', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Muniz Freire', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Muqui', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Nova Venécia', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Pancas', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Pedro Canário', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Pinheiros', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Piúma', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Ponto Belo', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Presidente Kennedy', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Rio Bananal', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Rio Novo do Sul', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Santa Leopoldina', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Santa Maria de Jetibá', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Santa Teresa', 'estado_id' => 8]);
        Cidade::create(['nome' => 'São Domingos do Norte', 'estado_id' => 8]);
        Cidade::create(['nome' => 'São Gabriel da Palha', 'estado_id' => 8]);
        Cidade::create(['nome' => 'São José do Calçado', 'estado_id' => 8]);
        Cidade::create(['nome' => 'São Mateus', 'estado_id' => 8]);
        Cidade::create(['nome' => 'São Roque do Canaã', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Serra', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Sooretama', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Vargem Alta', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Venda Nova do Imigrante', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Viana', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Vila Pavão', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Vila Valério', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Vila Velha', 'estado_id' => 8]);
        Cidade::create(['nome' => 'Vitória', 'estado_id' => 8]);
    }
}