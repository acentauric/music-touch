<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesRondoniaSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Alta Floresta d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Alto Alegre dos Parecis', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Alto Paraíso', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Alvorada d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Ariquemes', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Buritis', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Cabixi', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Cacaulândia', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Cacoal', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Campo Novo de Rondônia', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Candeias do Jamari', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Castanheiras', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Cerejeiras', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Chupinguaia', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Colorado do Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Corumbiara', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Costa Marques', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Cujubim', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Espigão d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Governador Jorge Teixeira', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Guajará-Mirim', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Itapuã do Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Jaru', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Ji-Paraná', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Machadinho d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Ministro Andreazza', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Mirante da Serra', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Monte Negro', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Nova Brasilândia d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Nova Mamoré', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Nova União', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Novo Horizonte do Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Ouro Preto do Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Parecis', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Pimenta Bueno', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Pimenteiras do Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Porto Velho', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Presidente Médici', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Primavera de Rondônia', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Rio Crespo', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Rolim de Moura', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Santa Luzia d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'São Felipe d`Oeste', 'estado_id' => 22]);
        Cidade::create(['nome' => 'São Francisco do Guaporé', 'estado_id' => 22]);
        Cidade::create(['nome' => 'São Miguel do Guaporé', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Seringueiras', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Teixeirópolis', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Theobroma', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Urupá', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Vale do Anari', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Vale do Paraíso', 'estado_id' => 22]);
        Cidade::create(['nome' => 'Vilhena', 'estado_id' => 22]);
    }
}