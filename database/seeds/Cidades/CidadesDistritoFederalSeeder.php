<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesDistritoFederalSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Brasília', 'estado_id' => 7]);
    }
}