<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesTocantinsSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Abreulândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Aguiarnópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Aliança do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Almas', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Alvorada', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Ananás', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Angico', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Aparecida do Rio Negro', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Aragominas', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Araguacema', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Araguaçu', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Araguaína', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Araguanã', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Araguatins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Arapoema', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Arraias', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Augustinópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Aurora do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Axixá do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Babaçulândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Bandeirantes do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Barra do Ouro', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Barrolândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Bernardo Sayão', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Bom Jesus do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Brasilândia do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Brejinho de Nazaré', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Buriti do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Cachoeirinha', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Campos Lindos', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Cariri do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Carmolândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Carrasco Bonito', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Caseara', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Centenário', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Chapada da Natividade', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Chapada de Areia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Colinas do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Colméia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Combinado', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Conceição do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Couto de Magalhães', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Cristalândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Crixás do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Darcinópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Dianópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Divinópolis do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Dois Irmãos do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Dueré', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Esperantina', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Fátima', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Figueirópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Filadélfia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Formoso do Araguaia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Fortaleza do Tabocão', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Goianorte', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Goiatins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Guaraí', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Gurupi', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Ipueiras', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Itacajá', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Itaguatins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Itapiratins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Itaporã do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Jaú do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Juarina', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Lagoa da Confusão', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Lagoa do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Lajeado', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Lavandeira', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Lizarda', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Luzinópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Marianópolis do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Mateiros', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Maurilândia do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Miracema do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Miranorte', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Monte do Carmo', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Monte Santo do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Muricilândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Natividade', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Nazaré', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Nova Olinda', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Nova Rosalândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Novo Acordo', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Novo Alegre', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Novo Jardim', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Oliveira de Fátima', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Palmas', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Palmeirante', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Palmeiras do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Palmeirópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Paraíso do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Paranã', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pau d`Arco', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pedro Afonso', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Peixe', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pequizeiro', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pindorama do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Piraquê', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pium', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Ponte Alta do Bom Jesus', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Ponte Alta do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Porto Alegre do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Porto Nacional', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Praia Norte', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Presidente Kennedy', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Pugmil', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Recursolândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Riachinho', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Rio da Conceição', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Rio dos Bois', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Rio Sono', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Sampaio', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Sandolândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Fé do Araguaia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Maria do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Rita do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Rosa do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Tereza do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Santa Terezinha do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Bento do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Félix do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Miguel do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Salvador do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Sebastião do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'São Valério da Natividade', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Silvanópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Sítio Novo do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Sucupira', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Taguatinga', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Taipas do Tocantins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Talismã', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Tocantínia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Tocantinópolis', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Tupirama', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Tupiratins', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Wanderlândia', 'estado_id' => 27]);
        Cidade::create(['nome' => 'Xambioá', 'estado_id' => 27]);
    }
}