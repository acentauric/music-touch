<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesAmapaSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
		//Armazena os registros no banco de dados		
		Cidade::create(['nome' => 'Amapá', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Calçoene', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Cutias', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Ferreira Gomes', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Itaubal', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Laranjal do Jari', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Macapá', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Mazagão', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Oiapoque', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Pedra Branca do Amaparí', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Porto Grande', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Pracuúba', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Santana', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Serra do Navio', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Tartarugalzinho', 'estado_id' => 3]);
		Cidade::create(['nome' => 'Vitória do Jari', 'estado_id' => 3]);
    }
}