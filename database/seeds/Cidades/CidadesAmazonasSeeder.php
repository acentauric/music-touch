<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesAmazonasSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados
        Cidade::create(['nome' => 'Alvarães', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Amaturá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Anamã', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Anori', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Apuí', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Atalaia do Norte', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Autazes', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Barcelos', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Barreirinha', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Benjamin Constant', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Beruri', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Boa Vista do Ramos', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Boca do Acre', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Borba', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Caapiranga', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Canutama', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Carauari', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Careiro', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Careiro da Várzea', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Coari', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Codajás', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Eirunepé', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Envira', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Fonte Boa', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Guajará', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Humaitá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Ipixuna', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Iranduba', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Itacoatiara', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Itamarati', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Itapiranga', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Japurá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Juruá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Jutaí', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Lábrea', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Manacapuru', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Manaquiri', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Manaus', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Manicoré', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Maraã', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Maués', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Nhamundá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Nova Olinda do Norte', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Novo Airão', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Novo Aripuanã', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Parintins', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Pauini', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Presidente Figueiredo', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Rio Preto da Eva', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Santa Isabel do Rio Negro', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Santo Antônio do Içá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'São Gabriel da Cachoeira', 'estado_id' => 4]);
        Cidade::create(['nome' => 'São Paulo de Olivença', 'estado_id' => 4]);
        Cidade::create(['nome' => 'São Sebastião do Uatumã', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Silves', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Tabatinga', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Tapauá', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Tefé', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Tonantins', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Uarini', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Urucará', 'estado_id' => 4]);
        Cidade::create(['nome' => 'Urucurituba', 'estado_id' => 4]);
    }
}