<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Cidade;

class CidadesAlagoasSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Armazena os registros no banco de dados        
        Cidade::create(['nome' => 'Água Branca', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Anadia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Arapiraca', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Atalaia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Barra de Santo Antônio', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Barra de São Miguel', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Batalha', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Belém', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Belo Monte', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Boca da Mata', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Branquinha', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Cacimbinhas', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Cajueiro', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Campestre', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Campo Alegre', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Campo Grande', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Canapi', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Capela', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Carneiros', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Chã Preta', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Coité do Nóia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Colônia Leopoldina', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Coqueiro Seco', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Coruripe', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Craíbas', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Delmiro Gouveia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Dois Riachos', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Estrela de Alagoas', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Feira Grande', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Feliz Deserto', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Flexeiras', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Girau do Ponciano', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Ibateguara', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Igaci', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Igreja Nova', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Inhapi', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Jacaré dos Homens', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Jacuípe', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Japaratinga', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Jaramataia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Jequiá da Praia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Joaquim Gomes', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Jundiá', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Junqueiro', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Lagoa da Canoa', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Limoeiro de Anadia', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Maceió', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Major Isidoro', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Mar Vermelho', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Maragogi', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Maravilha', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Marechal Deodoro', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Maribondo', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Mata Grande', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Matriz de Camaragibe', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Messias', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Minador do Negrão', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Monteirópolis', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Murici', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Novo Lino', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Olho d`Água das Flores', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Olho d`Água do Casado', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Olho d`Água Grande', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Olivença', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Ouro Branco', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Palestina', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Palmeira dos Índios', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Pão de Açúcar', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Pariconha', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Paripueira', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Passo de Camaragibe', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Paulo Jacinto', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Penedo', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Piaçabuçu', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Pilar', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Pindoba', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Piranhas', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Poço das Trincheiras', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Porto Calvo', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Porto de Pedras', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Porto Real do Colégio', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Quebrangulo', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Rio Largo', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Roteiro', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Santa Luzia do Norte', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Santana do Ipanema', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Santana do Mundaú', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São Brás', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São José da Laje', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São José da Tapera', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São Luís do Quitunde', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São Miguel dos Campos', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São Miguel dos Milagres', 'estado_id' => 2]);
        Cidade::create(['nome' => 'São Sebastião', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Satuba', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Senador Rui Palmeira', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Tanque d`Arca', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Taquarana', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Teotônio Vilela', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Traipu', 'estado_id' => 2]);
        Cidade::create(['nome' => 'União dos Palmares', 'estado_id' => 2]);
        Cidade::create(['nome' => 'Viçosa', 'estado_id' => 2]);
    }
}