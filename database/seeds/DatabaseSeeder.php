<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Executa todos os Seeds do Sistema
        $this->call(EstadosTableSeeder::class);
        $this->call(CidadesTableSeeder::class);
        $this->call(EstilosTableSeeder::class);
        $this->call(InstrumentosTableSeeder::class);
    }
}
