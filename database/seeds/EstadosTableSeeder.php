<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Estado;
use App\Models\Admin\Cidade;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Desabilita a checagem de constantes para permitir a deleção dos Estados
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                
        //Limpa todos os dados da tabela no banco de dados
        Estado::truncate();

        //Reabilita a deleção de verificação de chaves estrangeiras após a deleção dos Estados
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        //Desabilita o Log para evitar o uso desnecessário de memória
        DB::disableQueryLog();

        //Armazena os registros no banco de dados
        Estado::create(["id" => "1","cod_ibge" => "12","sigla" => "AC","nome" => "Acre"]);
        Estado::create(["id" => "2","cod_ibge" => "27","sigla" => "AL","nome" => "Alagoas"]);
        Estado::create(["id" => "3","cod_ibge" => "16","sigla" => "AP","nome" => "Amapá"]);
        Estado::create(["id" => "4","cod_ibge" => "13","sigla" => "AM","nome" => "Amazonas"]);
        Estado::create(["id" => "5","cod_ibge" => "29","sigla" => "BA","nome" => "Bahia"]);
        Estado::create(["id" => "6","cod_ibge" => "23","sigla" => "CE","nome" => "Ceará"]);
        Estado::create(["id" => "7","cod_ibge" => "53","sigla" => "DF","nome" => "Distrito Federal"]);
        Estado::create(["id" => "8","cod_ibge" => "32","sigla" => "ES","nome" => "Espírito Santo"]);
        Estado::create(["id" => "9","cod_ibge" => "52","sigla" => "GO","nome" => "Goiás"]);
        Estado::create(["id" => "10","cod_ibge" => "21","sigla" => "MA","nome" => "Maranhão"]);
        Estado::create(["id" => "11","cod_ibge" => "51","sigla" => "MT","nome" => "Mato Grosso"]);
        Estado::create(["id" => "12","cod_ibge" => "50","sigla" => "MS","nome" => "Mato Grosso do Sul"]);
        Estado::create(["id" => "13","cod_ibge" => "31","sigla" => "MG","nome" => "Minas Gerais"]);
        Estado::create(["id" => "14","cod_ibge" => "15","sigla" => "PA","nome" => "Pará"]);
        Estado::create(["id" => "15","cod_ibge" => "25","sigla" => "PB","nome" => "Paraíba"]);
        Estado::create(["id" => "16","cod_ibge" => "41","sigla" => "PR","nome" => "Paraná"]);
        Estado::create(["id" => "17","cod_ibge" => "26","sigla" => "PE","nome" => "Pernambuco"]);
        Estado::create(["id" => "18","cod_ibge" => "22","sigla" => "PI","nome" => "Piauí"]);
        Estado::create(["id" => "19","cod_ibge" => "33","sigla" => "RJ","nome" => "Rio de Janeiro"]);
        Estado::create(["id" => "20","cod_ibge" => "24","sigla" => "RN","nome" => "Rio Grande do Norte"]);
        Estado::create(["id" => "21","cod_ibge" => "43","sigla" => "RS","nome" => "Rio Grande do Sul"]);
        Estado::create(["id" => "22","cod_ibge" => "11","sigla" => "RO","nome" => "Rondônia"]);
        Estado::create(["id" => "23","cod_ibge" => "14","sigla" => "RR","nome" => "Roraima"]);
        Estado::create(["id" => "24","cod_ibge" => "42","sigla" => "SC","nome" => "Santa Catarina"]);
        Estado::create(["id" => "25","cod_ibge" => "35","sigla" => "SP","nome" => "São Paulo"]);
        Estado::create(["id" => "26","cod_ibge" => "28","sigla" => "SE","nome" => "Sergipe"]);
        Estado::create(["id" => "27","cod_ibge" => "17","sigla" => "TO","nome" => "Tocantins"]);

    }
}
