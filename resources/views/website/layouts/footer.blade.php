@section('footer')
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img class="footer-logo" src="{{ URL::asset('website/assets/images/logo_dark.png') }}" alt="" />
                    <address>
                        <ul class="list-unstyled">
                            <li class="footer-sprite address">
                                Incubadora UNIFACS, Sala 04<br>
                                Rua Vieira Lopes, Rio Vermelho<br>
                                Salvador/BA, Brasil<br>
                            </li>
                            <li class="footer-sprite phone">
                                Telefone: 055 71993699538
                            </li>
                            <li class="footer-sprite email">
                                <a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a>
                            </li>
                        </ul>
                    </address>
                </div>
                <div class="col-md-2">
                    <h4 class="letter-spacing-1">MÓDULOS</h4>
                    <ul class="footer-links list-unstyled">
                        <li><a href="#">Músicos</a></li>
                        <li><a href="#">Bandas</a></li>
                        <li><a href="#">Estúdios</a></li>
                        <li><a href="#">Produtoras</a></li>
                        <li><a href="#">Casas de Show</a></li>
                        <li><a href="#">Fornecedores</a></li>
                        <li><a href="#">Organizações</a></li>
                        <li><a href="#">Fãs</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="letter-spacing-1">OBJETIVOS</h4>
                    <ul class="footer-links list-unstyled">
                        <li>Gerenciar a carreira musical</li>
                        <li>Controlar as atividades executivas</li>
                        <li>Organizar as agendas de ensaio</li>
                        <li>Coordenar as ações de seus eventos</li>
                        <li>Monitorar os seus resultados</li>
                        <li>Encontra os seus clientes</li>
                        <li>Melhorar o seu acesso à informação</li>
                        <li>Acompanhar atualizações em tempo real</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4 class="letter-spacing-1">ACOMPANHE AS NOVIDADES</h4>
                    <p>Inscreva-se em nossa lista para receber Informações Importantes sobre o projeto.</p>

                    <form class="validate" action="php/newsletter.php" method="post" data-success="Inscrito! Obrigado!" data-toastr-position="bottom-right">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" id="email" name="email" class="form-control required" placeholder="Digite o seu Email">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="submit">Enviar</button>
                            </span>
                        </div>
                    </form>

                    <!-- Social Icons -->
                    <div class="margin-top-20">
                        <a href="#" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>

                        <a href="#" class="social-icon social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
                            <i class="icon-rss"></i>
                            <i class="icon-rss"></i>
                        </a>

                    </div>
                    <!-- /Social Icons -->

                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <ul class="pull-right nomargin list-inline mobile-block">
                    <li><a href="#">Termos &amp; Condições</a></li>
                    <li>&bull;</li>
                    <li><a href="#">Privacidade</a></li>
                </ul>
                &copy; Todos os Direitos Reservados , Alpha Centauri - Tecnologia da Informação
            </div>
        </div>
    </footer>
@endsection