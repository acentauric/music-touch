<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>Music Touch - Um Toque de Música</title>

        <!-- seo (avaliar remoção) -->
        <meta name="keywords" content="HTML5,CSS3,Template" />
        <meta name="description" content="" />
        <meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

        <!-- WEB FONTS : use %7C instead of | (pipe) -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

        <!-- CORE CSS -->
        <link href="{{ URL::asset('website/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- REVOLUTION SLIDER -->
        <link href="{{ URL::asset('website/assets/plugins/slider.revolution/css/extralayers.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('website/assets/plugins/slider.revolution/css/settings.css') }}" rel="stylesheet" type="text/css" />

        <!-- THEME CSS -->
        <link href="{{ URL::asset('website/assets/css/essentials.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('website/assets/css/layout.css') }}" rel="stylesheet" type="text/css" />

        <!-- PAGE LEVEL SCRIPTS -->
        <link href="{{ URL::asset('website/assets/css/header-1.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('website/assets/css/color_scheme/green.css') }}" rel="stylesheet" type="text/css" />
		
        <!-- GOOGLE ANALYTICS -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109608957-1"></script>
        <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-109608957-1');
        </script>

    </head>

    <body class="smoothscroll enable-animation">

        <!-- Slide Top -->
        <div id="slidetop">

            <!-- Cointainer de Informações -->
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h6><i class="icon-heart"></i> POR QUE USAR?</h6>
                        <p class="text-left">
                            Nós desenvolvemos essa solução para o Ecossitema Musical com bastante responsabilidade e cuidado.
                            Sabemos das particularidades de cada atividade desse cenário e buscamos entregar funcionalidades práticas e eficazes.
                            O Music Touch é uma ferramenta de trabalho, não apenas mais uma Rede Social.
                        </p>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h6><i class="icon-tags"></i> MÓDULOS DO SISTEMA</h6>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Músicos</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Bandas</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Estúdios</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Produtoras</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Casas de Show</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Fornecedores</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Organizações</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Fãs</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h6><i class="icon-envelope"></i> CONTATO</h6>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-map-marker"></i> <b>Endereço:</b><br />Incubadora UNIFACS, Sala 04<br />Rua Vieira Lopes, Rio Vermelho<br />Salvador/BA, Brasil</li>
                            <li><i class="fa fa-phone"></i> <b>Telefone:</b><br />055 71993699538 <i class="fa fa-whatsapp"></i></li>
                            <li><i class="fa fa-envelope"></i> <b>Email:</b><br /><a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Toggle button -->
            <a class="slidetop-toggle" href="#"></a>

        </div>
        <!-- /Slide Top -->

        <!-- wrapper -->
        <div id="wrapper">

            <!-- HEADER -->
            @yield('header')
            <!-- /HEADER -->

            <!-- CONTENT -->
            @yield('content')
            <!-- /CONTENT -->

            <!-- FOOTER -->
            @yield('footer')
            <!-- /FOOTER -->

        </div>
        <!-- /wrapper -->

        <!-- SCROLL TO TOP -->
        <a href="#" id="toTop"></a>

        <!-- PRELOADER -->
        <div id="preloader">
            <div class="inner">
                <span class="loader"></span>
            </div>
        </div>
        <!-- /PRELOADER -->

        <!-- JAVASCRIPT FILES -->
        <script type="text/javascript">var plugin_path = '{{ URL::asset('website/assets/plugins/') }}/';</script>
        <script type="text/javascript" src="{{ URL::asset('website/assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('website/assets/js/scripts.js') }}"></script>

        <!-- REVOLUTION SLIDER -->
        <script type="text/javascript" src="{{ URL::asset('website/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('website/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('website/assets/js/view/demo.revolution_slider.js') }}"></script>

    </body>

</html>