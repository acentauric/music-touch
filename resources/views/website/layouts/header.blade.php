@section('header')
    <div id="topBar" class="hidden-xs">
        <div class="container">

            <!-- Link de Contato e Configurações Gerais -->
            <ul class="top-links list-inline">
                <li class="hidden-xs"><a href="{{ url('/website/contato') }}">CONTATO</a></li>

                <!-- Estrutura para Conversão de Idioma -->
                <li>
                    <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
                        <img class="flag-lang" src="{{ URL::asset('website/assets/images/flags/br.png') }}" width="16" height="11" alt="lang"> PORTUGUÊS
                    </a>
                    <ul class="dropdown-langs dropdown-menu">
                        <li>
                            <a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-translate-pt-Br-modal-sm">
                                <img class="flag-lang" src="{{ URL::asset('website/assets/images/flags/br.png') }}" width="16" height="11" alt="lang"> PORTUGUÊS
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-translate-en-modal-sm">
                                <img class="flag-lang" src="{{ URL::asset('website/assets/images/flags/us.png') }}" width="16" height="11" alt="lang"> ENGLISH
                            </a>
                        </li>
                        <li>
                            <a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-translate-es-modal-sm">
                                <img class="flag-lang" src="{{ URL::asset('website/assets/images/flags/es.png') }}" width="16" height="11" alt="lang"> ESPAÑOL
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Modal contendo informações sobre a alteração do idioma para o Português -->
                <div class="modal fade bs-translate-pt-Br-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Atenção!!!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                Você já está utilizando este Idioma.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Modal contendo informações sobre a alteração do idioma para o Inlgês -->
                <div class="modal fade bs-translate-en-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Attention!!!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                The translation to this language is not finished.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Modal contendo informações sobre a alteração do idioma para o Espanhol -->
                <div class="modal fade bs-translate-es-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">¡Precaución!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                La traducción para ese idioma no está terminada.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Estrutura para conversão de moeda -->
                <li>
                    <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">BRL</a>
                    <ul class="dropdown-langs dropdown-menu">
                        <li><a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-coin-brl-modal-sm">BRL</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-coin-usd-modal-sm">USD</a></li>
                        <li><a tabindex="-1" href="#" data-toggle="modal" data-target=".bs-coin-eur-modal-sm">EUR</a></li>
                    </ul>
                </li>

                <!-- Modal contendo informações sobre a alteração da moeda para o BRL -->
                <div class="modal fade bs-coin-brl-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Atenção!!!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                Você já está utilizando esta moeda.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Modal contendo informações sobre a alteração da moeda para o USD -->
                <div class="modal fade bs-coin-usd-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Atenção!!!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                Não é possível alterar a moeda para USD. Para maiores informações entre contato com a nossa equipe.
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Modal contendo informações sobre a alteração da moeda para o EUR -->
                <div class="modal fade bs-coin-eur-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <!-- header modal -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="mySmallModalLabel">Atenção!!!</h4>
                            </div>

                            <!-- body modal -->
                            <div class="modal-body">
                                Não é possível alterar a moeda para EUR. Para maiores informações entre contato com a nossa equipe.
                            </div>

                        </div>
                    </div>
                </div>

            </ul>

            <!-- Links para Acesso com Usuário Autenticado -->
            <ul class="top-links list-inline pull-right">

                {{-- Verifica se o usuário está logado no sistema --}}
                @if (Auth::guest())
                    <li>
                        <a href="{{ url('/website/home/auth/login') }}">
                            <i class="fa fa-user hidden-xs"></i> ENTRAR
                        </a>
                    </li>

                    <!-- Login Modal -->
                    <div class="modal fade bs-modal-login-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">

                                <!-- header modal -->
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myLargeModalLabel">Acesse o Sistema</h4>
                                </div>

                                <!-- body modal -->
                                <div class="modal-body">
                                    <section>
                                        <div class="tab-content">

                                            <!-- LOGIN -->
                                            <div class="tab-pane fade in active" id="login">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">

                                                        <div class="box-static box-transparent box-bordered padding-30">

                                                            <!-- Formulário de Login -->
                                                            <form class="sky-form" role="form" method="POST" action="{{ url('/login') }}">
                                                                {!! csrf_field() !!}

                                                                <div class="clearfix">

                                                                    <!-- Email -->
                                                                    <div class="form-group">
                                                                        <label>Email</label>
                                                                        <label class="input margin-bottom-10">
                                                                            <i class="ico-append fa fa-envelope"></i>
                                                                            <input required="" type="email">
                                                                            <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                                        </label>
                                                                    </div>

                                                                    <!-- Password -->
                                                                    <div class="form-group">
                                                                        <label>Password</label>
                                                                        <label class="input margin-bottom-10">
                                                                            <i class="ico-append fa fa-lock"></i>
                                                                            <input required="" type="password">
                                                                            <b class="tooltip tooltip-bottom-right">Type your account password</b>
                                                                        </label>
                                                                    </div>

                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                                        <div class="form-tip pt-20">
                                                                            <a class="no-text-decoration size-13 margin-top-10 block" href="#">Forgot Password?</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                                                        <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>
                                                                    </div>
                                                                </div>

                                                            </form>

                                                            <hr />

                                                            <div class="text-center">
                                                                <div class="margin-bottom-20">&ndash; OR &ndash;</div>

                                                                <a class="btn btn-block btn-social btn-facebook margin-top-10">
                                                                    <i class="fa fa-facebook"></i> Sign in with Facebook
                                                                </a>

                                                                <a class="btn btn-block btn-social btn-twitter margin-top-10">
                                                                    <i class="fa fa-twitter"></i> Sign in with Twitter
                                                                </a>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- LOGIN -->

                                            <!-- REGISTER -->
                                            <div class="tab-pane fade" id="register">

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">

                                                        <!-- ALERT -->
                                                        <div class="alert alert-mini alert-danger margin-bottom-30">
                                                            <strong>Oh snap!</strong> Password do not match!
                                                        </div><!-- /ALERT -->

                                                        <div class="box-static box-transparent box-bordered padding-30">

                                                            <div class="box-title margin-bottom-30">
                                                                <h2 class="size-20">Don't have an account yet?</h2>
                                                            </div>

                                                            <form class="nomargin sky-form" action="#" method="post">
                                                                <fieldset>

                                                                    <div class="row">
                                                                        <div class="form-group">

                                                                            <div class="col-md-12">
                                                                                <label>First Name *</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-user"></i>
                                                                                    <input required="" type="text">
                                                                                    <b class="tooltip tooltip-bottom-right">Your First Name</b>
                                                                                </label>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <label for="register:last_name">Last Name *</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-user"></i>
                                                                                    <input required="" type="text">
                                                                                    <b class="tooltip tooltip-bottom-right">Your Last Name</b>
                                                                                </label>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group">

                                                                            <div class="col-md-12">
                                                                                <label for="register:email">Email *</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-envelope"></i>
                                                                                    <input required="" type="text">
                                                                                    <b class="tooltip tooltip-bottom-right">Your Email</b>
                                                                                </label>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <label for="register:phone">Phone</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-phone"></i>
                                                                                    <input type="text">
                                                                                    <b class="tooltip tooltip-bottom-right">Your Phone (optional)</b>
                                                                                </label>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group">

                                                                            <div class="col-md-12">
                                                                                <label for="register:pass1">Password *</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-lock"></i>
                                                                                    <input required="" type="password" class="err">
                                                                                    <b class="tooltip tooltip-bottom-right">Min. 6 characters</b>
                                                                                </label>
                                                                            </div>

                                                                            <div class="col-md-12">
                                                                                <label for="register:pass2">Password Again *</label>
                                                                                <label class="input margin-bottom-10">
                                                                                    <i class="ico-append fa fa-lock"></i>
                                                                                    <input required="" type="password" class="err">
                                                                                    <b class="tooltip tooltip-bottom-right">Type the password again</b>
                                                                                </label>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <hr />

                                                                    <label class="checkbox nomargin"><input class="checked-agree" type="checkbox" name="checkbox"><i></i>I agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms of Service</a></label>
                                                                    <label class="checkbox nomargin"><input type="checkbox" name="checkbox"><i></i>Subscribe to newsletter</label>

                                                                </fieldset>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> REGISTER</button>
                                                                    </div>
                                                                </div>

                                                            </form>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <!-- /REGISTER -->

                                        </div>
                                    </section>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /Login Modal -->

                @else
                    <li class="text-welcome hidden-xs">Bem Vindo ao Music Touch, <strong>{{ Auth::user()->name }}</strong></li>
                    <li>
                        <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
                            <i class="fa fa-user hidden-xs"></i> CONTA
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a tabindex="-1" href="#"><i class="fa fa-cog"></i> CONFIGURAÇÕES</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
                        </ul>
                    </li>
                @endif

            </ul>

        </div>
    </div>
    <div id="header" class="sticky clearfix">
        <header id="topNav">
            <div class="container">

                <!-- Mobile Menu Button -->
                <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Buttons -->
                <ul class="pull-right nav nav-pills nav-second-main">

                    <!-- SEARCH -->
                    <li class="search">
                        <a href="javascript:;">
                            <i class="fa fa-search"></i>
                        </a>
                        <div class="search-box">
                            <form action="page-search-result-1.html" method="get">
                                <div class="input-group">
                                    <input type="text" name="src" placeholder="Pesquisar ..." class="form-control" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">Pesquisar</button>
                                        </span>
                                </div>
                            </form>
                        </div>
                    </li>
                    <!-- /SEARCH -->

                </ul>
                <!-- /Buttons -->

                <!-- Logo -->
                <a class="logo pull-left" href="{{ url('/') }}">
                    <img src="{{ URL::asset('website/assets/images/logo_dark.svg') }}" alt=""/>
                </a>

                <!-- Top Nav -->
                <div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
                    <nav class="nav-main">
                        <ul id="topMain" class="nav nav-pills nav-main">
                            <li class="active"><a class="" href="{{ url('/') }}">HOME</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#">INFORMAÇÕES</a>
                                <ul class="dropdown-menu">
                                    <li class=""><a class="" href="{{ url('/website/informacoes/quem-somos') }}">QUEM SOMOS</a></li>
                                    <li class=""><a class="" href="#">SOBRE O SISTEMA</a></li>
                                    <li class=""><a class="" href="#">TERMO DE USO</a></li>
                                    <li class=""><a class="" href="#">POLÍTICA DE PRIVACIDADE</a></li>
                                    <li class=""><a class="" href="#">FAQ</a></li>
                                    <li class=""><a class="" href="#">BLOG</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#">MÓDULOS</a>
                                <ul class="dropdown-menu">
                                    <li class=""><a class="" href="#">MÚSICOS</a></li>
                                    <li class=""><a class="" href="#">BANDAS</a></li>
                                    <li class=""><a class="" href="#">ESTÚDIOS</a></li>
                                    <li class=""><a class="" href="#">PRODUTORAS</a></li>
                                    <li class=""><a class="" href="#">CASAS DE SHOW</a></li>
                                    <li class=""><a class="" href="#">FORNECEDORES</a></li>
                                    <li class=""><a class="" href="#">ORGANIZAÇÕES</a></li>
                                    <li class=""><a class="" href="#">FÃS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- /Top Nav -->

            </div>
        </header>
    </div>
@endsection