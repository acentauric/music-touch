<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Music Touch - Um Toque de Música</title>

    <!-- seo (avaliar remoção) -->
    <meta name="keywords" content="HTML5,CSS3,Template" />
    <meta name="description" content="" />
    <meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- WEB FONTS : use %7C instead of | (pipe) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

    <!-- CORE CSS -->
    <link href="website/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- REVOLUTION SLIDER -->
    <link href="website/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
    <link href="website/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

    <!-- THEME CSS -->
    <link href="website/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="website/assets/css/layout.css" rel="stylesheet" type="text/css" />

    <!-- PAGE LEVEL SCRIPTS -->
    <link href="website/assets/css/header-1.css" rel="stylesheet" type="text/css" />
    <link href="website/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
</head>

<body class="smoothscroll enable-animation">

    <!-- Slide Top -->
    <div id="slidetop">

        <!-- Cointainer de Informações -->
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h6><i class="icon-heart"></i> POR QUE USAR?</h6>
                    <p class="text-justify">Nós desenvolvemos soluções para o Ecossitema Musical com bastante responsabilidade e cuidado. Sabemos das particularidades de cada atividade deste cenário e damos a devida atenção para entregar a vocês ferramentas práticas e eficazes.</p>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <h6><i class="icon-tags"></i> MÓDULOS DISPONÍVEIS</h6>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Músicos</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Bandas</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Estúdios</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Produtoras</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Casas de Show</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Fornecedores</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Organizações</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Gerenciamento de Fãs</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h6><i class="icon-envelope"></i> CONTATO</h6>
                    <ul class="list-unstyled">
                        <li><b>Endereço:</b> Edifício Tecnocentro - Rua Mundo, <br /> 121 - Trobogy, Salvador - BA, 41745-715</li>
                        <li><b>Telefone:</b> 55-71-99369-9538 <i class="fa fa-whatsapp"></i></li>
                        <li><b>Email:</b><a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Toggle button -->
        <a class="slidetop-toggle" href="#"></a>

    </div>
    <!-- /Slide Top -->

    <!-- wrapper -->
    <div id="wrapper">

        <!-- Top Bar -->
        <div id="topBar" class="hidden-xs">
            <div class="container">

                <!-- right (don't change) -->
                <ul class="top-links list-inline pull-right">
                    <li class="text-welcome hidden-xs">Bem Vindo ao Music Touch, <strong>Ramon Santos Mudar Aqui</strong></li>
                    <li>
                        <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
                            <i class="fa fa-user hidden-xs"></i> CONTA</a>
                        <ul class="dropdown-menu pull-right">
                            <li><a tabindex="-1" href="#"><i class="fa fa-cog"></i> CONFIGURAÇÕES</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
                        </ul>
                    </li>
                </ul>

                <!-- left (don't change) -->
                <ul class="top-links list-inline">
                    <li class="hidden-xs"><a href="page-contact-1.html">CONTATO</a></li>
                    <li>
                        <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">
                            <img class="flag-lang" src="website/assets/images/flags/br.png" width="16" height="11" alt="lang"> PORTUGUÊS
                        </a>
                        <ul class="dropdown-langs dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">
                                    <img class="flag-lang" src="website/assets/images/flags/br.png" width="16" height="11" alt="lang"> PORTUGUÊS
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="#">
                                    <img class="flag-lang" src="website/assets/images/flags/us.png" width="16" height="11" alt="lang"> ENGLISH
                                </a>
                            </li>
                            <li>
                                <a tabindex="-1" href="#">
                                    <img class="flag-lang" src="website/assets/images/flags/es.png" width="16" height="11" alt="lang"> ESPAÑOL
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#">BRL</a>
                        <ul class="dropdown-langs dropdown-menu">
                            <li><a tabindex="-1" href="#">BRL</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="#">USD</a></li>
                            <li><a tabindex="-1" href="#">EUR</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
        <!-- /Top Bar -->

        <!-- Header -->
        <div id="header" class="sticky shadow-after-3 clearfix">

            <!-- Top Nav -->
            <header id="topNav">
                <div class="container">

                    <!-- Mobile Menu Button -->
                    <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- BUTTONS -->
                    <ul class="pull-right nav nav-pills nav-second-main">

                        <!-- SEARCH -->
                        <li class="search">
                            <a href="javascript:;">
                                <i class="fa fa-search"></i>
                            </a>
                            <div class="search-box">
                                <form action="page-search-result-1.html" method="get">
                                    <div class="input-group">
                                        <input type="text" name="src" placeholder="Pesquisar ..." class="form-control" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="submit">Pesquisar</button>
                                            </span>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <!-- /SEARCH -->

                    </ul>
                    <!-- /BUTTONS -->

                    <!-- Logo -->
                    <a class="logo pull-left" href="index.html">
                        <img src="website/assets/images/logo_dark.png" alt="" />
                    </a>

                    <!-- Top Nav -->
                    <div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
                        <nav class="nav-main">
                            <ul id="topMain" class="nav nav-pills nav-main">
                                <li class="active"><a class="" href="#">HOME</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#">INFORMAÇÕES</a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="#">SOBRE NÓS</a></li>
                                        <li class=""><a class="" href="#">TERMO DE USO</a></li>
                                        <li class=""><a class="" href="#">POLÍTICA DE PRIVACIDADE</a></li>
                                        <li class=""><a class="" href="#">FAQ</a></li>
                                        <li class=""><a class="" href="#">BLOG</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#">MÓDULOS</a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="#">MÚSICOS</a></li>
                                        <li class=""><a class="" href="#">BANDAS</a></li>
                                        <li class=""><a class="" href="#">ESTÚDIOS</a></li>
                                        <li class=""><a class="" href="#">PRODUTORAS</a></li>
                                        <li class=""><a class="" href="#">CASAS DE SHOW</a></li>
                                        <li class=""><a class="" href="#">FORNECEDORES</a></li>
                                        <li class=""><a class="" href="#">ORGANIZAÇÕES</a></li>
                                        <li class=""><a class="" href="#">FÃS</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a id="sidepanel_btn" href="#" class="fa fa-bars"></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- /Top Nav -->

                </div>
            </header>
            <!-- /Top Nav -->

        </div>
        <!-- /Header -->

        <!-- Revolution Slider -->
        <div class="slider fullwidthbanner-container roundedcorners">
            <div class="fullwidthbanner" data-height="600" data-shadow="0" data-navigationStyle="preview2">

                <!-- slides -->
                <ul class="hide">

                    <!-- SLIDE -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide 1">

                        <img src="website/assets/images/1x1.png" data-lazyload="website/assets/images/demo/video/back.jpg" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

                        <div class="tp-caption tp-fade fadeout fullscreenvideo"
                             data-x="0"
                             data-y="50"
                             data-speed="1000"
                             data-start="1100"
                             data-easing="Power4.easeOut"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1500"
                             data-endeasing="Power4.easeIn"
                             data-autoplay="true"
                             data-autoplayonlyfirsttime="false"
                             data-nextslideatend="true"
                             data-volume="mute"
                             data-forceCover="1"
                             data-aspectratio="16:9"
                             data-forcerewind="on" style="z-index: 2;">

                            <div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>

                            <video class="" preload="none" style="widt:100%;height:100%" poster="website/assets/images/demo/video/back.jpg">
                                <source src="website/assets/images/demo/video/back.webm" type="video/webm" />
                                <source src="website/assets/images/demo/video/back.mp4" type="video/mp4" />
                            </video>

                        </div>

                        <div class="tp-caption customin ltl tp-resizeme text_white"
                             data-x="center"
                             data-y="155"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1000"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            <span class="weight-300">GESTÃO DA INFORMAÇÃO PARA O ECOSSISTEMA MUSICAL</span>
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme large_bold_white"
                             data-x="center"
                             data-y="205"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1200"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            BEM VINDO AO MUSIC TOUCH
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme small_light_white font-lato"
                             data-x="center"
                             data-y="295"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1400"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
                            Nós mapeamos o funcionamento dos processo deste ecossistema e desenvolvemos funcionalidades práticas para auxiliar na gestão de suas atividades diárias.
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme"
                             data-x="center"
                             data-y="363"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1550"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            <a href="#conheca-o-ecossistema" class="btn btn-default btn-lg">
                                <span>Conheça o Ecossistema</span>
                            </a>
                        </div>

                    </li>

                    <!-- SLIDE -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide 2">

                        <img src="website/assets/images/1x1.png" data-lazyload="website/assets/images/demo/video/back.jpg" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

                        <div class="tp-caption tp-fade fadeout fullscreenvideo"
                             data-x="0"
                             data-y="50"
                             data-speed="1000"
                             data-start="1100"
                             data-easing="Power4.easeOut"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1500"
                             data-endeasing="Power4.easeIn"
                             data-autoplay="true"
                             data-autoplayonlyfirsttime="false"
                             data-nextslideatend="true"
                             data-volume="mute"
                             data-forceCover="1"
                             data-aspectratio="16:9"
                             data-forcerewind="on" style="z-index: 2;">

                            <div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>

                            <video class="" preload="none" style="widt:100%;height:100%" poster="website/assets/images/demo/video/back.jpg">
                                <source src="website/assets/images/demo/video/back.webm" type="video/webm" />
                                <source src="website/assets/images/demo/video/back.mp4" type="video/mp4" />
                            </video>

                        </div>

                        <div class="tp-caption customin ltl tp-resizeme text_white"
                             data-x="center"
                             data-y="155"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1000"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            <span class="weight-300">GESTÃO DA INFORMAÇÃO PARA O ECOSSISTEMA MUSICAL</span>
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme large_bold_white"
                             data-x="center"
                             data-y="205"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1200"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            BEM VINDO AO MUSIC TOUCH
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme small_light_white font-lato"
                             data-x="center"
                             data-y="295"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1400"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
                            Nós mapeamos o funcionamento dos processo deste ecossistema e desenvolvemos funcionalidades práticas para auxiliar na gestão de suas atividades diárias.
                        </div>

                        <div class="tp-caption customin ltl tp-resizeme"
                             data-x="center"
                             data-y="363"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800"
                             data-start="1550"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style="z-index: 3;">
                            <a href="#conheca-o-ecossistema" class="btn btn-default btn-lg">
                                <span>Conheça o Ecossistema</span>
                            </a>
                        </div>

                    </li>

                </ul>

                <!-- progress bar -->
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!-- /Revolution Slider -->

        <!-- Info Bar -->
        <section class="info-bar info-bar-clean">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 hidden-xs">
                        <i class="glyphicon glyphicon-globe"></i>
                        <h3>APLICAÇÃO ESCALÁVEL</h3>
                        <p>Pensando pra funcionar em qualquer lugar do mundo.</p>
                    </div>
                    <div class="col-sm-4 hidden-xs">
                        <i class="glyphicon glyphicon-music"></i>
                        <h3>ECOSSISTEMA MUSICAL</h3>
                        <p>Pensado para atender a todos os atores do ecossistema.</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="glyphicon glyphicon-time"></i>
                        <h3>FUNCIONALIDADES PRÁTICAS</h3>
                        <p>Pensado para valorizar o seu tempo enquanto utiliza.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Info Bar -->

        <!-- Conheça o Ecossistema -->
        <a name="conheca-o-ecossistema"></a>
        <section class="nopadding-bottom">
            <div class="container">
                <header class="text-center margin-bottom-40">
                    <h1 class="weight-300">O Ecossistema</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>ENTENDA POR MEIO DESTE CONTEXTO</span></h2>
                </header>
                <div class="text-center">
                    <p class="lead">
                        Alguém decide tornar-se <b>Músico</b>, então procura amigos e formam uma <b>Banda</b>.
                        Eles encontram <b>Estúdios</b> para ensaiar e gravar <b>Músicas</b>.
                        Agora desejam participar de <b>Eventos</b> e então um <b>Produtor</b>
                        os encontra. Ele reserva uma <b>Casa de Shows</b> e aluga material dos seus <b>Fornecedores</b>.
                        A festa é formalizada junto às <b>Organizações</b> responsáveis
                        e os <b>Fãs</b> enfim podem curtir suas bandas favoritas.
                    </p>

                    <a href="#conheca-os-modulos" class="btn btn-default btn-lg">
                        <span>Conheça os Módulos</span>
                    </a>

                    <div class="margin-top-40">
                        <img class="img-responsive" src="website/assets/images/demo/index/ipad.jpg" alt="welcome" />
                    </div>

                </div>
            </div>
        </section>
        <!-- /Conheça o Ecossistema -->

        <!-- Conheça os Módulos -->
        <a name="conheca-os-modulos"></a>
        <section class="alternate">
            <div class="container">
                <header class="text-center margin-bottom-40">
                    <h1 class="weight-300">Os Módulos</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>ACESSE O MÓDULO ADEQUADO PARA VOCÊ</span></h2>
                </header>
                <div class="row">
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="fa fa-user"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Músicos</h2>
                            </a>
                            <p>Gerencie suas Músicas, Bandas e toda a sua carreira profissional por meio da nossa plataforma.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="fa fa-users"></i>
                            <a class="box-icon-title" href="{{ url('website/modulos/bandas') }}">
                                <h2>Bandas</h2>
                            </a>
                            <p>Gerencie seus Músicos, Ensaios e muito mais com as nossas funcionalidades feitas na medida certa.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="fa fa-microphone"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Estúdios</h2>
                            </a>
                            <p>Gerencie a sua agenda de Ensaios e a disponibilidade do seu estúdio de forma ágil por meio da internet.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="fa fa-bullhorn"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Produtoras</h2>
                            </a>
                            <p>Gerencie todas as atividades do seu Evento e armazene uma base de informações para utilizar nos próximos.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="glyphicon glyphicon-home"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Casas de Show</h2>
                            </a>
                            <p>Gerencie os seus Eventos e acompanhe por meio de históricos o desempenho de cada um deles.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Fornecedores</h2>
                            </a>
                            <p>Gerencie os seus clientes e seja informado imediatamente quando eles precisarem de seus serviços.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="glyphicon glyphicon-certificate"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Organizações</h2>
                            </a>
                            <p>Gerencie as suas atividades por meio do acesso às informações atualizadas do ecossistema como um todo.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-icon box-icon-side box-icon-color box-icon-round">
                            <i class="glyphicon glyphicon-headphones"></i>
                            <a class="box-icon-title" href="#">
                                <h2>Fãs</h2>
                            </a>
                            <p>Gerencie as suas Bandas preferidas e fique sempre por dentro das atividades delas pro meio de notícias.</p>
                            <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- / -->

        <!-- -->
        <section>
            <div class="container">
                <header class="text-center margin-bottom-40">
                    <h1 class="weight-300">Real Support. Real People.</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>THE MOST COMPLETE TEMPLATE EVER</span></h2>
                </header>

                <div class="row">

                    <div class="col-sm-6">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero quod consequuntur quibusdam, enim expedita sed quia nesciunt incidunt accusamus necessitatibus modi adipisci officia libero accusantium esse hic, obcaecati, ullam, laboriosam!</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti vero, animi suscipit id facere officia. Aspernatur, quo, quos nisi dolorum aperiam fugiat deserunt velit rerum laudantium cum magnam.</p>

                        <a class="btn btn-default" href="#">LEARN MORE</a>
                    </div>

                    <div class="col-sm-6">
                        <img class="img-responsive" src="website/assets/images/demo/index/support.jpg" alt="support" />
                    </div>

                </div>
            </div>
        </section>
        <!-- / -->

        <!-- COLOR BOXES -->
        <section class="nopadding noborder">
            <!--
                .box-pink
                .box-blue
                .box-orange
                .box-yellow
                .box-purple
                .box-red
                .box-brown
                .box-green
                .box-black
                .box-gray
                .box-teal



            CREATE YOUR OWN COLORS - CSS EXAMPLE:
                .box-pink>div:nth-child(1) {
                    background-color:#e2476b;
                }
                .box-pink>div:nth-child(2) {
                    background-color:#e9738f;
                }
                .box-pink>div:nth-child(3) {
                    background-color:#f09fb2;
                }
                .box-pink>div:nth-child(4) {
                    background-color:#f7cbd5;
                }

            -->
            <div class="row box-gradient box-gray">
                <div class="col-xs-6 col-sm-3">
                    <i class="fa fa-child fa-4x"></i>
                    <h2 class="countTo font-raleway" data-speed="3000">8165</h2>
                    <p>HAPPY CUSTOMERS</p>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <i class="fa fa-smile-o fa-4x"></i>
                    <h2 class="countTo font-raleway" data-speed="3000">1033</h2>
                    <p>FACIALS</p>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <i class="fa fa-heart fa-4x"></i>
                    <h2 class="countTo font-raleway" data-speed="3000">24567</h2>
                    <p>MASSAGES</p>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <i class="fa fa-female fa-4x"></i>
                    <h2 class="countTo font-raleway" data-speed="3000">68</h2>
                    <p>MOTHERS TO BE</p>
                </div>
            </div>
        </section>
        <!-- /COLOR BOXES -->

        <!-- Features -->
        <section>
            <div class="container">

                <header class="text-center margin-bottom-40">
                    <h1 class="weight-300">Smarty Features</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>WE TRULY CARE ABOUT OUR CUSTOMERS</span></h2>
                </header>

                <div class="row margin-top-80">

                    <div class="col-lg-4 col-md-4 col-md-push-4 text-center">

                        <img class="img-responsive" src="website/assets/images/demo/iphone-min.png" alt="" />

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-md-pull-4">

                        <div class="box-icon box-icon-right">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-eye"></i>
                                <h2>Retina Ready</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-right">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-check"></i>
                                <h2>Unlimited Options</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-right">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-rocket"></i>
                                <h2>Premium Sliders</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-right">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-flash"></i>
                                <h2>performance</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6">

                        <div class="box-icon box-icon-left">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-tablet"></i>
                                <h2>Fully Reposnive</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-left">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-random"></i>
                                <h2>Clean Design</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-left">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-tint"></i>
                                <h2>Reusable Elements</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                        <div class="box-icon box-icon-left">
                            <a class="box-icon-title" href="#">
                                <i class="fa fa-cogs"></i>
                                <h2>Multipurpose</h2>
                            </a>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                        </div>

                    </div>

                </div>


            </div>
        </section>
        <!-- /Features -->

        <!-- CALLOUT -->
        <section class="callout-dark heading-title heading-arrow-bottom">
            <div class="container">

                <header class="text-center">
                    <h1 class="weight-300 size-40">Our Latest Works</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>WE TRULY CARE ABOUT OUR CUSTOMERS</span></h2>
                </header>

            </div>
        </section>
        <!-- /CALLOUT -->

        <!-- WORK -->
        <section class="nopadding">
            <div id="portfolio" class="portfolio-nogutter">
                <div class="row mix-grid">

                    <div class="col-md-3 col-sm-3 mix development"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/1-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/1-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Nature Photography</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Photography</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix development"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/11-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/11-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Nature Photography</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Photography</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix photography"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/20-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/20-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Fashion Design</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Photography</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix design"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/19-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/19-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Ocean Project</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Photography</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix design"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/12-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/12-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Architect Project</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Architecture</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix development"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/13-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/13-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Speaker Design</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Audio</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix photography"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/14-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/14-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Mobile Development</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Development</a></li>
                                    <li><a href="#">Design</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->


                    <div class="col-md-3 col-sm-3 mix design"><!-- item -->

                        <div class="item-box">
                            <figure>
                                        <span class="item-hover">
                                            <span class="overlay dark-5"></span>
                                            <span class="inner">

                                                <!-- lightbox -->
                                                <a class="ico-rounded lightbox" href="website/assets/images/demo/mockups/1200x800/15-min.jpg" data-plugin-options='{"type":"image"}'>
                                                    <span class="fa fa-plus size-20"></span>
                                                </a>

                                                <!-- details -->
                                                <a class="ico-rounded" href="portfolio-single-slider.html">
                                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                </a>

                                            </span>
                                        </span>

                                <img class="img-responsive" src="website/assets/images/demo/mockups/600x399/15-min.jpg" width="600" height="399" alt="">
                            </figure>

                            <div class="item-box-desc">
                                <h3>Nature Art</h3>
                                <ul class="list-inline categories nomargin">
                                    <li><a href="#">Nature</a></li>
                                    <li><a href="#">Art</a></li>
                                </ul>
                            </div>

                        </div>

                    </div><!-- /item -->

                </div>

            </div>
        </section>
        <!-- /WORK -->

        <!-- -->
        <section>
            <div class="container">

                <div class="row">

                    <!-- toggle -->
                    <div class="col-md-4 col-sm-4">
                        <div class="toggle toggle-accordion toggle-transparent toggle-bordered-full">

                            <div class="toggle active">
                                <label>Lorem ipsum dolor.</label>
                                <div class="toggle-content">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>

                            <div class="toggle">
                                <label>Sit amet, consectetur.</label>
                                <div class="toggle-content">
                                    <p>Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc.</p>
                                </div>
                            </div>

                            <div class="toggle">
                                <label>Consectetur adipiscing elit.</label>
                                <div class="toggle-content">
                                    <p>Ut enim massa, sodales tempor convallis et, iaculis ac massa.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /toggle -->

                    <!-- skills -->
                    <div class="col-md-4 col-sm-4">
                        <h4>Our Skills</h4>

                        <label>
                            <span class="pull-right">60%</span>
                            MARKETING
                        </label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%; min-width: 2em;"></div>
                        </div>

                        <label>
                            <span class="pull-right">88%</span>
                            SALES
                        </label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 88%; min-width: 2em;"></div>
                        </div>

                        <label>
                            <span class="pull-right">93%</span>
                            DESIGN
                        </label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 93%; min-width: 2em;"></div>
                        </div>

                        <label>
                            <span class="pull-right">77%</span>
                            DEVELOPMENT
                        </label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 77%; min-width: 2em;"></div>
                        </div>

                        <label>
                            <span class="pull-right">99%</span>
                            OTHER
                        </label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 99%; min-width: 2em;"></div>
                        </div>

                    </div>
                    <!-- /skills -->

                    <!-- recent news -->
                    <div class="col-md-4 col-sm-4">
                        <h4>Recent News</h4>

                        <div class="row tab-post"><!-- post -->
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <a href="blog-sidebar-left.html">
                                    <img src="website/assets/images/demo/people/300x300/1-min.jpg" width="50" alt="" />
                                </a>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <a href="blog-sidebar-left.html" class="tab-post-link">Maecenas metus nulla</a>
                                <small>June 29 2014</small>
                            </div>
                        </div><!-- /post -->

                        <div class="row tab-post"><!-- post -->
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <a href="blog-sidebar-left.html">
                                    <img src="website/assets/images/demo/people/300x300/2-min.jpg" width="50" alt="" />
                                </a>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <a href="blog-sidebar-left.html" class="tab-post-link">Curabitur pellentesque neque eget</a>
                                <small>June 29 2014</small>
                            </div>
                        </div><!-- /post -->

                        <div class="row tab-post"><!-- post -->
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <a href="blog-sidebar-left.html">
                                    <img src="website/assets/images/demo/people/300x300/3-min.jpg" width="50" alt="" />
                                </a>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <a href="blog-sidebar-left.html" class="tab-post-link">Nam et lacus neque. Ut enim massa</a>
                                <small>June 29 2014</small>
                            </div>
                        </div><!-- /post -->

                    </div>
                    <!-- /recent news -->

                </div>

            </div>
        </section>
        <!-- / -->

        <!-- PARALLAX -->
        <section class="parallax parallax-2" style="background-image: url('website/assets/images/demo/1200x800/3-min.jpg');">
            <div class="overlay dark-8"><!-- dark overlay [1 to 9 opacity] --></div>

            <div class="container">

                <div class="text-center">
                    <h3 class="nomargin">Smarty Inside</h3>
                    <p class="font-lato weight-300 lead nomargin-top">We can't solve problems by using the same kind of thinking we used when we created them.</p>
                </div>

                <ul class="margin-top-80 social-icons list-unstyled list-inline">
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-facebook"></i>
                            <h4>Facebook</h4>
                            <span>Be Our Friend</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-twitter"></i>
                            <h4>Twitter</h4>
                            <span>Follow Us</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-youtube"></i>
                            <h4>Youtube</h4>
                            <span>Our Videos</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-instagram"></i>
                            <h4>Instagram</h4>
                            <span>See Our Images</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-linkedin"></i>
                            <h4>Linkedin</h4>
                            <span>Check Our Identity</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fa fa-pinterest"></i>
                            <h4>Pinterest</h4>
                            <span>Visual Discovery</span>
                        </a>
                    </li>
                </ul>

            </div>

        </section>
        <!-- /PARALLAX -->

        <!-- TEAM -->
        <section>
            <div class="container">

                <header class="text-center margin-bottom-60">
                    <h1 class="weight-300">Smarty Team</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>WE TRULY CARE ABOUT OUR CUSTOMERS</span></h2>
                </header>

                <div class="row">

                    <!-- item -->
                    <div class="col-md-3 col-sm-6">

                        <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                            <div class="front">
                                <div class="box1 box-default">
                                    <div class="box-icon-title">
                                        <img class="img-responsive" src="website/assets/images/demo/people/460x700/7-min.jpg" alt="" />
                                        <h2>Felica Doe</h2>
                                        <small>CEO</small>
                                    </div>
                                </div>
                            </div>

                            <div class="back">
                                <div class="box2 box-default">
                                    <h4 class="nomargin">Felica Doe</h4>
                                    <small>CEO</small>

                                    <hr />

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere</p>

                                    <hr />

                                    <a href="#" class="social-icon social-icon-sm social-facebook">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-twitter">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /item -->

                    <!-- item -->
                    <div class="col-md-3 col-sm-6">

                        <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                            <div class="front">
                                <div class="box1 box-default">
                                    <div class="box-icon-title">
                                        <img class="img-responsive" src="website/assets/images/demo/people/460x700/12-min.jpg" alt="" />
                                        <h2>Joana Doe</h2>
                                        <small>Art Director</small>
                                    </div>
                                </div>
                            </div>

                            <div class="back">
                                <div class="box2 box-default">
                                    <h4 class="nomargin">Joana Doe</h4>
                                    <small>Art Director</small>

                                    <hr />

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere</p>

                                    <hr />

                                    <a href="#" class="social-icon social-icon-sm social-facebook">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-twitter">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /item -->

                    <!-- item -->
                    <div class="col-md-3 col-sm-6">

                        <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                            <div class="front">
                                <div class="box1 box-default">
                                    <div class="box-icon-title">
                                        <img class="img-responsive" src="website/assets/images/demo/people/460x700/10-min.jpg" alt="" />
                                        <h2>Melissa Doe</h2>
                                        <small>Web Developer</small>
                                    </div>
                                </div>
                            </div>

                            <div class="back">
                                <div class="box2 box-default">
                                    <h4 class="nomargin">Melissa Doe</h4>
                                    <small>Web Developer</small>

                                    <hr />

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere</p>

                                    <hr />

                                    <a href="#" class="social-icon social-icon-sm social-facebook">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-twitter">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /item -->

                    <!-- item -->
                    <div class="col-md-3 col-sm-6">

                        <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                            <div class="front">
                                <div class="box1 box-default">
                                    <div class="box-icon-title">
                                        <img class="img-responsive" src="website/assets/images/demo/people/460x700/8-min.jpg" alt="" />
                                        <h2>Jessica Doe</h2>
                                        <small>Manager</small>
                                    </div>
                                </div>
                            </div>

                            <div class="back">
                                <div class="box2 box-default">
                                    <h4 class="nomargin">Jessica Doe</h4>
                                    <small>Manager</small>

                                    <hr />

                                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere</p>

                                    <hr />

                                    <a href="#" class="social-icon social-icon-sm social-facebook">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-twitter">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon social-icon-sm social-linkedin">
                                        <i class="fa fa-linkedin"></i>
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /item -->

                </div>


            </div>
        </section>
        <!-- /TEAM -->




        <!-- RECENT NEWS -->
        <section>
            <div class="container">

                <header class="text-center margin-bottom-60">
                    <h1 class="weight-300">Recent News</h1>
                    <h2 class="weight-300 letter-spacing-1 size-13"><span>WE TRULY CARE ABOUT OUR CUSTOMERS</span></h2>
                </header>

                <!--
                    controlls-over		= navigation buttons over the image
                    buttons-autohide 	= navigation buttons visible on mouse hover only

                    data-plugin-options:
                        "singleItem": true
                        "autoPlay": true (or ms. eg: 4000)
                        "navigation": true
                        "pagination": true
                        "items": "4"

                    owl-carousel item paddings
                        .owl-padding-0
                        .owl-padding-3
                        .owl-padding-6
                        .owl-padding-10
                        .owl-padding-15
                        .owl-padding-20
                -->
                <div class="owl-carousel owl-padding-10 buttons-autohide controlls-over" data-plugin-options='{"singleItem": false, "items":"4", "autoPlay": 4000, "navigation": true, "pagination": false}'>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/24-min.jpg" alt="">
                        </a>

                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/17-min.jpg" alt="">
                        </a>

                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/30-min.jpg" alt="">
                        </a>

                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/26-min.jpg" alt="">
                        </a>

                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/18-min.jpg" alt="">
                        </a>
                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/34-min.jpg" alt="">
                        </a>
                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/37-min.jpg" alt="">
                        </a>
                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="img-hover">
                        <a href="blog-single-default.html">
                            <img class="img-responsive" src="website/assets/images/demo/451x300/23-min.jpg" alt="">
                        </a>
                        <h4 class="text-left margin-top-20"><a href="blog-single-default.html">Lorem Ipsum Dolor</a></h4>
                        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in.</p>
                        <ul class="text-left size-12 list-inline list-separator">
                            <li>
                                <i class="fa fa-calendar"></i>
                                29th Jan 2015
                            </li>
                            <li>
                                <a href="blog-single-default.html#comments">
                                    <i class="fa fa-comments"></i>
                                    3
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </section>
        <!-- /RECENT NEWS -->


        <!--
            controlls-over		= navigation buttons over the image
            buttons-autohide 	= navigation buttons visible on mouse hover only

            data-plugin-options:
                "singleItem": true
                "autoPlay": true (or ms. eg: 4000)
                "navigation": true
                "pagination": true
        -->
        <div class="text-center margin-top-30 margin-bottom-30">
            <div class="owl-carousel nomargin" data-plugin-options='{"items":6, "singleItem": false, "autoPlay": true}'>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/1.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/2.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/3.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/4.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/5.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/6.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/7.jpg" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="website/assets/images/demo/brands/8.jpg" alt="">
                </div>
            </div>
        </div>


        <!-- FOOTER -->
        <footer id="footer">
            <div class="container">

                <div class="row">

                    <div class="col-md-3">
                        <!-- Footer Logo -->
                        <img class="footer-logo" src="website/assets/images/logo-footer.png" alt="" />

                        <!-- Small Description -->
                        <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

                        <!-- Contact Address -->
                        <address>
                            <ul class="list-unstyled">
                                <li class="footer-sprite address">
                                    PO Box 21132<br>
                                    Here Weare St, Melbourne<br>
                                    Vivas 2355 Australia<br>
                                </li>
                                <li class="footer-sprite phone">
                                    Phone: 1-800-565-2390
                                </li>
                                <li class="footer-sprite email">
                                    <a href="mailto:support@yourname.com">support@yourname.com</a>
                                </li>
                            </ul>
                        </address>
                        <!-- /Contact Address -->

                    </div>

                    <div class="col-md-3">

                        <!-- Latest Blog Post -->
                        <h4 class="letter-spacing-1">LATEST NEWS</h4>
                        <ul class="footer-posts list-unstyled">
                            <li>
                                <a href="#">Donec sed odio dui. Nulla vitae elit libero, a pharetra augue</a>
                                <small>29 June 2015</small>
                            </li>
                            <li>
                                <a href="#">Nullam id dolor id nibh ultricies</a>
                                <small>29 June 2015</small>
                            </li>
                            <li>
                                <a href="#">Duis mollis, est non commodo luctus</a>
                                <small>29 June 2015</small>
                            </li>
                        </ul>
                        <!-- /Latest Blog Post -->

                    </div>

                    <div class="col-md-2">

                        <!-- Links -->
                        <h4 class="letter-spacing-1">EXPLORE SMARTY</h4>
                        <ul class="footer-links list-unstyled">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Our Clients</a></li>
                            <li><a href="#">Our Pricing</a></li>
                            <li><a href="#">Smarty Tour</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                        <!-- /Links -->

                    </div>

                    <div class="col-md-4">

                        <!-- Newsletter Form -->
                        <h4 class="letter-spacing-1">KEEP IN TOUCH</h4>
                        <p>Subscribe to Our Newsletter to get Important News &amp; Offers</p>

                        <form class="validate" action="php/newsletter.php" method="post" data-success="Subscribed! Thank you!" data-toastr-position="bottom-right">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="email" id="email" name="email" class="form-control required" placeholder="Enter your Email">
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="submit">Subscribe</button>
                                        </span>
                            </div>
                        </form>
                        <!-- /Newsletter Form -->

                        <!-- Social Icons -->
                        <div class="margin-top-20">
                            <a href="#" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">

                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon social-icon-border social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>

                            <a href="#" class="social-icon social-icon-border social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">
                                <i class="icon-gplus"></i>
                                <i class="icon-gplus"></i>
                            </a>

                            <a href="#" class="social-icon social-icon-border social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>

                            <a href="#" class="social-icon social-icon-border social-rss pull-left" data-toggle="tooltip" data-placement="top" title="Rss">
                                <i class="icon-rss"></i>
                                <i class="icon-rss"></i>
                            </a>

                        </div>
                        <!-- /Social Icons -->

                    </div>

                </div>

            </div>

            <div class="copyright">
                <div class="container">
                    <ul class="pull-right nomargin list-inline mobile-block">
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li>&bull;</li>
                        <li><a href="#">Privacy</a></li>
                    </ul>
                    &copy; All Rights Reserved, Company LTD
                </div>
            </div>
        </footer>
        <!-- /FOOTER -->

    </div>
    <!-- /wrapper -->


    <!--
        SIDE PANEL

            sidepanel-dark 			= dark color
            sidepanel-light			= light color (white)
            sidepanel-theme-color		= theme color

            sidepanel-inverse		= By default, sidepanel is placed on right (left for RTL)
                            If you add "sidepanel-inverse", will be placed on left side (right on RTL).
    -->
    <div id="sidepanel" class="sidepanel-light">
        <a id="sidepanel_close" href="#"><!-- close -->
            <i class="glyphicon glyphicon-remove"></i>
        </a>

        <div class="sidepanel-content">
            <h2 class="sidepanel-title">Explore Smarty</h2>

            <!-- SIDE NAV -->
            <ul class="list-group">

                <li class="list-group-item">
                    <a href="#">
                        <i class="ico-category et-heart"></i>
                        ABOUT US
                    </a>
                </li>
                <li class="list-group-item list-toggle"><!-- add "active" to stay open on page load -->
                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-1" class="collapsed">
                        <i class="ico-dd icon-angle-down"><!-- Drop Down Indicator --></i>
                        <i class="ico-category et-strategy"></i>
                        PORTFOLIO
                    </a>
                    <ul id="collapse-1" class="list-unstyled collapse"><!-- add "in" to stay open on page load -->
                        <li><a href="#"><i class="fa fa-angle-right"></i> 1 COLUMN</a></li>
                        <li class="active">
                            <span class="badge">New</span>
                            <a href="#"><i class="fa fa-angle-right"></i> 2 COLUMNS</a>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> 3 COLUMNS</a></li>
                    </ul>
                </li>
                <li class="list-group-item list-toggle"><!-- add "active" to stay open on page load -->
                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-2" class="collapsed">
                        <i class="ico-dd icon-angle-down"><!-- Drop Down Indicator --></i>
                        <i class="ico-category et-trophy"></i>
                        PORTFOLIO
                    </a>
                    <ul id="collapse-2" class="list-unstyled collapse"><!-- add "in" to stay open on page load -->
                        <li><a href="#"><i class="fa fa-angle-right"></i> SLIDER</a></li>
                        <li class="active"><a href="#"><i class="fa fa-angle-right"></i> HEADERS</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> FOOTERS</a></li>
                    </ul>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <i class="ico-category et-happy"></i>
                        BLOG
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <i class="ico-category et-beaker"></i>
                        FEATURES
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <i class="ico-category et-map-pin"></i>
                        CONTACT
                    </a>
                </li>

            </ul>
            <!-- /SIDE NAV -->

            <!-- social icons -->
            <div class="text-center margin-bottom-30">

                <a href="#" class="social-icon social-icon-sm social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                </a>

                <a href="#" class="social-icon social-icon-sm social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                </a>

                <a href="#" class="social-icon social-icon-sm social-linkedin" data-toggle="tooltip" data-placement="top" title="Linkedin">
                    <i class="icon-linkedin"></i>
                    <i class="icon-linkedin"></i>
                </a>

                <a href="#" class="social-icon social-icon-sm social-rss" data-toggle="tooltip" data-placement="top" title="RSS">
                    <i class="icon-rss"></i>
                    <i class="icon-rss"></i>
                </a>

            </div>
            <!-- /social icons -->

        </div>

    </div>
    <!-- /SIDE PANEL -->


    <!-- SCROLL TO TOP -->
    <a href="#" id="toTop"></a>


    <!-- PRELOADER -->
    <div id="preloader">
        <div class="inner">
            <span class="loader"></span>
        </div>
    </div><!-- /PRELOADER -->


    <!-- JAVASCRIPT FILES -->
    <script type="text/javascript">var plugin_path = 'website/assets/plugins/';</script>
    <script type="text/javascript" src="website/assets/plugins/jquery/jquery-2.1.4.min.js"></script>

    <script type="text/javascript" src="website/assets/js/scripts.js"></script>

    <!-- REVOLUTION SLIDER -->
    <script type="text/javascript" src="website/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="website/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="website/assets/js/view/demo.revolution_slider.js"></script>

</body>
</html>