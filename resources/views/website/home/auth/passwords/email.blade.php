{{-- Adiciona a estrutura de Layout Base --}}
@extends('website.layouts.website')
@extends('website.layouts.header')
@extends('website.layouts.footer')

{{-- Adiciona o Conteúdo da View --}}
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    {{-- Apresentação de Erros se Existirem --}}
                    @if (($errors->has('email')) || ($errors->has('password')))
                    <div class="alert alert-mini alert-danger margin-bottom-30">
                        <ul>
                            @if ($errors->has('email'))<li><strong>Atenção!</strong> {{ $errors->first('email') }}</li>@endif
                            @if ($errors->has('password'))<li><strong>Atenção!</strong> {{ $errors->first('password') }}</li>@endif
                        </ul>
                    </div>
                    @endif

                    {{--  --}}
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <!-- Formulário para envio de Email de Recuperação de Senha -->
                    <div class="box-static box-border-top padding-30">
                        <div class="box-title margin-bottom-30">
                            <h2 class="size-20">Informe o Email cadastrado</h2>
                        </div>
                        <form class="nomargin" role="form" method="POST" action="{{ url('/password/email') }}" autocomplete="off">
                            {!! csrf_field() !!}
                            <div class="clearfix">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/website/home/auth/login') }}';"><i class="fa fa-btn fa-arrow-circle-o-left"></i> RETORNAR</button>
                                    <button class="btn btn-primary"><i class="fa fa-btn fa-envelope"></i> ENVIAR LINK</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection