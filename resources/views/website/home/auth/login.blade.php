{{-- Adiciona a estrutura de Layout Base --}}
@extends('website.layouts.website')
@extends('website.layouts.header')
@extends('website.layouts.footer')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb -->
    <section class="page-header page-header-xs">
        <div class="container">
            <h1>ENTRAR</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="active">Entrar</li>
            </ol>
        </div>
    </section>

    <!-- Section para realização do Login -->
    <section style="background:url('{{ URL::asset('website/assets/images/demo/wall2.jpg') }}')">
        <div class="display-table">
            <div class="display-table-cell vertical-align-middle">
                <div class="container">
                    <div class="row">
                        <!-- Opções de Login-->
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-md-push-7 col-lg-push-8 col-sm-push-7">

                            {{-- Apresentação de Erros se Existirem --}}
                            @if (($errors->has('email')) || ($errors->has('password')))
                                <div class="alert alert-mini alert-danger margin-bottom-30">
                                    <ul>
                                        @if ($errors->has('email'))<li><strong>Atenção!</strong> {{ $errors->first('email') }}</li>@endif
                                        @if ($errors->has('password'))<li><strong>Atenção!</strong> {{ $errors->first('password') }}</li>@endif
                                    </ul>
                                </div>
                            @endif
                            {{-- /Apresentação de Erros se Existirem --}}

                            <!-- Formulário de Login -->
                            <form class="sky-form boxed" role="form" method="POST" action="{{ url('/login') }}">
                                {!! csrf_field() !!}

                                <!-- Título -->
                                <header><i class="fa fa-sign-in"></i> Realize o Login</header>

                                <!-- Campos do Formulário de Login -->
                                <fieldset class="nomargin">
                                    <label class="label margin-top-20">E-mail</label>
                                    <label class="input">
                                        <i class="ico-append fa fa-envelope"></i>
                                        <input type="email" name="email" value="{{ old('email') }}">
                                        <span class="tooltip tooltip-top-right">Endereço de Email</span>
                                    </label>
                                    <label class="label margin-top-20">Senha</label>
                                    <label class="input">
                                        <i class="ico-append fa fa-lock"></i>
                                        <input type="password" name="password">
                                        <b class="tooltip tooltip-top-right">Digite a sua Senha</b>
                                    </label>
                                    <label class="label margin-top-20"></label>
                                    <label class="switch switch-primary switch-round">
                                        <input type="checkbox" checked="">
                                        <span class="switch-label" data-on="WEb" data-off="ADM"></span>
                                        <span> Selecione o destino após o Login</span>
                                    </label>
                                </fieldset>

                                <!-- Campos do Formulário de Login -->
                                <footer class="celarfix">
                                    <button type="submit" class="btn btn-primary noradius pull-right"><i class="fa fa-check"></i> ENTRAR</button>
                                    <div class="login-forgot-password pull-left">
                                        <a href="{{ url('/website/home/auth/passwords/email') }}">Recuperar Senha?</a>
                                    </div>
                                </footer>

                            </form>
                            <!-- /Formulário de Login -->

                            <!-- Login Social -->
                            <div class="margin-bottom-20 text-center">
                                – <b>Acessar Usando</b> –
                            </div>
                            <div class="socials margin-top10 text-center">
                                <a href="#" class="social-icon social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon social-google" data-toggle="tooltip" data-placement="top" title="Google">
                                    <i class="icon-google-plus"></i>
                                    <i class="icon-google-plus"></i>
                                </a>

                            </div>
                            <!-- /Login Social -->

                        </div>
                        <!-- /Opções de Login-->

                        <!-- Informações Sobre o Login-->
                        <div class="col-xs-12 col-md-7 col-sm-7 col-lg-8 col-lg-pull-4 col-md-pull-5 col-sm-pull-5">

                            <!-- Título -->
                            <h2 class="size-20 text-center-xs">Por qual motivo se Cadastrar?</h2>

                            <!-- Texto -->
                            <p>Ao utilizar o <strong>Music Touch</strong> você tem em suas mãos uma ferramenta extremamente funcional de suporte à
                                realização de suas atividades diárias. Nós pensamos em desenvolver funcionalidades aderentes às reais demandas de trabalho
                                de todos as pessoas qua atuam no ecossistema musical. </p>

                            <!-- Módulos -->
                            <ul class="list-unstyled login-features">
                                <li><i class="fa fa-user"></i> <strong>Músicos: </strong> gerenciem sua carreira musical.</li>
                                <li><i class="fa fa-users"></i> <strong>Bandas: </strong> controlem as suas atividades executivas.</li>
                                <li><i class="fa fa-microphone"></i> <strong>Estúdios: </strong> organizem as suas agendas de ensaio.</li>
                                <li><i class="fa fa-bullhorn"></i> <strong>Produtoras: </strong> coordenem as ações de seus eventos.</li>
                                <li><i class="glyphicon glyphicon-home"></i> <strong>Casas de Show: </strong> monitorem os seus resultados.</li>
                                <li><i class="glyphicon glyphicon-shopping-cart"></i> <strong>Fornecedores: </strong> encontrem os seus clientes.</li>
                                <li><i class="glyphicon glyphicon-certificate"></i> <strong>Organizações: </strong> melhorem o seu acesso à informação.</li>
                                <li><i class="glyphicon glyphicon-headphones"></i> <strong>Fãs: </strong> acompanhem as atualizações em tempo real.</li>
                            </ul>

                        </div>
                        <!-- /Informações Sobre o Login-->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Section para realização do Login  -->

@endsection


