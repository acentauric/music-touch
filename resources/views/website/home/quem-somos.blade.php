{{-- Adiciona a estrutura de Layout Base --}}
@extends('website.layouts.website')
@extends('website.layouts.header')
@extends('website.layouts.footer')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb -->
    <section class="page-header page-header-xs">
        <div class="container">
            <h1>QUEM SOMOS</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="#">Informações</a></li>
                <li class="active">Quem Somos</li>
            </ol>
        </div>
    </section>

    <!-- Informações da Página -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading-title heading-border-bottom">
                        <h3>Buscamos Inovação &amp; Praticidade</h3>
                    </div>
                    <p>
                        Somos uma Startup atuante nas áreas de Economia Criativa e Tecnologia da Informação e Comunicação.
                        Estamos em um processo intensivo de amadurecimento e buscamos profissionalizar ainda mais a realização de nossas atividades.
                        Desejamos nos tornar uma empresa que desenvolve projetos que atuam com tecnologias inovadoras ligadas a processos criativos.
                        Como grupo de empreendedores, nós atuamos em conjunto desde a nossa adolescência.
                        Nossas atividades já permearam as mais diversas áreas e os mais diversos cenários.
                        Já trabalhamos com Direção Musical, Produção Musical, Produção Cultural, Gerenciamento de Estúdios, Desenvolvimento de Websites e Desenvolvimento de Jogos.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Membros do Projeto (Título) -->
    <section class="callout-dark heading-title heading-arrow-bottom">
        <div class="container">
            <header class="text-center">
                <h1 class="weight-300 size-40">NOSSO TIME</h1>
                <h2 class="weight-300 letter-spacing-1 size-13"><span>Conheça as pessoas que desenvolvem o projeto</span></h2>
            </header>
        </div>
    </section>

    <!-- Membros do Projeto -->
    <section class="alternate">
        <div class="container">		    
			<div class="col-md-2 col-sm-2">
                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center" style="min-height: 436px;">
                    <div class="front">
                        <div class="box1 box-default" style="min-height: 436px;">
                            <div class="box-icon-title">
                                <img class="img-responsive" src="{{ URL::asset('website/images/quem-somos/marcelli-min.jpg') }}" alt="">
                                <h2>Neto Dias</h2>
                                <small>COO</small>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <div class="box2 box-default" style="min-height: 436px;">
                            <h4 class="nomargin">Neto Dias</h4>
                            <small>COO</small>
                            <hr>
                            <p>Chief Operating Officer - Responsável por cuidar das operações de negócio da empresa</p>
                            <hr>
                            <a href="https://www.facebook.com/neto.hard.5" target="_blank" class="social-icon social-icon-sm social-facebook">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-twitter">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-linkedin">
                                <i class="fa fa-linkedin"></i>
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-md-2 col-sm-2">
                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center" style="min-height: 436px;">
                    <div class="front">
                        <div class="box1 box-default" style="min-height: 436px;">
                            <div class="box-icon-title">
                                <img class="img-responsive" src="{{ URL::asset('website/images/quem-somos/neto-min.jpg') }}" alt="">
                                <h2>Neto Dias</h2>
                                <small>COO</small>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <div class="box2 box-default" style="min-height: 436px;">
                            <h4 class="nomargin">Neto Dias</h4>
                            <small>COO</small>
                            <hr>
                            <p>Chief Operating Officer - Responsável por cuidar das operações de negócio da empresa</p>
                            <hr>
                            <a href="https://www.facebook.com/neto.hard.5" target="_blank" class="social-icon social-icon-sm social-facebook">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-twitter">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-linkedin">
                                <i class="fa fa-linkedin"></i>
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-md-2 col-sm-2">
                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center" style="min-height: 436px;">
                    <div class="front">
                        <div class="box1 box-default" style="min-height: 436px;">
                            <div class="box-icon-title">
                                <img class="img-responsive" src="{{ URL::asset('website/images/quem-somos/neto-min.jpg') }}" alt="">
                                <h2>Neto Dias</h2>
                                <small>COO</small>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <div class="box2 box-default" style="min-height: 436px;">
                            <h4 class="nomargin">Neto Dias</h4>
                            <small>COO</small>
                            <hr>
                            <p>Chief Operating Officer - Responsável por cuidar das operações de negócio da empresa</p>
                            <hr>
                            <a href="https://www.facebook.com/neto.hard.5" target="_blank" class="social-icon social-icon-sm social-facebook">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-twitter">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon social-icon-sm social-linkedin">
                                <i class="fa fa-linkedin"></i>
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </section>

    <!-- Informações da Página -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="heading-title heading-border-bottom heading-color">
                                <h3>Nosso Projeto</h3>
                                <p>Visão geral do que desejamos produzir</p>
                            </div>
                            <p>
                                O projeto “Music Touch - Um Toque de Música” consiste basicamente em organizar os processos do ecossistema musical e entregar,
                                por meio de uma solução de Tecnologia da Informação, funcionalidades práticas de auxílios às atividades diárias para as pessoas
                                que atuam neste cenário.
                                Estamos criando uma solução que parte do conceito da prática da Escalabilidade, conforme definição no site da Endeavor:
                                “um negócio escalável é aquele que apresenta potencial de expansão, de preferência sem limites, que pode funcionar com sucesso enquanto se desenvolve”.
                                Desta forma, entendemos que será possível realizar um processo de validação de maneira mais controlada.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <div class="heading-title heading-border-bottom heading-color">
                                <h3>12 Anos de Exepriência</h3>
                                <p>Nosso conhecimento e nossa visão</p>
                            </div>
                            <p>
                                Possuímos quantitativamente 12 anos de experiência no ecossistema musical e iniciamos os primeiros contatos com este cenário em 2004.
                                Neste período, individualmente ou em grupo, continuadamente ou sazonalmente, passamos por diversas realidades no cenário musical.
                                Conhecemos as oito áreas que abordaremos neste projeto: músicos, bandas, estúdios, produtoras, casas de show, organizações, fornecedores e fãs.
                                Há ao menos duas premissas que podem ser formadas a partir desta nossa experiência: não há processos padronizados
                                e, mesmo sem eles, as coisas acontecem por meio de soluções alternativas que são aderentes às necessidades individuais de cada ator deste ecossistema.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="heading-title heading-border-bottom heading-color">
                                <h3>Por que Utilizar?</h3>
                                <p>Descubra as vantages de utilizar o sistema</p>
                            </div>
                            <p>
                                A seguir foram listadas diversas funcionalidades que estamos desenvolvendo para facilitar o seu dia a dia.
                                O nosso objetivo é entender o ecossistema musical como um todo e tratá-lo como um ambiente de atividades que permita o suporte de um sistema informatizado.
                                O Music Touch tem o papel de atuar como uma solução de suporte informatizado para facilitar o seu trabalho.
                            </p>
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="list-unstyled list-icons">
                                        <li><i class="fa fa-user text-success"></i> Gerencie sua carreira musical.</li>
                                        <li><i class="fa fa-users text-success"></i> Controle as suas atividades executivas.</li>
                                        <li><i class="fa fa-microphone text-success"></i> Organize as suas agendas de ensaio.</li>
                                        <li><i class="fa fa-bullhorn text-success"></i> Coordene as ações de seus eventos.</li>
                                        <li><i class="glyphicon glyphicon-home text-success"></i> Monitore os seus resultados.</li>
                                        <li><i class="glyphicon glyphicon-shopping-cart text-success"></i> Encontre os seus clientes.</li>
                                        <li><i class="glyphicon glyphicon-certificate text-success"></i> Melhore o seu acesso à informação.</li>
                                        <li><i class="glyphicon glyphicon-headphones text-success"></i> Acompanhe as atualizações em tempo real.</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="list-unstyled list-icons">
                                        <li><i class="fa fa-user text-success"></i> Monte o seu currículo profissional.</li>
                                        <li><i class="fa fa-users text-success"></i> Encontre produtoras e estúdios próximos.</li>
                                        <li><i class="fa fa-microphone text-success"></i> Mantenha um histórico dos seus pagamentos.</li>
                                        <li><i class="fa fa-bullhorn text-success"></i> Monte a grade do seu evento de forma fácil.</li>
                                        <li><i class="glyphicon glyphicon-home text-success"></i> Avalie quais atrações são mais adequedas.</li>
                                        <li><i class="glyphicon glyphicon-shopping-cart text-success"></i> Gerencie sua carteira de clientes.</li>
                                        <li><i class="glyphicon glyphicon-certificate text-success"></i> Monitore as atividades em sua região.</li>
                                        <li><i class="glyphicon glyphicon-headphones text-success"></i> Acesse as informações de forma organizada.</li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="list-unstyled list-icons">
                                        <li><i class="fa fa-user text-success"></i> Mantenha as suas atividades formalizadas.</li>
                                        <li><i class="fa fa-users text-success"></i> Divulge suas atividades e encontre fãs.</li>
                                        <li><i class="fa fa-microphone text-success"></i> Organize os equipamentos em suas salas.</li>
                                        <li><i class="fa fa-bullhorn text-success"></i> Obtenha todas as documentações rapidamente.</li>
                                        <li><i class="glyphicon glyphicon-home text-success"></i> Entenda o perfil dos fornecedores e clientes.</li>
                                        <li><i class="glyphicon glyphicon-shopping-cart text-success"></i> Saiba quem está precisando dos seus serviços.</li>
                                        <li><i class="glyphicon glyphicon-certificate text-success"></i> Obtenha documentos organizados e padronizados.</li>
                                        <li><i class="glyphicon glyphicon-headphones text-success"></i> Compartilhe com a sua comunidade as novidades.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection