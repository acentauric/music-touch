{{-- Adiciona a estrutura de Layout Base --}}
@extends('website.layouts.website')
@extends('website.layouts.header')
@extends('website.layouts.footer')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Revolution Slider -->
    <div class="slider fullwidthbanner-container roundedcorners">
        <div class="fullwidthbanner" data-height="600" data-shadow="0" data-navigationStyle="preview2">

            <!-- slides -->
            <ul class="hide">

                <!-- SLIDE -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide 1">

                    <img src="website/assets/images/1x1.png" data-lazyload="website/assets/images/demo/video/back.jpg" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <div class="tp-caption tp-fade fadeout fullscreenvideo"
                         data-x="0"
                         data-y="50"
                         data-speed="1000"
                         data-start="1100"
                         data-easing="Power4.easeOut"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1500"
                         data-endeasing="Power4.easeIn"
                         data-autoplay="true"
                         data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true"
                         data-volume="mute"
                         data-forceCover="1"
                         data-aspectratio="16:9"
                         data-forcerewind="on" style="z-index: 2;">

                        <div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>

                        <video class="" preload="none" style="widt:100%;height:100%" poster="website/assets/images/demo/video/back.jpg">
                            <source src="website/assets/images/demo/video/back.webm" type="video/webm" />
                            <source src="website/assets/images/demo/video/back.mp4" type="video/mp4" />
                        </video>

                    </div>

                    <div class="tp-caption customin ltl tp-resizeme text_white"
                         data-x="center"
                         data-y="155"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        <span class="weight-300">GESTÃO DA INFORMAÇÃO PARA O ECOSSISTEMA MUSICAL</span>
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme large_bold_white"
                         data-x="center"
                         data-y="205"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1200"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        BEM VINDO AO MUSIC TOUCH
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme small_light_white font-lato"
                         data-x="center"
                         data-y="295"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1400"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
                        Nós mapeamos o funcionamento dos processo deste ecossistema e desenvolvemos funcionalidades práticas para auxiliar na gestão de suas atividades diárias.
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme"
                         data-x="center"
                         data-y="363"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1550"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        <a href="#conheca-o-ecossistema" class="btn btn-default btn-lg">
                            <span>Conheça o Ecossistema</span>
                        </a>
                    </div>

                </li>

                <!-- SLIDE -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide 2">

                    <img src="website/assets/images/1x1.png" data-lazyload="website/assets/images/demo/video/back.jpg" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <div class="tp-caption tp-fade fadeout fullscreenvideo"
                         data-x="0"
                         data-y="50"
                         data-speed="1000"
                         data-start="1100"
                         data-easing="Power4.easeOut"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1500"
                         data-endeasing="Power4.easeIn"
                         data-autoplay="true"
                         data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true"
                         data-volume="mute"
                         data-forceCover="1"
                         data-aspectratio="16:9"
                         data-forcerewind="on" style="z-index: 2;">

                        <div class="tp-dottedoverlay twoxtwo"><!-- dotted overlay --></div>

                        <video class="" preload="none" style="widt:100%;height:100%" poster="website/assets/images/demo/video/back.jpg">
                            <source src="website/assets/images/demo/video/back.webm" type="video/webm" />
                            <source src="website/assets/images/demo/video/back.mp4" type="video/mp4" />
                        </video>

                    </div>

                    <div class="tp-caption customin ltl tp-resizeme text_white"
                         data-x="center"
                         data-y="155"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        <span class="weight-300">GESTÃO DA INFORMAÇÃO PARA O ECOSSISTEMA MUSICAL</span>
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme large_bold_white"
                         data-x="center"
                         data-y="205"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1200"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        BEM VINDO AO MUSIC TOUCH
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme small_light_white font-lato"
                         data-x="center"
                         data-y="295"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1400"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3; width: 100%; max-width: 750px; white-space: normal; text-align:center; font-size:20px;">
                        Nós mapeamos o funcionamento dos processo deste ecossistema e desenvolvemos funcionalidades práticas para auxiliar na gestão de suas atividades diárias.
                    </div>

                    <div class="tp-caption customin ltl tp-resizeme"
                         data-x="center"
                         data-y="363"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="800"
                         data-start="1550"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="z-index: 3;">
                        <a href="#conheca-o-ecossistema" class="btn btn-default btn-lg">
                            <span>Conheça o Ecossistema</span>
                        </a>
                    </div>

                </li>

            </ul>

            <!-- progress bar -->
            <div class="tp-bannertimer"></div>
        </div>
    </div>
    <!-- /Revolution Slider -->

    <!-- Info Bar -->
    <section class="info-bar info-bar-clean">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    <i class="glyphicon glyphicon-globe"></i>
                    <h3>APLICAÇÃO ESCALÁVEL</h3>
                    <p>Pensando pra funcionar em qualquer lugar do mundo.</p>
                </div>
                <div class="col-sm-4 hidden-xs">
                    <i class="glyphicon glyphicon-music"></i>
                    <h3>ECOSSISTEMA MUSICAL</h3>
                    <p>Pensado para atender a todos os atores do ecossistema.</p>
                </div>
                <div class="col-sm-4">
                    <i class="glyphicon glyphicon-time"></i>
                    <h3>FUNCIONALIDADES PRÁTICAS</h3>
                    <p>Pensado para valorizar o seu tempo enquanto utiliza.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- /Info Bar -->

    <!-- Conheça o Ecossistema -->
    <a name="conheca-o-ecossistema"></a>
    <section class="nopadding-bottom">
        <div class="container">
            <header class="text-center margin-bottom-40">
                <h1 class="weight-300">O Ecossistema</h1>
                <h2 class="weight-300 letter-spacing-1 size-13"><span>ENTENDA POR MEIO DESTE CONTEXTO</span></h2>
            </header>
            <div class="text-center">
                <p class="lead">
                    Alguém decide tornar-se <b>Músico</b>, então procura amigos e formam uma <b>Banda</b>.
                    Eles encontram <b>Estúdios</b> para ensaiar e gravar <b>Músicas</b>.
                    Agora desejam participar de <b>Eventos</b> e então um <b>Produtor</b>
                    os encontra. Ele reserva uma <b>Casa de Shows</b> e aluga material dos seus <b>Fornecedores</b>.
                    A festa é formalizada junto às <b>Organizações</b> responsáveis
                    e os <b>Fãs</b> enfim podem curtir suas bandas favoritas.
                </p>

                <a href="#conheca-os-modulos" class="btn btn-default btn-lg">
                    <span>Conheça os Módulos</span>
                </a>

                <div class="margin-top-40">
                    <img class="img-responsive" src="website/assets/images/demo/index/ipad.jpg" alt="welcome" />
                </div>

            </div>
        </div>
    </section>
    <!-- /Conheça o Ecossistema -->

    <!-- Conheça os Módulos -->
    <a name="conheca-os-modulos"></a>
    <section class="alternate">
        <div class="container">
            <header class="text-center margin-bottom-40">
                <h1 class="weight-300">Os Módulos</h1>
                <h2 class="weight-300 letter-spacing-1 size-13"><span>ACESSE O MÓDULO ADEQUADO PARA VOCÊ</span></h2>
            </header>
            <div class="row">
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="fa fa-user"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Músicos</h2>
                        </a>
                        <p>Gerencie suas Músicas, Bandas e toda a sua carreira profissional por meio da nossa plataforma.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="fa fa-users"></i>
                        <a class="box-icon-title" href="{{ url('website/modulos/bandas') }}">
                            <h2>Bandas</h2>
                        </a>
                        <p>Gerencie seus Músicos, Ensaios e muito mais com as nossas funcionalidades feitas na medida certa.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="fa fa-microphone"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Estúdios</h2>
                        </a>
                        <p>Gerencie a sua agenda de Ensaios e a disponibilidade do seu estúdio de forma ágil por meio da internet.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="fa fa-bullhorn"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Produtoras</h2>
                        </a>
                        <p>Gerencie todas as atividades do seu Evento e armazene uma base de informações para utilizar nos próximos.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="glyphicon glyphicon-home"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Casas de Show</h2>
                        </a>
                        <p>Gerencie os seus Eventos e acompanhe por meio de históricos o desempenho de cada um deles.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="glyphicon glyphicon-shopping-cart"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Fornecedores</h2>
                        </a>
                        <p>Gerencie os seus clientes e seja informado imediatamente quando eles precisarem de seus serviços.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="glyphicon glyphicon-certificate"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Organizações</h2>
                        </a>
                        <p>Gerencie as suas atividades por meio do acesso às informações atualizadas do ecossistema como um todo.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-icon box-icon-side box-icon-color box-icon-round">
                        <i class="glyphicon glyphicon-headphones"></i>
                        <a class="box-icon-title" href="#">
                            <h2>Fãs</h2>
                        </a>
                        <p>Gerencie as suas Bandas preferidas e fique sempre por dentro das atividades delas pro meio de notícias.</p>
                        <a class="box-icon-more font-lato weight-300" href="#">Acessar Módulo</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /Conheça os Módulos -->

    <!-- Nosso Banco de Dados -->
    <section class="nopadding noborder nomargin">
        <div class="row nopadding noborder nomargin">
            <div class="col-md-6 nopadding noborder nomargin">
                <div class="row box-gradient box-gray">
                    <div class="col-xs-6 col-sm-3"  style="padding-bottom: 34px;">
                        <i class="fa fa-user fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">8165</h2>
                        <p>MÚSICOS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3"  style="padding-bottom: 34px;">
                        <i class="fa fa-users fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">1033</h2>
                        <p>BANDAS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3"  style="padding-bottom: 34px;">
                        <i class="fa fa-microphone fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">24567</h2>
                        <p>ESTÚDIOS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3" style="padding-bottom: 34px;">
                        <i class="fa fa-bullhorn fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">68</h2>
                        <p>PRODUTORAS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 nopadding noborder nomargin">
                <div class="row box-gradient box-gray">
                    <div class="col-xs-6 col-sm-3">
                        <i class="glyphicon glyphicon-home fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">8165</h2>
                        <p>MÚSICOS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <i class="glyphicon glyphicon-shopping-cart fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">1033</h2>
                        <p>BANDAS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <i class="glyphicon glyphicon-certificate fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">24567</h2>
                        <p>ESTÚDIOS</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <i class="glyphicon glyphicon-headphones fa-4x"></i>
                        <h2 class="countTo font-raleway" data-speed="3000">68</h2>
                        <p>PRODUTORAS</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Nosso Banco de Dados  -->

    <!-- Nossos Parceiros -->
    <section class="callout-dark heading-title heading-arrow-bottom">
        <div class="container">
            <header class="text-center">
                <h1 class="weight-300 size-40">Nossos Parceiros</h1>
                <h2 class="weight-300 letter-spacing-1 size-13"><span>CONHEÇA AGORA OS ALGUNS PARCEIROS</span></h2>
            </header>
        </div>
    </section>
    <div class="text-center margin-top-30 margin-bottom-30">
        <div class="owl-carousel nomargin" data-plugin-options='{"items":6, "singleItem": false, "autoPlay": true}'>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/1.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/2.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/3.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/4.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/5.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/6.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/7.jpg" alt="">
            </div>
            <div>
                <img class="img-responsive" src="website/assets/images/demo/brands/8.jpg" alt="">
            </div>
        </div>
    </div>

@endsection