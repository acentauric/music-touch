{{-- Adiciona a estrutura de Layout Base --}}
@extends('website.layouts.website')
@extends('website.layouts.header')
@extends('website.layouts.footer')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb -->
    <section class="page-header page-header-xs">
        <div class="container">
            <h1>CONTATO</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="active">Contato</li>
            </ol>
        </div>
    </section>

    <!-- Apresenta a estrutura da página -->
    <section>
        <div class="container">
            <div class="row">

                <!-- Fromulário -->
                <div class="col-md-9 col-sm-9">

                    <!-- Mansagens e Alertas -->
                    <h3>Preencha o <strong><em>Formulário de Contato!</em></strong></h3>
                    <div id="alert_success" class="alert alert-success margin-bottom-30">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Obrigado!</strong> Sua mensagem foi enviada com sucesso!
                    </div>
                    <div id="alert_failed" class="alert alert-danger margin-bottom-30">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>[SMTP] Error!</strong> Erro interno do servidor!
                    </div>
                    <div id="alert_mandatory" class="alert alert-danger margin-bottom-30">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Desculpe!</strong> Você precisa preencher todos os campos (*) obrigatórios!
                    </div>

                    <!-- http://www.easylaravelbook.com/blog/2015/02/09/creating-a-contact-form-in-laravel-5-using-the-form-request-feature/ -->

                    <!-- Formulário de Contato -->
                    <form action="{{ url('/website/contato') }}" method="post" enctype="multipart/form-data">

                        {{-- Adiciona Informações de Controle do Framework --}}
                        {{ csrf_field() }}

                        <fieldset>
                            <input type="hidden" name="action" value="contact_send" />
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label for="contact:name">Nome *</label>
                                        <input required type="text" value="" class="form-control" name="contact[name][required]" id="contact:name">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="contact:email">E-mail *</label>
                                        <input required type="email" value="" class="form-control" name="contact[email][required]" id="contact:email">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="contact:phone">Telefone</label>
                                        <input type="text" value="" class="form-control" name="contact[phone]" id="contact:phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <label for="contact:subject">Assunto *</label>
                                        <input required type="text" value="" class="form-control" name="contact[subject][required]" id="contact:subject">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="contact_department">Tipo</label>
                                        <select class="form-control pointer" name="contact[department]">
                                            <option value="">--- Selecione ---</option>
                                            <option value="Duvidas">Dúvidas</option>
                                            <option value="Elogio">Elogio</option>
                                            <option value="Reclamacao">Reclamação</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="contact:message">Mensagem *</label>
                                        <textarea required maxlength="10000" rows="8" class="form-control" name="contact[message]" id="contact:message"></textarea>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> ENVIAR</button>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /Formulário -->

                <!-- Informações -->
                <div class="col-md-3 col-sm-3">

                    <h2>Importante</h2>
                    <p>Preencha corretamente os campos do formulário e principalmente o seu endereço de e-mail, pois iremos responder o seu contato por meio dele.</p>

                    <hr />

                    <h4 class="font300">Contatos</h4>
                    <p>
                        <span class="block"><strong><i class="fa fa-map-marker"></i></strong> Salvador, Bahia, Brasil</span>
                        <span class="block"><strong><i class="fa fa-phone"></i></strong> <a href="tel:055 71 993699538">055 71 993699538</a></span>
                        <span class="block"><strong><i class="fa fa-envelope"></i></strong> <a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></span>
                    </p>

                    <hr />

                    <h4 class="font300">Horário de Atendimento</h4>
                    <p>
                        <span class="block"><strong>Segunda - Sexta:</strong> 08:00 as 18:00</span>
                        <span class="block"><strong>Sábado:</strong> 08:00 as 12:00</span>
                        <span class="block"><strong>Domingo:</strong> Lei Imperial n.º 3.353</span>
                    </p>

                </div>
                <!-- /Informações -->

            </div>
        </div>
    </section>

@endsection