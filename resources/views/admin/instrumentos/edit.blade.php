{{-- Adiciona a estrutura de Layout Base --}}
@extends('layouts.app')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}">Home</a></li>
    <li><a href="{{ url('/bandas') }}">Sistema de Bandas</a></li>
    <li><a href="{{ url('/estilos') }}">Estilos</a></li>
    <li class="active">Editar</li>
    </ol>

    <!-- Formulário para a Edição do Registro Consultado -->
    <form method="POST" class="form-horizontal" action="{{ url('estilo/'.$estilo->id) }}">

        {{-- Adiciona Informações de Controle do Framework --}}
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informações do Estilo</h3>
            </div>
            <div class="panel-body">

                <!-- Apresenta Erros na Ação se Existirem -->
                @include('common.errors')

                <!-- Campos do Formulário -->
                <div class="form-group">
                    <label for="estilo" class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-6">
                        <input type="text" name="nome" id="estilo-nome" class="form-control" value="{{$estilo->nome}}">
                    </div>
                </div>

            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Editar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/estilos') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>

    </form>

@endsection