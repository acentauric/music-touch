{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Músicos</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/musicos') }}">Músicos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Formulário para a Edição do Registro Consultado -->
        <form method="POST" class="" action="{{ url('/admin/musicos/atualizar/'.$musico->id) }}" data-success="Edição Realizada com sucesso!">

            {{-- Adiciona Informações de Controle do Framework --}}
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-cog"></i> <strong>DADOS BÁSICOS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <!-- Apresenta Erros na Ação se Existirem -->
                    @include('common.errors')

                    <!-- Campos do Formulário -->
                    <fieldset>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Nome *</label>
                                    <div class="fancy-form">
                                        <i class="fa fa-tag"></i>
                                            <input type="text" name="nome" value="{{ $musico->nome }}" class="form-control required" data-placeholder="X" placeholder="Informe o Nome Artístico do Músico">
                                        <span class="fancy-tooltip top-left">
                                            <em>Informe o Nome Artístico do Músico</em>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Data de Nascimento</label>
                                    <div class="fancy-form">
                                        <i class="fa fa-birthday-cake"></i>
                                        @if($musico->data_de_nascimento != '')
                                            <input type="text" name="data_de_nascimento" value="{{ $musico->data_de_nascimento->format('d/m/Y') }}" class="form-control datepicker" data-format="dd/mm/yyyy" data-lang="pt-Br" data-rtl="false">
                                        @else
                                            <input type="text" name="data_de_nascimento" value="" class="form-control datepicker" data-format="dd/mm/yyyy" data-lang="pt-Br" data-rtl="false">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4">
                                    <label>Estado</label>
                                    <div class="fancy-form fancy-form-select">
                                        <select class="form-control select2" name="estado_id" id="estado_id">
                                            <option value="NULL">[ --- Selecione --- ]</option>
                                            @foreach ($estados as $estado)
                                                @if (isset($musico->estado->id) and ($musico->estado->id == $estado->id))
                                                    <option value="{{ $estado->id }}" selected>{{ $estado->nome }}</option>
                                                @else
                                                    <option value="{{ $estado->id }}">{{ $estado->nome }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <i class="fancy-arrow"></i>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label>Cidade</label>
                                    <div class="fancy-form fancy-form-select">
                                        <select class="form-control select2" name="cidade_id">
                                            <option value="NULL">[ --- Selecione --- ]</option>
                                            @foreach ($cidades as $cidade)
                                                @if (isset($musico->cidade->id) and ($musico->cidade->id == $cidade->id))
                                                    <option value="{{ $cidade->id }}" selected>{{ $cidade->nome }}</option>
                                                @else
                                                    <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <i class="fancy-arrow"></i>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <label>Logradouro</label>
                                    <div class="fancy-form">
                                        <i class="fa fa-globe"></i>
                                        <input type="tel" name="logradouro" value="{{ $musico->logradouro }}" class="form-control required">
                                        <span class="fancy-tooltip top-left"><em>Inform o Bairro, Região, Avenida, ou Similar!</em></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </div>
            </div>

            <!-- Botões para Navegação -->
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-edit"></i> Salvar
                </button>
                <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/musicos') }}';">
                    <i class="glyphicon glyphicon-home"></i> Retornar
                </button>
            </div>

        </form>

    </div>

@endsection

@section('post-script')
    <script type="text/javascript">
        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
            $.get('{{ url('/get-cidades') }}/' + idEstado, function (cidades) {
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nome + '</option>');
                });
            });
        });
    </script>
@endsection