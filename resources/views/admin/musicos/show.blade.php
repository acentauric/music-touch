{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Músicos</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/musicos') }}">Músicos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Dados do Registro -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-cog"></i> DADOS BÁSICOS
                        <ul class="options pull-right list-inline">
                            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                            <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Nome:</strong> {{ $musico->nome }}</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Data de Nascimento:</strong> {{ date("d/m/Y", strtotime($musico->data_de_nascimento)) }}</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Ano de Início da Carreira:</strong> 2003 (15 anos de experiência)</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Objetivo:</strong> Profissional</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-cog"></i> DADOS DO REGISTRO
                        <ul class="options pull-right list-inline">
                            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                            <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Identificador:</strong> {{ $musico->id }}</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Cadastrado Por:</strong> Ramon Santana Santos</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Data de Criação:</strong> {{ $musico->created_at->format('d/m/Y H:i:s') }} ({{ $musico->created_at->diffForHumans() }})</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Data de Alteração:</strong> {{ $musico->updated_at->format('d/m/Y H:i:s') }} ({{ $musico->updated_at->diffForHumans() }})</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-cog"></i> ENDEREÇOS
                        <ul class="options pull-right list-inline">
                            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                            <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Residencial:</strong> Endereço: SQN 215 Bloco J; Nº: 0; Complemento: ap. 107; Bairro: Asa Norte; Cidade: Brasília; UF: DF.</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Profissional:</strong> Endereço: SQN 215 Bloco J; Nº: 0; Complemento: ap. 107; Bairro: Asa Norte; Cidade: Brasília; UF: DF.</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Estúdio:</strong> Endereço: SQN 215 Bloco J; Nº: 0; Complemento: ap. 107; Bairro: Asa Norte; Cidade: Brasília; UF: DF.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-cog"></i> DADOS MUSICAIS
                        <ul class="options pull-right list-inline">
                            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                            <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Tipos de função:</strong> Baixista, Guitarrista.</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Tipo de som:</strong> Eu toco qualquer coisa que esteja envolvido com ROck desde que tenha tambem o trabalho autoral.</li>
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Influências: </strong> Kiss, Guns, Rancid, Ramones e algumas coisas 80tistas.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="button" class="btn btn-primary" onclick="Javascript: location.href='{{ url('admin/musicos/editar/'.$musico->id) }}';">
                <i class="glyphicon glyphicon-ok-circle"></i> Editar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/musicos') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>

    </div>

@endsection