{{-- Adiciona a estrutura de Layout Base --}}
@extends('layouts.app')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}">Home</a></li>
        <li><a href="{{ url('/bandas') }}">Sistema de Bandas</a></li>
        <li><a href="{{ url('/estilos') }}">Estilos</a></li>
        <li class="active">Consultar</li>
    </ol>

    <!-- Painel com Informações do Registro Consultado -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Informações da Estilo</h3>
        </div>
        <div class="panel-body">

            <!-- Informações do Registro -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><i class="fa fa-headphones"></i> Nome</div>
                        <div class="panel-body">
                            {{ $estilo->nome }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Cadastrado Por</div>
                        <div class="panel-body">
                            Ramon Santana Santos
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-info">
                        <div class="panel-heading"><i class="glyphicon glyphicon-cog"></i> Identificador</div>
                        <div class="panel-body">
                            {{ $estilo->id }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-info">
                        <div class="panel-heading text-center"><i class="glyphicon glyphicon-time"></i> Data de Criação</div>
                        <div class="panel-body text-center">
                            {{ $estilo->created_at->format('d/m/Y H:i:s') }} ({{ $estilo->created_at->diffForHumans() }})
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-info">
                        <div class="panel-heading text-center"><i class="glyphicon glyphicon-time"></i> Data de Alteração</div>
                        <div class="panel-body text-center">
                            {{ $estilo->updated_at->format('d/m/Y H:i:s') }} ({{ $estilo->updated_at->diffForHumans() }})
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Botões para Navegação -->
    <div class="text-right">
        <button type="button" class="btn btn-primary" onclick="Javascript: location.href='{{ url('estilo/editar/'.$estilo->id) }}';">
            <i class="glyphicon glyphicon-ok-circle"></i> Editar
        </button>
        <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/estilos') }}';">
            <i class="glyphicon glyphicon-home"></i> Retornar
        </button>
    </div>

@endsection