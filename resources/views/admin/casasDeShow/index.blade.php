{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Casas de Show</h1>
        <ol class="breadcrumb">
            <li><a href="#">Casas de Show</a></li>
            <li class="active">Início</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Painel com a Listagem dos Registros Cadastrados JQGRID TABLE -->
        <table id="jqgrid"></table>
        <div id="pager_jqgrid"></div>

        <!-- Token de Segurança -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <!-- JQGRID CSS -->
        <link href="{{ URL::asset('admin/assets/plugins/jqgrid/css/ui.jqgrid.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin/assets/css/layout-jqgrid.css') }}" rel="stylesheet" type="text/css" />

        <!-- JQGRID SCRIPTS -->
        <script type="text/javascript">

            //Define o caminho para o diretório de plugins
            var plugin_path = "{{URL::asset('admin/assets/plugins/')}}/";

            //Enviando o Token de Segurança via Ajax
            $.ajaxSetup({headers:{'X-CSRF-Token': $('input[name="_token"]').val()}});

            //Executa as funções para o funcionamento do JqGrid
            loadScript(plugin_path + "jqgrid/js/jquery.jqGrid.js", function(){
                loadScript(plugin_path + "jqgrid/js/i18n/grid.locale-en.js", function(){
                    loadScript(plugin_path + "bootstrap.datepicker/js/bootstrap-datepicker.min.js", function(){

                        //Armazena o objeto Json enviado pelo controller
                        var jqgrid_data = {!! $registrosJson !!};

                        //Realiza a montagem do Jgrid
                        jQuery("#jqgrid").jqGrid({
                            data : jqgrid_data,
                            datatype : "local",
                            height : '',
                            colNames : ['Ações', 'Id', 'User Id', 'Nome', 'Data de Criação', 'Data de Alteração'],
                            colModel : [
                                { name:'act', index:'act', sortable:false, width:100 },
                                { name:'id', index:'id', sorttype:"int", width:50 },
                                { name:'user_id', index:'user_id', sorttype:"int"  },
                                { name:'nome', index:'nome', editable : true, sorttype:"text"  },
                                { name:'created_at', index:'screated_at', sorttype:"date", align:"center", width:100, classes:"visible-desktop" },
                                { name:'updated_at', index:'supdated_at', sorttype:"date", align:"center", width:100, classes:"visible-desktop" }],
                            rowNum : 10,

                            //Apresenta a quantidade de opções a depender da quantidade de registros
                            rowList :[@for ($i = 1; $i <= round($registrosQtd / 10); $i++){{ $i * 10 }}, @endfor],

                            pager : '#pager_jqgrid',
                            sortname : 'nome',
                            toolbarfilter: true,
                            viewrecords : true,
                            sortorder : "asc",
                            gridComplete: function(){
                                var ids = jQuery("#jqgrid").jqGrid('getDataIDs');
                                for(var i=0;i < ids.length;i++){
                                    var cl = ids[i];
                                    sh = "<button class='btn btn-xs btn-default btn-quick' title='Consultar' onclick=\"$(location).attr('href', '{{ url('/admin/produtoras/consultar') }}/"+cl+"');\"><i class='fa fa-eye'></i></button>";
                                    be = "<button class='hidden-desktop btn btn-xs btn-default btn-quick' title='Edição Rápida' onclick=\"jQuery('#jqgrid').editRow('"+cl+"', '', '', '', '', '');\"><i class='fa fa-pencil'></i></button>";
                                    se = "<button class='hidden-desktop btn btn-xs btn-default btn-quick' title='Salvar Edição' onclick=\"jQuery('#jqgrid').saveRow('"+cl+"');\"><i class='fa fa-save'></i></button>";
                                    ca = "<button class='hidden-desktop btn btn-xs btn-default btn-quick' title='Cancel Edição' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
                                    ge = "<button class='btn btn-xs btn-default btn-quick' title='Gerenciar' onclick=\"$(location).attr('href', '{{ url('/admin/produtoras/gerenciar/') }}/"+cl+"');\"><i class='fa fa-gear'></i></button>";
                                    jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:sh+be+se+ca+ge});
                                }
                            },
                            editurl : "{{ url('/admin/casas-de-show/ajax') }}",
                            caption : "Casas de Show",
                            multiselect : false,
                            autowidth : true,
                        });

                        //
                        jQuery("#jqgrid").jqGrid('navGrid', "#pager_jqgrid", {
                            edit : false,
                            add : false,
                            del : true
                        });

                        //
                        jQuery("#jqgrid").jqGrid('inlineNav', "#pager_jqgrid");

                        // Get Selected ID's
                        jQuery("a.get_selected_ids").bind("click", function() {
                            s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
                            alert(s);
                        });

                        // Select/Unselect specific Row by id
                        jQuery("a.select_unselect_row").bind("click", function() {
                            jQuery("#jqgrid").jqGrid('setSelection', "13");
                        });

                        // Select/Unselect specific Row by id
                        jQuery("a.delete_row").bind("click", function() {
                            var su=jQuery("#jqgrid").jqGrid('delRowData',1);
                            if(su) alert("Succes. Write custom code to delete row from server"); else alert("Already deleted or not in list");
                        });

                        // On Resize
                        jQuery(window).resize(function() {

                            if(window.afterResize) {
                                clearTimeout(window.afterResize);
                            }

                            // After Resize Code
                            window.afterResize = setTimeout(function() {
                                jQuery("#jqgrid").jqGrid('setGridWidth', jQuery("#middle").width() - 32);
                            }, 500);

                        });

                        /**
                         @STYLING
                        **/
                        jQuery(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
                        jQuery(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
                        jQuery(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
                        jQuery(".ui-jqgrid-pager").removeClass("ui-state-default");
                        jQuery(".ui-jqgrid").removeClass("ui-widget-content");

                        jQuery(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
                        jQuery(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
                        jQuery(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
                        jQuery(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
                        jQuery(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
                        jQuery(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
                        jQuery(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
                        jQuery(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
                        jQuery(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

                        jQuery( ".ui-icon.ui-icon-seek-prev" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
                        jQuery(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

                        jQuery( ".ui-icon.ui-icon-seek-first" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
                        jQuery(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

                        jQuery( ".ui-icon.ui-icon-seek-next" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
                        jQuery(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

                        jQuery( ".ui-icon.ui-icon-seek-end" ).wrap( "<div class='btn btn-sm btn-default'></div>" );
                        jQuery(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");

                    });
                });
            });
        </script>

    </div>

@endsection