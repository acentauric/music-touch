@section('navside')

<aside id="aside">
    <!--
        Always open:
        <li class="active alays-open">

        LABELS:
            <span class="label label-danger pull-right">1</span>
            <span class="label label-default pull-right">1</span>
            <span class="label label-warning pull-right">1</span>
            <span class="label label-success pull-right">1</span>
            <span class="label label-info pull-right">1</span>
    -->
    <nav id="sideNav"><!-- MAIN MENU -->
        <ul class="nav nav-list">
            <li class="active">
                <a class="dashboard" href="#"><!-- warning - url used by default by ajax (if eneabled) -->
                    <i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-user"></i> <span>Músicos</span>
                </a>
                <ul>
                    <li><a href="{{ url('/admin/musicos') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-users"></i> <span>Bandas</span>
                </a>
                <ul>
                    <li><a href="{{ url('/admin/bandas') }}">Listar Bandas</a></li>
                    <li><a href="{{ url('/admin/bandas') }}">Cadastrar Banda</a></li>
                    <li><a href="{{ url('/admin/bandas') }}">Gerenciar Banda</a></li>
                    <li><a href="{{ url('/admin/bandas/listar/shows') }}">Listar Shows</a></li>
                    <li><a href="{{ url('/admin/bandas/cadastrar/show') }}">Cadastrar Show</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-microphone"></i> <span>Estúdios</span>
                </a>
                <ul>
                    <li><a href="{{ url('/bandas') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-bullhorn"></i> <span>Produtoras</span>
                </a>
                <ul>
                    <li><a href="{{ url('/admin/produtoras') }}">Início</a></li>
                    <li><a href="{{ url('/admin/produtoras/cadastrar/') }}">Cadastrar</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon glyphicon glyphicon-home"></i> <span>Casas de Show</span>
                </a>
                <ul>
                    <li><a href="{{ url('/admin/casas-de-show') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon glyphicon glyphicon-shopping-cart"></i> <span>Fornecedores</span>
                </a>
                <ul>
                    <li><a href="{{ url('/bandas') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon glyphicon glyphicon-certificate"></i> <span>Organizações</span>
                </a>
                <ul>
                    <li><a href="{{ url('/bandas') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon glyphicon glyphicon-headphones"></i> <span>Fãs</span>
                </a>
                <ul>
                    <li><a href="{{ url('/bandas') }}">Início</a></li>
                    <li><a href="#">Relatorio</a></li>
                </ul>
            </li>

        </ul>

        <!-- SECOND MAIN LIST -->
        <h3>MAIS</h3>
        <ul class="nav nav-list">
            <li class="" id="">
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-book"></i> <span>Cadastros</span>
                </a>
                <ul style="display: none;">
                    <li>
                        <a href="#"><i class="fa fa-menu-arrow pull-right"></i> Musical</a>
                        <ul style="display: none;">
                            <li><a href="{{ url('/estilos') }}">Estilos/Gênero</a></li>
                            <li><a href="{{ url('/instrumentos') }}">Instrumentos</a></li>
                            <li><a href="{{ url('/estados') }}">Tipos de Função</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-menu-arrow pull-right"></i> Geográfico</a>
                        <ul style="display: none;">
                            <li><a href="{{ url('/estados') }}">Estados</a></li>
                            <li><a href="{{ url('/bandas') }}">Cidades</a></li>
                            <li><a href="{{ url('/bandas') }}">Região</a></li>
                            <li><a href="{{ url('/bandas') }}">Bairro</a>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="" id="">
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-globe"></i> <span>Crawlers</span>
                </a>
                <ul style="display: none;">
                    <li><a href="{{ url('/crawlers') }}">Início</a></li>
                    <li><a href="{{ url('/crawlers/fsb') }}">Forme sua Banda</a></li>
                </ul>
            </li>
        </ul>

    </nav>

    <span id="asidebg"><!-- aside fixed background --></span>
</aside>
@endsection