{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Músicos</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/produtoras') }}">Produtoras</a></li>
            <li class="active">Consultar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Dados do Registro -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-cog"></i> DADOS BÁSICOS
                        <ul class="options pull-right list-inline">
                            <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                            <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                            <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="glyphicon glyphicon-cog"></i> <strong>Nome:</strong> {{ $produtora->nome }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="button" class="btn btn-primary" onclick="Javascript: location.href='{{ url('produtora/editar/'.$produtora->id) }}';">
                <i class="glyphicon glyphicon-ok-circle"></i> Editar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/produtoras') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>

    </div>

@endsection