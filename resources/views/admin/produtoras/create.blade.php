{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Músicos</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/produtoras') }}">Produtoras</a></li>
            <li class="active">Cadastrar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Formulário para a Edição do Registro Consultado -->
        <form method="POST" class="" action="{{ url('/admin/produtoras/') }}" data-success="Edição Realizada com sucesso!">

            {{-- Adiciona Informações de Controle do Framework --}}
            {{ csrf_field() }}

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-cog"></i> <strong>DADOS BÁSICOS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body">

                    <!-- Apresenta Erros na Ação se Existirem -->
                    @include('common.errors')

                    <!-- Campos do Formulário -->
                    <fieldset>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label>Nome *</label>
                                    <div class="fancy-form">
                                        <i class="fa fa-tag"></i>
                                        <input type="text" name="nome" value="{{ $produtora->nome }}" class="form-control required" data-placeholder="X" placeholder="Informe o Nome da Produtora">
                                        <span class="fancy-tooltip top-left">
                                            <em>Informe o Nome da Produtora</em>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </div>
            </div>

            <!-- Botões para Navegação -->
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-edit"></i> Salvar
                </button>
                <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/produtoras') }}';">
                    <i class="glyphicon glyphicon-home"></i> Retornar
                </button>
            </div>
        </form>
    </div>

@endsection