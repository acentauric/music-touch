{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Produtoras</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/produtoras') }}">Produtoras</a></li>
            <li class="active">Gerenciar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Apresenta Erros na Ação se Existirem -->
        @include('common.errors')

        <!-- Formulário para a Edição do Registro Consultado -->
        <form method="POST" class="" action="{{ url('/admin/produtoras/atualizar/'.$produtora->id) }}" data-success="Edição Realizada com sucesso!">

            {{-- Adiciona Informações de Controle do Framework --}}
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="glyphicon glyphicon-cog"></i> <strong>DADOS BÁSICOS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="toggle">
                        <label>Nome</label>
                        <div class="toggle-content" style="display: block;">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="fancy-form">
                                                <i class="fa fa-tag"></i>
                                                <input type="text" name="nome" value="{{ $produtora->nome }}" class="form-control required" data-placeholder="X" placeholder="Informe o Nome da Produtora">
                                        <span class="fancy-tooltip top-left">
                                            <em>Informe o Nome da Produtora</em>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="toggle">
                        <label>Contatos</label>
                        <div class="toggle-content" style="display: none;">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Telefone Fixo</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="text" name="fixo" value="{{ $produtora->fixo }}" class="form-control masked" data-format="(99) 9999-9999" data-placeholder="X" placeholder="Exemplo: (71) 3333-3333">
                                                <span class="fancy-tooltip top-left"><em>Informe o Telefone Fixo!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Telefone Celular</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="text" name="celular" value="{{ $produtora->celular }}" class="form-control masked" data-format="(99) 99999-9999" data-placeholder="X" placeholder="Exemplo: (71) 99999-9999">
                                                <span class="fancy-tooltip top-left"><em>Informe o Telefone Celular!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>E-mail</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="logradouro" value="{{ $produtora->logradouro }}" class="form-control required" placeholder="Exemplo: contato@produtora.com.br">
                                                <span class="fancy-tooltip top-left"><em>Informe o E-mail!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Site Oficial</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="site" value="{{ $produtora->site }}" class="form-control required" placeholder="Exemplo: http://www.produtora.com.br">
                                                <span class="fancy-tooltip top-left"><em>Informe o Site Oficial!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Página no Facebook</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="site" value="{{ $produtora->facebook }}" class="form-control required" placeholder="Exemplo: http://www.facebook.com/produtora">
                                                <span class="fancy-tooltip top-left"><em>Informe a Página no Facebook!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Canal no Youtube</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="site" value="{{ $produtora->youtube }}" class="form-control required" placeholder="Exemplo: http://www.youtube.com/produtora">
                                                <span class="fancy-tooltip top-left"><em>Informe o Canal no Youtube!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>

                    <div class="toggle">
                        <label>Endereço</label>
                        <div class="toggle-content" style="display: none;">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>País</label>
                                            <div class="fancy-form fancy-form-select">
                                                <select class="form-control" name="estado_id" id="estado_id">
                                                    <option value="NULL">[ --- Selecione --- ]</option>
                                                    @foreach ($estados as $estado)
                                                        @if (isset($produtora->estado->id) and ($produtora->estado->id == $estado->id))
                                                            <option value="{{ $estado->id }}" selected>{{ $estado->nome }}</option>
                                                        @else
                                                            <option value="{{ $estado->id }}">{{ $estado->nome }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Estado</label>
                                            <div class="fancy-form fancy-form-select">
                                                <select class="form-control" name="estado_id" id="estado_id">
                                                    <option value="NULL">[ --- Selecione --- ]</option>
                                                    @foreach ($estados as $estado)
                                                        @if (isset($produtora->estado->id) and ($produtora->estado->id == $estado->id))
                                                            <option value="{{ $estado->id }}" selected>{{ $estado->nome }}</option>
                                                        @else
                                                            <option value="{{ $estado->id }}">{{ $estado->nome }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Cidade</label>
                                            <div class="fancy-form fancy-form-select">
                                                <select class="form-control" name="cidade_id">
                                                    <option value="NULL">[ --- Selecione --- ]</option>
                                                    @foreach ($cidades as $cidade)
                                                        @if (isset($produtora->cidade->id) and ($produtora->cidade->id == $cidade->id))
                                                            <option value="{{ $cidade->id }}" selected>{{ $cidade->nome }}</option>
                                                        @else
                                                            <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Logradouro</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="logradouro" value="{{ $produtora->logradouro }}" class="form-control required" placeholder="Exemplo: Avenida Tiradentes; Edifício 403;">
                                                <span class="fancy-tooltip top-left"><em>Informe Bairro, Região, Avenida, ou Similar!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Número</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="logradouro" value="{{ $produtora->numero }}" class="form-control required" placeholder="Exemplo: 402 C">
                                                <span class="fancy-tooltip top-left"><em>Informe o Número da Residência, do Bloco, ou Similar!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>CEP</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="text" name="logradouro" value="{{ $produtora->cep }}" class="form-control masked" data-format="99.999-999" data-placeholder="X" placeholder="Exemplo: 40.440-360">
                                                <span class="fancy-tooltip top-left"><em>Informe Número do CEP!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Botões para Navegação -->
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-edit"></i> Salvar
                </button>
                <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/produtoras') }}';">
                    <i class="glyphicon glyphicon-home"></i> Retornar
                </button>
            </div>
        </form>
    </div>

@endsection