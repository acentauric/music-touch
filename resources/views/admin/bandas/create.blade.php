{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}">Home</a></li>
        <li><a href="{{ url('/bandas') }}">Sistema de Bandas</a></li>
        <li><a href="{{ url('/bandas') }}">Estilos</a></li>
        <li class="active">Cadastrar</li>
    </ol>

    <!-- Formulário para o Cadastro de um Novo Registro -->
    <form method="POST" class="form-horizontal" action="{{ url('banda') }}" >

        {{-- Adiciona Informações de Controle do Framework --}}
        {{ csrf_field() }}

        <!-- Painel com Informações para o Cadastro -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informações da Banda</h3>
            </div>
            <div class="panel-body">

                <!-- Apresenta Erros na Ação se Existirem -->
                @include('common.errors')

                <!-- Campos do Formulário -->
                <div class="form-group">
                    <label for="nome" class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-8">
                        <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome da Banda">
                    </div>
                </div>
                <div class="form-group">
                    <label for="data_de_formacao" class="col-sm-2 control-label">Data de Formação</label>
                    <div class="col-sm-8">
                        <input type="text" name="data_de_formacao" id="data_de_formacao" class="form-control" placeholder="dd/mm/AAAA">
                    </div>
                </div>
                <div class="form-group">
                    <label for="data_de_termino" class="col-sm-2 control-label">Data de Témino</label>
                    <div class="col-sm-8">
                        <input type="text" name="data_de_termino" id="data_de_termino" class="form-control" placeholder="dd/mm/AAAA">
                    </div>
                </div>

            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-ok-circle"></i> Cadastrar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/bandas') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>

    </form>

@endsection