{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

@section('pre-style')
<link href="{{URL::asset('admin/assets/css/layout-nestable.css')}}" rel="stylesheet" type="text/css">
@endsection

{{-- Adiciona o Conteúdo da View --}}
@section('content')

<!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
<header id="page-header">
    <h1>Bandas</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/produtoras') }}">Bandas</a></li>
        <li class="active">Cadastrar Show</li>
    </ol>
</header>

<!-- Estrutura Principal da Página -->
<div id="content" class="padding-20">

    <!-- Formulário para a Edição do Registro Consultado -->
    <form method="POST" class="" action="{{ url('/banda/cadastrar/show') }}" data-success="Edição Realizada com sucesso!">

        {{-- Adiciona Informações de Controle do Framework --}}
        {{ csrf_field() }}

        <!-- Apresenta Erros na Ação se Existirem -->
        @include('common.errors')

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-cog"></i> <strong>DADOS BÁSICOS</strong>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                </ul>
            </div>
            <div class="panel-body" style="display: none;">
                <fieldset>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12">
                                <label>Nome do Show *</label>
                                <div class="fancy-form">
                                    <i class="fa fa-tag"></i>
                                    <input type="text" name="nome" value="" class="form-control required" data-placeholder="X" placeholder="Informe o Nome do Show">
                                    <span class="fancy-tooltip top-left">
                                        <em>Informe o Nome do Show</em>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3">
                                <label>Início Previsto (Data) *</label>
                                <div class="fancy-form">
                                    <i class="fa fa-tag"></i>
                                    <input type="text" class="form-control datepicker" data-format="dd/mm/yyyy" data-lang="pt" data-RTL="false">
                                    <span class="fancy-tooltip top-left">
                                        <em>Informe o Início Previso (Data) para Realização do Show</em>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label>Início Previsto (Hora) *</label>
                                <div class="fancy-form">
                                    <input type="text" class="form-control timepicker" value="00 : 00 : PM">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label>Término Previsto (Data) *</label>
                                <div class="fancy-form">
                                    <i class="fa fa-tag"></i>
                                    <input type="text" class="form-control datepicker" data-format="dd/mm/yyyy" data-lang="pt" data-RTL="false">
                                    <span class="fancy-tooltip top-left">
                                        <em>Informe o Término Previso (Data) para Realização do Show</em>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <label>Término Previsto (Hora) *</label>
                                <div class="fancy-form">
                                    <input type="text" class="form-control timepicker" value="00 : 00 : 00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12">
                                <label>Local (Casa de Show) *</label>
                                <div class="fancy-form fancy-form-select">
                                    <select class="form-control" name="casa_de_show_id" id="casa_de_show_id">
                                        <option value="NULL">[ --- Selecione --- ]</option>
                                        @foreach ($casas_de_show as $casa_de_show)
                                            <option value="{{ $casa_de_show->id }}">{{ $casa_de_show->nome }}</option>
                                        @endforeach
                                    </select>
                                    <i class="fancy-arrow"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-users"></i> <strong>BANDAS PARTICIPANTES</strong>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                </ul>
            </div>
            <div class="panel-body" style="display: block;">
                <fieldset>
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <label>Nome da Banda *</label>
                            <div class="fancy-form fancy-form-select">
                                <select class="form-control" name="banda_id" id="banda_id">
                                    <option value="NULL">[ --- Selecione --- ]</option>
                                    @foreach ($bandas as $banda)
                                        <option value="{{ $banda->id }}">{{ $banda->nome }}</option>
                                    @endforeach
                                </select>
                                <i class="fancy-arrow"></i>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <label>Ação</label>
                            <div class="fancy-form">
                                <button type="button" class="btn btn-default" id="adicionar">
                                    <i class="fa fa-sign-in"></i> Adicionar
                                </button>
                                <button type="button" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Cadastrar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-hover nomargin" id="bandas_adicionadas">
                                    <thead>
                                        <tr>
                                            <th width="10%">Ação</th>
                                            <th>Bandas Adicionadas</th>
                                            <th width="30%">Informações</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list"></i> <strong>GRADE DO SHOW</strong>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                </ul>
            </div>
            <div class="panel-body" style="display: block;">
                <div class="col-sm-12 col-md-12">

                    <!-- Lista -->
                    <div class="dd" id="nestable_list_3">
                        <ol class="dd-list"></ol>
                    </div>

                    <!-- Ordenamento Json da Lista -->
                    <input type="hidden" id="nestable_list_3_output" class="form-control margin-bottom10"/>

                </div>
            </div>
        </div>


        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Salvar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/bandas') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>
    </form>
</div>

@endsection

@section('post-script')

    <script>
        //Função para Adicionar Bandas à lista de Bandas Adicionadas
        $(document).ready(function(){

            //Captura a ação do Clique do botão Adicionar para realizar a ação de Adição
            $(document).on('click', '#adicionar', function()
            {
                //**************************************************
                //Captura o ID e o Nome de Banda que será Adicionada
                var banda_id = $('#banda_id').find('option:selected').val();
                var banda_nome = $('#banda_id').find('option:selected').text();

                //Constroe a colunas que serão adicionadas na tabela de Bandas Adicionadas
                var coluna_acoes = '<td><button type="button" class="btn btn-default btn-xs" name="remover" id="remover_'+banda_id+'"><i class="fa fa-trash-o nopadding"></i></button></td>';
                var coluna_nome = '<td>'+banda_nome+'<input type="hidden" name="lista_bandas_adicionadas" value="'+banda_id+'" /></td>';
                var coluna_informacoes = '<td><span class="label label-success">Adicionada </span></td>';

                //Constroe a nova linha que será adicionada na tabela de Bandas Adicionadas
                var nova_tr = '<tr>'+coluna_acoes+''+coluna_nome+''+coluna_informacoes+'</tr>';

                //Adiciona a banda Selecionada pelo combo de Bandas na lista de Bandas Adicionadas
                $('#bandas_adicionadas').append(nova_tr);

                //********************************************************************
                //Constroe o novo item da lista que será adicionada na Grade de Bandas Adicionadas
                var nova_li = '<li class="dd-item dd3-item" data-id="'+banda_id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content">'+banda_nome+'</div></li>';

                //Adiciona a banda Selecionada pelo combo de Bandas na Grade Do Evento
                $('.dd-list').append(nova_li);

            });
        });

        //Função para Remvoer Bandas da lista de Bandas Adicionadas
        $(document).ready(function(){

            //Captura a ação do Clique do botão Adicionar para realizar a ação de Adição
            $(document).on('click', '[name=remover]', function()
            {
                //Remover a banda Deletada da lista de Bandas Adicionadas
                $(this).closest('tr').remove();

                //Remover a banda Deletada da Grade do Show


            });
        });
    </script>

    <script type="text/javascript">
        loadScript(plugin_path + "nestable/jquery.nestable.js", function(){

            if(jQuery().nestable) {

                var updateOutput = function (e) {
                    var list = e.length ? e : $(e.target),
                            output = list.data('output');
                    if (window.JSON) {
                        output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                    } else {
                        output.val('JSON browser support required for this demo.');
                    }
                };

                // Nestable list 3
                jQuery('#nestable_list_3').nestable({
                    group: 1
                }).on('change', updateOutput);

                // output initial serialised data
                updateOutput(jQuery('#nestable_list_3').data('output', jQuery('#nestable_list_3_output')));

                // Expand All
                jQuery("button[data-action=expand-all]").bind("click", function() {
                    jQuery('.dd').nestable('expandAll');
                });

                // Collapse All
                jQuery("button[data-action=collapse-all]").bind("click", function() {
                    jQuery('.dd').nestable('collapseAll');
                });

            }

        });
    </script>

    <script type="text/javascript" src="{{URL::asset('admin/assets/plugins/nestable/jquery.nestable.js')}}"></script>
@endsection