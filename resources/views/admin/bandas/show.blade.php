{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

<!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
<header id="page-header">
    <h1>Bandas</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/bandas') }}">Bandas</a></li>
        <li class="active">Consultar</li>
    </ol>
</header>

<!-- Estrutura Principal da Página -->
<div id="content" class="padding-20">

    <!-- Apresenta Erros na Ação se Existirem -->
    @include('common.errors')

    <!-- Formulário para a Edição do Registro Consultado -->
    <form method="POST" class="" action="{{ url('/admin/bandas/atualizar/'.$banda->id) }}" data-success="Edição Realizada com sucesso!">

        {{-- Adiciona Informações de Controle do Framework --}}
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-tag"></i> <strong>{{ $banda->nome }}</strong>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                </ul>
            </div>
            <div class="panel-body">

                <div class="toggle">
                    <label>Dados Básicos</label>
                    <div class="toggle-content">
                        <ul class="list-unstyled">
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                            <li><strong>Telefone Fixo:</strong> {{ $banda->telefone_fixo }}</li>
                            <li><strong>Telefone Celular:</strong> {{ $banda->telefone_celular }}</li>
                            <li><strong>E-mail:</strong> {{ $banda->email }}</li>
                            <li><strong>Site Oficial:</strong> {{ $banda->site }}</li>
                            <li><strong>Página no Facebook:</strong> {{ $banda->facebook }}</li>
                            <li><strong>Canal no Youtube:</strong> {{ $banda->youtube }}</li>
                            <li><strong>País:</strong> (Brasil)</li>
                            <li><strong>Estado:</strong> {{ @$banda->cidade->estado->nome }}</li>
                            <li><strong>Cidade:</strong> {{ @$banda->cidade->nome }}</li>
                            <li><strong>Logradouro:</strong> {{ $banda->logradouro }}</li>
                            <li><strong>Número:</strong> {{ $banda->numero }}</li>
                            <li><strong>CEP:</strong> {{ $banda->cep }}</li>
                        </ul>
                    </div>
                </div>
                <div class="toggle">
                    <label>Músicos</label>
                    <div class="toggle-content">
                        <ul class="list-unstyled">
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                            <li><strong>Nome:</strong> {{ $banda->nome }}</li>
                        </ul>
                    </div>
                </div>
                <div class="toggle">
                    <label>Shows</label>
                    <div class="toggle-content">
                        <div class="table-responsive">
                            <table class="table table-condensed nomargin">
                                <thead>
                                    <tr>
                                        <th>Link</th>
                                        <th>Nome</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($banda->shows as $show)
                                        <tr>
                                            <td>#</td>
                                            <td>{{ $show->nome }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="toggle">
                    <label>Conexões</label>
                    <div class="toggle-content">
                        <ul class="list-unstyled">
                            <li><strong>Fotolog:</strong> (http://www.fotolog.com/hardcallrock/)</li>
                            <li><strong>Youtube:</strong> (https://www.youtube.com/channel/UCoKp_e0E1E66mYMWfLaU_Lg)</li>
                            <li><strong>Facebook:</strong> (https://www.facebook.com/hardcall.rock.7)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-circle-o"></i> Ações <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/admin/bandas/gerenciar/'.$banda->id) }}"><i class="fa fa-gear"></i> Gerenciar Banda</a></li>
                    <li><a href="{{ url('/admin/bandas/gerenciar/'.$banda->id) }}"><i class="fa fa-gear"></i> Cadastrar Show</a></li>
                    <li><a href="#"><i class="fa fa-question-circle"></i> Another action</a></li>
                    <li><a href="#"><i class="fa fa-print"></i> Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-cogs"></i> Separated link</a></li>
                </ul>
            </div>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/bandas') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>
    </form>
</div>

@endsection

{{-- Adiciona os Scripts da View --}}
@section('post-script')
    <script type="text/javascript">
        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
            $.get('{{ url('/admin/estadoAjax/get-cidades') }}/' + idEstado, function (cidades) {
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nome + '</option>');
                });
            });
        });
    </script>
@endsection