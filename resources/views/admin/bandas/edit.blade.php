{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}">Home</a></li>
    <li><a href="{{ url('/bandas') }}">Sistema de Bandas</a></li>
    <li><a href="{{ url('/bandas') }}">Bandas</a></li>
    <li class="active">Editar</li>
    </ol>

    <!-- Formulário para a Edição do Registro Consultado -->
    <form method="POST" class="form-horizontal" action="{{ url('banda/'.$banda->id) }}">

        {{-- Adiciona Informações de Controle do Framework --}}
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informações da Banda</h3>
            </div>
            <div class="panel-body">

                <!-- Apresenta Erros na Ação se Existirem -->
                @include('common.errors')

                <!-- Campos do Formulário -->
                <div class="form-group">
                    <label for="banda" class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-6">
                        <input type="text" name="nome" id="banda-nome" class="form-control" value="{{$banda->nome}}">
                    </div>
                </div>

            </div>
        </div>

        <!-- Botões para Navegação -->
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-edit"></i> Editar
            </button>
            <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/bandas') }}';">
                <i class="glyphicon glyphicon-home"></i> Retornar
            </button>
        </div>

    </form>

@endsection