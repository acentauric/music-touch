{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Bandas</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/bandas') }}">Bandas</a></li>
            <li><a href="{{ url('/admin/bandas/gerenciar/'.$banda->id) }}">Gerenciar</a></li>
            <li class="active">Conexões</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Apresenta Erros na Ação se Existirem -->
        @include('common.errors')

        <!-- Painel com Informações para a Edição do Registro -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-plug"></i> <strong>CONEXÃO COM O FOTOLOG</strong>
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                </ul>
            </div>
            <div class="panel-body" style="display: block;">
                <div class="toggle active">
                    <label>Informações do Rastreamento</label>
                    <div class="toggle-content">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <h4><strong>Dados de Rastreamento</strong></h4>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Site:</strong> Fotolog</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Usuário:</strong> {{ $crawled->user }}</li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <h4><strong>links Rastreados</strong></h4>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Link Principal:</strong> http://www.fotolog.com/hardcallrock</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Link Secundário:</strong> http://www.fotolog.com/hardcallrock/mosaic</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="toggle active">
                    <label>Informações Coletadas</label>
                    <div class="toggle-content">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <h4><strong>Informações Relacionáveis</strong></h4>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-thumbs-down"></i> <strong>Aniversário:</strong> {{ $crawled->aniversario }}</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Cidade:</strong> {{ $crawled->cidade }}</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Estado:</strong> {{ $crawled->estado }}</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>País:</strong> <span></a>{{ $crawled->pais }}</span></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <h4><strong>Informações Não Relacionáveis</strong></h4>
                                <ul class="list-unstyled">
                                    <li><strong>Membro Desde:</strong> 08/05/2007</li>
                                    <li><strong>Fotos Publicadas:</strong> 747</li>
                                    <li><strong>Flashs:</strong> 0</li>
                                    <li><strong>Visualizações:</strong> 10K</li>
                                    <li><strong>Amigos:</strong> 2320</li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <h4><strong>Informações Acessórias</strong></h4>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Sexo:</strong> {{ $crawled->sexo }}</li>
                                    <li><i class="fa fa-thumbs-o-up"></i> <strong>Estado Civil:</strong> {{ $crawled->estado_civil }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Formulário para a Edição do Registro Consultado -->
        <form method="POST" class="" action="{{ url('/admin/bandas/atualizar/'.$banda->id) }}" data-success="Edição Realizada com sucesso!">

            {{-- Adiciona Informações de Controle do Framework --}}
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Botões para Navegação -->
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-edit"></i> Salvar
                </button>
                <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/bandas') }}';">
                    <i class="glyphicon glyphicon-home"></i> Retornar
                </button>
            </div>
        </form>
    </div>

@endsection


@section('post-script')
    <script type="text/javascript">
        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
            $.get('{{ url('/admin/estadoAjax/get-cidades') }}/' + idEstado, function (cidades) {
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nome + '</option>');
                });
            });
        });
    </script>
@endsection