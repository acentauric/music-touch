{{-- Adiciona a estrutura de Layout Base --}}
@extends('admin.layouts.admin')
@extends('admin.layouts.header')
@extends('admin.layouts.navside')

{{-- Adiciona o Conteúdo da View --}}
@section('content')

    <!-- Breadcrumb com Links de Navegação entre as Telas do Sistema-->
    <header id="page-header">
        <h1>Bandas</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/bandas') }}">Bandas</a></li>
            <li class="active">Gerenciar</li>
        </ol>
    </header>

    <!-- Estrutura Principal da Página -->
    <div id="content" class="padding-20">

        <!-- Apresenta Erros na Ação se Existirem -->
        @include('common.errors')

        <!-- Formulário para a Edição do Registro Consultado -->
        <form method="POST" class="" action="{{ url('/admin/bandas/atualizar/'.$banda->id) }}" data-success="Edição Realizada com sucesso!">

            {{-- Adiciona Informações de Controle do Framework --}}
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-cog"></i> <strong>DADOS BÁSICOS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="toggle">
                        <label>Nome</label>
                        <div class="toggle-content">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="fancy-form">
                                                <i class="fa fa-tag"></i>
                                                <input type="text" name="nome" value="{{ $banda->nome }}" class="form-control required" data-placeholder="X" placeholder="Informe o Nome da Produtora">
                                        <span class="fancy-tooltip top-left">
                                            <em>Informe o Nome da Produtora</em>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="toggle">
                        <label>Contatos</label>
                        <div class="toggle-content" style="display: none;">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Telefone Fixo</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-phone"></i>
                                                <input type="text" name="telefone_fixo" value="{{ $banda->telefone_fixo }}" class="form-control masked" data-format="(99) 9999-9999" data-placeholder="X" placeholder="Exemplo: (71) 3333-3333">
                                                <span class="fancy-tooltip top-left"><em>Informe o Telefone Fixo!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Telefone Celular</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-mobile-phone"></i>
                                                <input type="text" name="telefone_celular" value="{{ $banda->telefone_celular }}" class="form-control masked" data-format="(99) 99999-9999" data-placeholder="X" placeholder="Exemplo: (71) 99999-9999">
                                                <span class="fancy-tooltip top-left"><em>Informe o Telefone Celular!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>E-mail</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-envelope"></i>
                                                <input type="tel" name="email" size="100" value="{{ $banda->email }}" class="form-control required" placeholder="Exemplo: contato@banda.com.br">
                                                <span class="fancy-tooltip top-left"><em>Informe o E-mail!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Site Oficial</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-globe"></i>
                                                <input type="tel" name="site" size="100" value="{{ $banda->site }}" class="form-control required" placeholder="Exemplo: http://www.banda.com.br">
                                                <span class="fancy-tooltip top-left"><em>Informe o Site Oficial!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Página no Facebook</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-facebook"></i>
                                                <input type="tel" name="facebook" size="100" value="{{ $banda->facebook }}" class="form-control required" placeholder="Exemplo: http://www.facebook.com/banda">
                                                <span class="fancy-tooltip top-left"><em>Informe a Página no Facebook!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Canal no Youtube</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-youtube"></i>
                                                <input type="tel" name="youtube" size="100" value="{{ $banda->youtube }}" class="form-control required" placeholder="Exemplo: http://www.youtube.com/banda">
                                                <span class="fancy-tooltip top-left"><em>Informe o Canal no Youtube!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                    <div class="toggle">
                        <label>Endereço</label>
                        <div class="toggle-content" style="display: none;">
                            <fieldset>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>País</label>
                                            <div class="fancy-form fancy-form-select">
                                                <select class="form-control" name="estado_id" id="estado_id">
                                                    <option value="NULL">Brasil</option>
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Estado</label>
                                            <div class="fancy-form fancy-form-select">
                                                <select class="form-control" name="estado_id" id="estado_id">
                                                    <option value="NULL">[ --- Selecione --- ]</option>
                                                    @foreach ($estados as $estado)
                                                        @if (isset($banda->cidade->estado->id) and ($banda->cidade->estado->id == $estado->id))
                                                            <option value="{{ $estado->id }}" selected>{{ $estado->nome }}</option>
                                                        @else
                                                            <option value="{{ $estado->id }}">{{ $estado->nome }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Cidade</label>
                                            <div class="fancy-form fancy-form-select">

                                                <select class="form-control" name="cidade_id">
                                                    <option value="NULL">[ --- Selecione --- ]</option>
                                                    @foreach ($cidades as $cidade)
                                                        @if (isset($banda->cidade->id) and ($banda->cidade->id == $cidade->id))
                                                            <option value="{{ $cidade->id }}" selected>{{ $cidade->nome }}</option>
                                                        @else
                                                            <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 col-sm-4">
                                            <label>Logradouro</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-home"></i>
                                                <input type="tel" name="logradouro" size="255" value="{{ $banda->logradouro }}" class="form-control required" placeholder="Exemplo: Avenida Tiradentes; Edifício 403;">
                                                <span class="fancy-tooltip top-left"><em>Informe Bairro, Região, Avenida, ou Similar!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>Número</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-flag"></i>
                                                <input type="tel" name="numero" size="50" value="{{ $banda->numero }}" class="form-control required" placeholder="Exemplo: 402 C">
                                                <span class="fancy-tooltip top-left"><em>Informe o Número da Residência, do Bloco, ou Similar!</em></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <label>CEP</label>
                                            <div class="fancy-form">
                                                <i class="fa fa-map-marker"></i>
                                                <input type="text" name="cep" value="{{ $banda->cep }}" class="form-control masked" data-format="99.999-999" data-placeholder="X" placeholder="Exemplo: 40.440-360">
                                                <span class="fancy-tooltip top-left"><em>Informe Número do CEP!</em></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-user"></i> <strong>MÚSICOS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
            </div>

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-star"></i> <strong>SHOWS</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="toggle">
                        <label>Shows Realizados</label>
                        <div class="toggle-content" style="display: none;">
                            <div class="table-responsive">
                                <table class="table table-hover nomargin">
                                    <thead>
                                        <tr>
                                            <th width="25px" align="center">Link</th>
                                            <th class="">Nome</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($banda->shows as $show)
                                            <tr>
                                                <td align="center"><a href="#"><i class="fa fa-external-link"></i></a></td>
                                                <td>{{ $show->id }} - {{ $show->nome }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Painel com Informações para a Edição do Registro -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-plug"></i> <strong>CONEXÕES</strong>
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="panel_colapse plus" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Expand"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="toggle">
                        <label>Links</label>
                        <div class="toggle-content" style="display: block;">
                            <div class="table-responsive">
                                <table class="table table-hover nomargin">
                                    <thead>
                                        <tr>
                                            <th width="25px" align="center">Link</th>
                                            <th>Site</th>
                                            <th class="hidden-xs">Column name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td align="center"><i class="fa fa-external-link"></i></td>
                                            <td>Fotolog</td>
                                            <td class="hidden-xs"><a href="http://www.fotolog.com/hardcallrock/" target="_blank">http://www.fotolog.com/hardcallrock/</a></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><i class="fa fa-external-link"></i></td>
                                            <td>Youtube</td>
                                            <td class="hidden-xs"><a href="https://www.youtube.com/channel/UCoKp_e0E1E66mYMWfLaU_Lg" target="_blank">https://www.youtube.com/channel/UCoKp_e0E1E66mYMWfLaU_Lg</a></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><i class="fa fa-external-link"></i></td>
                                            <td>Facebook</td>
                                            <td class="hidden-xs"><a href="https://www.facebook.com/hardcall.rock.7" target="_blank">https://www.facebook.com/hardcall.rock.7</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Botões para Navegação -->
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-edit"></i> Salvar
                </button>
                <button type="button" class="btn btn-default" onclick="Javascript: location.href='{{ url('/admin/bandas') }}';">
                    <i class="glyphicon glyphicon-home"></i> Retornar
                </button>
            </div>
        </form>
    </div>

@endsection


@section('post-script')
    <script type="text/javascript">
        $('select[name=estado_id]').change(function () {
            var idEstado = $(this).val();
            $.get('{{ url('/admin/estadoAjax/get-cidades') }}/' + idEstado, function (cidades) {
                $('select[name=cidade_id]').empty();
                $.each(cidades, function (key, value) {
                    $('select[name=cidade_id]').append('<option value=' + value.id + '>' + value.nome + '</option>');
                });
            });
        });
    </script>
@endsection